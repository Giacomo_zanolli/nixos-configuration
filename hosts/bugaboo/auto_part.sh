#! /usr/bin/env nix-shell
#! nix-shell -i bash -p git nixUnstable

#The above lines allow us to run the following code as a bash script,
#inside an environment created by nix-shell where git and nix flakes are
#available.

colorPrint() {
  echo -e "$(tput setaf 6)$1$(tput sgr0)"
}

errorPrint() {
  echo -e "$(tput setaf 1)$1$(tput sgr0)"
}

bannerPrint() {
  echo -e "$(tput setaf 3)$1$(tput sgr0)"
}

colorPrint "Welcome. this script will let you set up a new machine with NixOS.\n"

nixosHostName=bugaboo
colorPrint "Building for the following hostname: $nixosHostName"

#Ensure we are running as root
if [ $(id -u) -ne 0 ]; then
  errorPrint "Please run as root (you can use 'sudo su' to get a shell)"
  exit 1
fi

if [ ! -d /sys/firmware/efi/ ]; then
  errorPrint "It seems you did not boot via UEFI. Exiting"
  exit 1
fi

RANDOMNESS=$(cat /proc/sys/kernel/random/entropy_avail)
if [ $RANDOMNESS -lt 256 ]; then
  echo "Not enough entropy in the system. Plug some drives, generate some network traffic, use the keyboard"
  exit 1
fi

read -sp "Choose the passphrase to unlock the root partition: " luksPassword
colorPrint
read -sp "Repeat the passphrase:" luksPasswordConf
colorPrint

if [ "$luksPassword" != "$luksPasswordConf" ]; then
  colorPrint "Passphrases do not match. Treminating"
  exit 1
fi

echo "TODO! Get sops key file and save it in a temporary location so that you can copy it later" && exit 1
tmp_age_keyfile=/run/age.key
cp selected_file $tmp_age_keyfile

prompt_confirm() {
  while true; do
    read -r -p "${1:-Continue?} [y/n]: " reply
    case $reply in
    [yY][eE][sS] | [yY])
      return 0
      ;;
    [nN][oO] | [nN])
      return 1
      ;;
    *) printf " \033[31m %s \n\033[0m" "invalid input" ;;
    esac
  done
}

prompt_confirm "Do you also want to securely wipe the hard disk? This takes quite a long time"
WIPE_DISK=$?

colorPrint "Installing NixOS with host $nixosHostName"

#Select device to format
lsblk
while
  colorPrint "-- Choose the disk to format"
  read disk
  regex="^${disk}\+\s\+[0-9]\+:[0-9]\+\s\+[0-9]\+\s\+[0-9]\+\([,.][0-9]\+\)\?\w\s\+[0-9]\+\s\+disk\b"
  ! (lsblk | grep -q $regex)
do
  colorPrint "-- Invalid disk ($disk)"
  lsblk
done

colorPrint "You are about to choose disk $disk"
colorPrint
lsblk | grep $regex
colorPrint
colorPrint "The contents of the disk are about to be completely erased. Are you sure you want to proceed?"
if ! prompt_confirm; then
  errorPrint "Operation cancelled, terminating"
  exit
fi
colorPrint "Are you ABSOLUTELY sure this is the right one?"
if ! prompt_confirm; then
  errorPrint "Operation cancelled, terminating"
  exit
fi

# These are the partition labels we will use.
# In the configs, we use /dev/disk/by-label/ to refer to these.
# It is therefore important that they are unique across all the
# disks that are connected to the computer.
boot_partition_label="nixboot"
swap_partition_label="nixswap"

partition_labels=($boot_partition_label $swap_partition_label)
readarray -t existing_partitions <<<$(echo -e "$(lsblk -o NAME,PATH,LABEL -n)")
for p in "${existing_partitions[@]}"; do
  #Check if first letter is alfanumeric
  if [[ ${p:0:1} =~ [a-zA-Z0-9] ]]; then
    #p is a disk
    cur_disk_name=$(echo ${p} | awk '{print $1}')
  else
    #p is a partition
    parts_with_disk_names+=("${cur_disk_name} ${p}")
  fi
done
colorPrint "Checking if any existing partition labels conflicts with ours"
for p_lable in "${partition_labels[@]}"; do
  same_label="$(printf "%s\n" "${parts_with_disk_names[@]}" | grep "$p_lable$")"
  #If there was at least one match
  if [ "$same_label" ]; then
    not_on_selected_disk="$(echo "${same_label}" | grep --invert-match "${disk}\b")"
    #If there was at least one match
    if [ "$not_on_selected_disk" ]; then
      errorPrint "One of the partition labels we want to use, '${p_lable}',"
      errorPrint "is already in use on another disk:"
      errorPrint "${not_on_selected_disk}"
      errorPrint ""
      errorPrint "You have to either change the label here and in the configs"
      errorPrint "Or to change the label of the omonimous partition on the other disk."
      errorPrint ""
      errorPrint "You can use the 'lsblk' command to list available partitions and disks."
      exit 1
    fi
  fi
done

colorPrint "--- Hic sunt leones. From here, we start partitioning ---"

#Give the user a little time to ctrl+c
sleep 2

devpath="/dev/${disk}"

colorPrint "Wiping the disk"
wipefs -af $devpath
zpool labelclear -f $devpath
blkdiscard -f $devpath

if $WIPE_DISK; then
  colorPrint "Securely deleting the contents of the disk. This should take a while"
  shred --verbose --random-source=/dev/urandom -n4 $devpath
fi

colorPrint "---                Alea iacta est                     ---"
colorPrint "---             Previous data is gone                 ---"

colorPrint "The disk will be partitioned as follows:"
colorPrint "|----------------------------------------------------------------------------------------|"
colorPrint "| EFI |                                 LUKS encrypted                                   |"
colorPrint "|512MB|                                                                                  |"
colorPrint "|----------------------------------------------------------------------------------------|"
colorPrint "|/////|                                      LVM                                         |"
colorPrint "|/////|                                                                                  |"
colorPrint "|----------------------------------------------------------------------------------------|"
colorPrint "|/////|                                    ZFS pool                                 |swap|"
colorPrint "|/////|                                                                             |8GB |"
colorPrint "|----------------------------------------------------------------------------------------|"
colorPrint "|/////|     /     |                          /nix                                   |////|"
colorPrint "|/////| (amnesic) |                 (store and persistence)                         |////|"
colorPrint "|----------------------------------------------------------------------------------------|"

colorPrint "Partitioning the drive"
#Boot partition
sgdisk --new 1:0:+512MB --typecode 1:ef00 --change-name 1:$boot_partition_label $devpath
#LUKS encrypted partition
sgdisk --new 2:0:0 --typecode 2:8200 --change-name 2:luks $devpath

#Mark the first partition as bootable
parted $devpath -- set 1 esp on

colorPrint "Formatting the EFI partition"
mkfs.fat -F32 -n efi ${devpath}1

colorPrint "Configuring LUKS2"
luksPart=${devpath}2
echo -n $luksPassword | cryptsetup --type luks2 luksFormat $luksPart -d -
colorPrint "LUKS2 has been configured on the root partition."
echo -n $luksPassword | cryptsetup open $luksPart lvmpart -d -
cryptsetup config $luksPart --label cryptroot

colorPrint "Setting up LVM"
pvcreate /dev/mapper/lvmpart
vgcreate vg /dev/mapper/lvmpart
lvcreate -L 8G vg -n swap
lvcreate -l +100%FREE vg -n poolpart

colorPrint "Creating new ZFS pool"
zpool create -f \
  -O mountpoint=none \
  -O compression=lz4 \
  rpool /dev/mapper/vg-poolpart

colorPrint "Creating root dataset (ZFS)"
zfs create -o mountpoint=legacy rpool/root
colorPrint "Mounting root dataset to /mnt"
mount -t zfs rpool/root /mnt

colorPrint "Creating the /nix dataset"
zfs create -o mountpoint=legacy rpool/nix
mkdir /mnt/nix
mount -t zfs rpool/nix /mnt/nix

colorPrint "Mounting efi to /mnt/boot"
mkdir -p /mnt/boot/efi
mount /dev/disk/by-label/$boot_partition_label /mnt/boot

colorPrint "Creating swap"
mkswap -L $swap_partition_label /dev/mapper/vg-swap
colorPrint "Activating swap"
swapon /dev/mapper/vg-swap

rm -r /mnt/etc/nixos/ &>/dev/null

colorPrint "Cloning config"
git clone https://gitlab.com/Giacomo_zanolli/nixos-configuration.git /mnt/etc/nixos

colorPrint "Creating persistent directories"
# This directory will have backup enabled
persistentPath=/nix/persist/safe
mkdir -p /mnt$persistentPath
# This directory won't have backup enabled
mkdir -p /mnt/nix/persist/unsafe

colorPrint "Copying the sops key and setting its permissions"
SOPS_KEYFILE=/mnt/nix/persist/safe/sops_key
mv $tmp_age_keyfile $SOPS_KEYFILE
groupadd -g 1741 age_keyfile
chown root:root $SOPS_KEYFILE
chmod 600 $SOPS_KEYFILE

nixosConfigFolder=$persistentPath/config
colorPrint "Moving config to final destination in ${nixosConfigFolder}"
mntNixosFolder=/mnt$nixosConfigFolder
mkdir -p /mnt$persistentPath
mv /mnt/etc/nixos $mntNixosFolder
chown -R 1000:100 $mntNixosFolder
colorPrint "Creating symlink in /etc"
ln -s $nixosConfigFolder /mnt/etc/nixos
colorPrint "Creating snapshot of blank root"
zfs snapshot rpool/root@blank
colorPrint "Fixing permissions on config (just for the installation)"
# The installer expects the configs to be in /mnt/etc/nixos with no symlinks
# and owned by root (at least, according to my tests so far)
rm /mnt/etc/nixos
cp -r $mntNixosFolder /mnt/etc/nixos
chown -R root:root /mnt/etc/nixos

colorPrint "Load efivars kernel module"
modprobe efivars

bannerPrint "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
bannerPrint "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
bannerPrint "                         ====         :::::::     :::::::             "
bannerPrint "                        ======         :::::::   :::::::              "
bannerPrint "                         ======         ::::::: :::::::               "
bannerPrint "                          ======         :::::::::::::                "
bannerPrint "                           ======         :::::::::::                 "
bannerPrint "                  ======================== :::::::::        =         "
bannerPrint "                 =========================== :::::::       ====       "
bannerPrint "                ============================= :::::::     ======      "
bannerPrint "                          :::::::              :::::::   ======       "
bannerPrint "                         :::::::                ::::::: ======        "
bannerPrint "                        :::::::                  ::::: ======         "
bannerPrint "                       :::::::                    ::: ==============  "
bannerPrint "          ::::::::::::::::::                       : ================ "
bannerPrint "           :::::::::::::::: =                       =================="
bannerPrint "            :::::::::::::: ===                     =======            "
bannerPrint "                  ::::::: =====                   =======             "
bannerPrint "                 :::::::  ======                 =======              "
bannerPrint "                :::::::    ======               =======               "
bannerPrint "                ::::::      ====== :::::::::::::::::::::::::::::      "
bannerPrint "                 ::::        ====== :::::::::::::::::::::::::::       "
bannerPrint "                  ::        ======== :::::::::::::::::::::::::        "
bannerPrint "                           ==========         :::::::                 "
bannerPrint "                          ============         :::::::                "
bannerPrint "                         ======   ======         :::::::              "
bannerPrint "                        ======     ======         :::::::             "
bannerPrint ""
bannerPrint "-------------------------INSTALLING THE OPERATING SYSTEM------------------------"
bannerPrint ""
bannerPrint ""
bannerPrint "          Kindly, do NOT power off the machine nor stop the installation"
bannerPrint ""
bannerPrint ""
bannerPrint "--------------------------------------------------------------------------------"
bannerPrint ""
bannerPrint ""
bannerPrint ""
nixos-install --no-root-password --flake /mnt/etc/nixos#$nixosHostName
installationReturnValue=$?

if [ $installationReturnValue -ne 0 ]; then
  echo "----------Unsuccessful installation----------"
  echo "Exiting"
  exit 1
fi

colorPrint "Backing up LUKS header in $persistentPath/luks_header_cryptroot.backup"
cryptsetup luksHeaderBackup /dev/disk/by-label/cryptroot \
  --header-backup-file $persistentPath/luks_header_cryptroot.backup

colorPrint "Unmounting the target drive, ${disk}"
umount -R /mnt/*
umount -R /mnt
colorPrint "Closing the encrypted partition"
cryptsetup close lvmpart

colorPrint "-----------Everything done! rebooting-----------"

reboot
