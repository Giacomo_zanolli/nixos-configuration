{ ... }:

{
  imports =
    [
      ./modules/swhkd.nix
      ./services/adb.nix
      ./services/dnscrypt-proxy.nix
      ./services/gpg.nix
      #./services/nix-serve.nix
      ./services/restic.nix
      ./services/ssh.nix
      ./services/udisks2.nix
      ./services/virtualisation.nix
      ./boot.nix
      ./display.nix
      ./fonts.nix
      ./hardening.nix
      ./hardware-configuration.nix
      ./impermanence.nix
      ./misc.nix
      ./nix.nix
      ./networking.nix
      ./packages.nix
      ./sops.nix
      ./users.nix
      ./zfs/zfs.nix
    ];

  environment.etc = {
    "systemd/logind.conf".text = ''
      HandlePowerKey=ignore
    '';
  };

  hardware.bluetooth.powerOnBoot = true;
  services.auto-cpufreq.enable = true;
}
