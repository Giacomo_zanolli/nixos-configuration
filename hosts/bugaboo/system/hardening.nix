{ pkgs, ... }:

{
  services.logrotate.enable = true;

  # environment.memoryAllocator.provider = "graphene-hardened";
  security = {
    forcePageTableIsolation = true;
    #   audit.enable = true; # or "lock", but read the docs
    #   auditd.enable = true;
    #   audit.failureMode = "panic";
    #   # tpm2.enable=true
    #   # apparmor.enable=true
    sudo.execWheelOnly = true;
    protectKernelImage = true;
    #allowUserNamespaces = false;
    sudo.wheelNeedsPassword = true;
    isolate.enable = true;
    #   # kernel.printk = "3 3 3 3"; #Only print errors

    #   # TODO: https://madaidans-insecurities.github.io/guides/linux-hardening.html#hidepid
  };

  # boot = {
  #   kernelPackages = pkgs.linuxKernel.packages.linux_hardened;
  #   kernelParams = [
  #     "slab_nomerge"
  #     "init_on_alloc=1"
  #     "init_on_free=1"
  #     "page_alloc.shuffle=1"
  #     "pti=on"
  #     "randomize_kstack_offset=on"
  #     "vsyscall=none"
  #     "debugfs=off"
  #     "oops=panic"
  #     "module.sig_enforce=1"
  #     "lockdown=confidentiality"
  #     "random.trust_cpu=off"
  #     "intel_iommu=on"
  #     "amd_iommu=on"
  #     "efi=disable_early_pci_dma"
  #   ];
  #   kernel.sysctl = {
  #     # Docs on the options on https://www.kernel.org/doc/html/latest/admin-guide/sysctl/kernel.html
  #     # Inspiration https://madaidans-insecurities.github.io/guides/linux-hardening.html#sysctl

  #     "kernel.kptr_restrict" = 2;
  #     "kernel.dmesg_restrict" = 1;
  #     "kernel.kexec_load_disabled" = 1;
  #     "kernel.sysrq" = 4;
  #     "kernel.unprivileged_userns_clone" = 0;
  #     "kernel.unprivileged_bpf_disabled" = 1;
  #     "kernel.yama.ptrace_scope" = 2;

  #     "net.core.bpf_jit_harden" = 2;
  #     "net.ipv4.tcp_rfc1337" = 1;
  #     "net.ipv4.conf.all.rp_filter" = 1;
  #     "net.ipv4.conf.default.rp_filter" = 1;
  #     "net.ipv4.conf.all.accept_redirects" = 0;
  #     "net.ipv4.conf.default.accept_redirects" = 0;
  #     "net.ipv4.conf.all.secure_redirects" = 0;
  #     "net.ipv4.conf.default.secure_redirects" = 0;
  #     "net.ipv4.conf.all.send_redirects" = 0;
  #     "net.ipv4.conf.default.send_redirects" = 0;
  #     "net.ipv4.conf.all.accept_source_route" = 0;
  #     "net.ipv4.conf.default.accept_source_route" = 0;

  #     "net.ipv6.conf.all.accept_source_route" = 0;
  #     "net.ipv6.conf.default.accept_source_route" = 0;
  #     "net.ipv6.conf.all.accept_ra" = 0;
  #     "net.ipv6.conf.default.accept_ra" = 0;
  #     "net.ipv6.conf.all.accept_redirects" = 0;
  #     "net.ipv6.conf.default.accept_redirects" = 0;
  #     "net.ipv6.conf.all.use_tempaddr" = 2;
  #     "net.ipv6.conf.default.use_tempaddr" = 2;

  #     "net.ipv4.tcp_sack" = 0;
  #     "net.ipv4.tcp_dsack" = 0;
  #     "net.ipv4.tcp_fack" = 0;

  #     "net.ipv4.icmp_echo_ignore_all" = 1;

  #     #dev.tty.ldisc_autoload=0    Does this even exist as an option?

  #     "vm.unprivileged_userfaultfd" = 0;
  #     "vm.mmap_rnd_bits" = 32; # These values are for x86 architectures
  #     "vm.mmap_rnd_compat_bits" = 16; # These values are for x86 architectures

  #     "fs.protected_symlinks" = 1;
  #     "fs.protected_hardlinks" = 1;
  #     "fs.protected_fifos" = 2;
  #     "fs.protected_regular" = 2;

  #   };
  #   extraModprobeConfig = ''
  #     install dccp /bin/false
  #     install sctp /bin/false
  #     install rds /bin/false
  #     install tipc /bin/false
  #     install n-hdlc /bin/false
  #     install ax25 /bin/false
  #     install netrom /bin/false
  #     install x25 /bin/false
  #     install rose /bin/false
  #     install decnet /bin/false
  #     install econet /bin/false
  #     install af_802154 /bin/false
  #     install ipx /bin/false
  #     install appletalk /bin/false
  #     install psnap /bin/false
  #     install p8023 /bin/false
  #     install p8022 /bin/false
  #     install can /bin/false
  #     install atm /bin/false

  #     install cramfs /bin/false
  #     install freevxfs /bin/false
  #     install jffs2 /bin/false
  #     install hfs /bin/false
  #     install hfsplus /bin/false
  #     install squashfs /bin/false
  #     install udf /bin/false

  #     install cifs /bin/true
  #     install nfs /bin/true
  #     install nfsv3 /bin/true
  #     install nfsv4 /bin/true
  #     install ksmbd /bin/true
  #     install gfs2 /bin/true

  #     install vivid /bin/false

  #     install bluetooth /bin/false
  #     install btusb /bin/false

  #     install firewire-core /bin/false
  #     install thunderbolt /bin/false
  #   '';
  #   # TODO: SElinux https://madaidans-insecurities.github.io/guides/linux-hardening.html#mac
  # };
}
