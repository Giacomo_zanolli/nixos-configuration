{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.swhkd;
  swhkd = pkgs.swhkd;
in
{
  options.services.swhkd = {
    enable = mkEnableOption "swhkd service";
  };

  config = mkIf cfg.enable {
    boot.kernelModules = [ "uinput" "evdev" ];

    # system.activationScripts.swhkd.stdio.text = "ok";
    system.activationScripts.swhkd = ''
      #Link the pkexec policy file
      mkdir -p /usr/share/polkit-1/actions
      ln -sfn ${swhkd}/share/polkit-1/actions/com.github.swhkd.pkexec.policy /usr/share/polkit-1/actions/com.github.swhkd.pkexec.policy
    '';

    systemd.services.pre_pre_swhks = {
      description = "ensure directory exists for swhks";
      bindsTo = [ "graphical.target" ];
      wantedBy = [ "pre_swhks.service" ];
      script = ''
        echo -n $(id -u) > /tmp/aaaaaaaaaaaaaaaaaaaah
      '';
      serviceConfig = {
        Type = "simple";
        User = config.username;
      };
    };
    systemd.services.pre_swhks = {
      description = "ensure directory exists for swhks";
      bindsTo = [ "graphical.target" ];
      wantedBy = [ "swhks.service" ];
      script = ''
        USER_ID=$(head -n 1 /tmp/aaaaaaaaaaaaaaaaaaaah)
        DIR=/run/user/$USER_ID/
        #Create the folder, make it writable
        mkdir -p $DIR
        chown $USER_ID $DIR
        # Clean up temporary file
        rm /tmp/aaaaaaaaaaaaaaaaaaaah
        echo "And now the directory is there"
      '';
      serviceConfig = {
        Type = "simple";
      };
    };
    systemd.services.swhks = {
      description = "swhks server for swhkd hotkey daemon";
      bindsTo = [ "graphical.target" ];
      wantedBy = [ "swhkd.service" ];
      script = ''
        echo "Launching swhks"
        ${swhkd}/bin/swhks
      '';
      serviceConfig = {
        Type = "simple";
        User = config.username;
        # Group = config.username;
      };
    };
    systemd.services.swhkd = {
      description = "swhkd hotkey daemon";
      bindsTo = [ "graphical.target" ];
      wantedBy = [ "graphical.target" ];
      script = ''
        # echo "---Launching pkttyagent"
        # /run/current-system/sw/bin/pkttyagent --process $$ &
        # # pkttyagent_child=$!
        echo "---Launching swhkd"
        /run/wrappers/bin/pkexec ${swhkd}/bin/swhkd -c /home/${config.username}/.config/swhkd/swhkdrc
      '';
      serviceConfig = {
        Type = "simple";
        User = config.username;
        # Group = config.username;
        #PrivateTmp = "yes";
        #PrivateDevices = "yes";
      };
      environment = {
        # environment.PATH = mkForce (pkgs.linkFarm "pkexec" [
        #   {
        #     name = "pkexec";
        #     path = "${config.security.wrapperDir}/pkexec";
        #   }
        # ]);
        #XDG_RUNTIME_DIR = "/tmp";
        SHELL = "/run/current-system/sw/bin/bash";
        # and /etc/shells has this content
        #   /run/current-system/sw/bin/bash
        #   /run/current-system/sw/bin/sh
        #   /nix/store/0h73sj1n8hzc6fs36cjvsvcvz3av7n47-bash-interactive-5.1-p16/bin/bash
        #   /nix/store/0h73sj1n8hzc6fs36cjvsvcvz3av7n47-bash-interactive-5.1-p16/bin/sh
        #   /bin/sh
        #   /usr/bin/env bash
        #   /etc/profiles/per-user/civts/bin/zsh
        #   /nix/store/q8w9rjszh8lz9csm0kqfrdgs63x13c10-home-manager-path/bin/zsh
      };
    };
    # environment.systemPackages = with pkgs; [
    #   libsForQt5.polkit-kde-agent
    # ];
  };
}
