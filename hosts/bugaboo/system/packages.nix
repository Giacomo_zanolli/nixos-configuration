{ config, flakeInputs, pkgs, ... }:

{
  environment.systemPackages =
    let
      deploy-rs = flakeInputs.deploy-rs.defaultPackage.${pkgs.system};
    in
    with pkgs; [
      git
      unzip
      zip
      wget
      dig
      #pciutils #contains lspci and others
      btop
      cpulimit
      killall
      neofetch

      sshfs #mount other filesystems over ssh
      fuse
      ntfs3g
    ];

  programs.sysdig = {
    enable = true;
  };

}
