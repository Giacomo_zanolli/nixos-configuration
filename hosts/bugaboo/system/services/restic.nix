{ config, lib, pkgs, ... }:

let
  # The path to backup (must end with '/')
  path = "/nix/persist/safe/";
  # Where to create the backup(s)
  destinations = [
    { name = "offsite"; hasEnvFile = true; }
  ];

  destinationToConfig = { name, hasEnvFile }:
    let
      includeFile = "/run/restic_includes_${name}.txt";
      progressFile = "/run/restic_status_${name}";
      envPath = ../../../../secrets/bugaboo/restic + "/${name}.env";
    in
    {
      services.restic.backups = {
        ${name} = {
          repositoryFile = config.sops.secrets."restic-repo-${name}".path;
          passwordFile = config.sops.secrets."restic-password-${name}".path;
          initialize = true;
          extraBackupArgs = [
            "--files-from-verbatim ${includeFile}"
            # Persist this directory for better performance
            # See https://restic.readthedocs.io/en/latest/manual_rest.html#caching
            "--cache-dir /var/cache/restic"
          ];
          paths = [ " " ]; # This is a bit hacky, but works for now
          backupPrepareCommand = ''
            rm -f ${progressFile}
            touch ${progressFile}
            chmod 644 ${progressFile}

            echo -n "Preparing" > ${progressFile}

            rm -f ${includeFile}
            cd ${path}

            echo "Generating the list of files to include (excluding .gitignored)"
            ${pkgs.ripgrep}/bin/rg --files --hidden > ${includeFile}

            echo "Escaping the prefix ${path} so it can be used in sed"
            PREFIX="${path}"
            # Note: the order is important
            PREFIX=$(echo "$PREFIX" | sed 's/\\/\\\\/g')
            PREFIX=$(echo "$PREFIX" | sed 's/\//\\\//g')
            PREFIX=$(echo "$PREFIX" | sed 's/\&/\\\&/g')

            echo "Prepending each line with the prefix, so we get the absolute path"
            sed --in-place "s/^/$PREFIX/" ${includeFile}

            echo -n "Running" > ${progressFile}
            echo "Ready to start the backup"
          '';
          backupCleanupCommand = ''
            echo "Backup complete, removing temporary include file"
            rm ${includeFile}
            echo -n "Done" > ${progressFile}
          '';
          extraOptions = [
            #This is a good idea if I was using sftp (having a dedicated remote user),
            #but I am using the rest server so I can tell it to be append-only)
            #"sftp.command='ssh backup@cetus -i /etc/nixos/secrets/backup-private-key -s sftp'"
          ];
        };
      };

      systemd.timers."restic-backups-${name}" = lib.mkForce {
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = "*-*-* *:00:00"; #every hour
          RandomizedDelaySec = "15m";
          Unit = "restic-backups-${name}.service";
        };
      };

      systemd.services."restic-backups-${name}" = {
        serviceConfig =
          let
            envSecretFile = config.sops.secrets."restic-env-${name}".path;
          in
          {
            EnvironmentFile =
              if hasEnvFile
              then envSecretFile
              else null;
            # #See man systemd.resource-control.5
            CPUAccounting = true;
            MemoryAccounting = true;
            CPUQuota = "50%";
            MemoryMax = "12%";
            IOWeight = 50;
          };
        onFailure = [ "restic-backups-${name}-failed.service" ];
      };

      systemd.services."restic-backups-${name}-failed" = {
        script = ''
          echo -n "Failed" > ${progressFile}
        '';
      };

      sops.secrets = {
        "restic-password-${name}" = {
          key = "${name}-password";
          sopsFile = ../../../../secrets/bugaboo/restic/repos.yaml;
        };
        "restic-repo-${name}" = {
          key = "${name}-url";
          sopsFile = ../../../../secrets/bugaboo/restic/repos.yaml;
        };
        "restic-env-${name}" = lib.mkIf hasEnvFile {
          format = "dotenv";
          sopsFile = envPath;
        };
      };
    };

  # https://stackoverflow.com/a/54505212
  recursiveMerge = attrList:
    let
      f = with lib; attrPath:
        zipAttrsWith (n: values:
          if tail values == [ ]
          then head values
          else if all isList values
          then unique (concatLists values)
          else if all isAttrs values
          then f (attrPath ++ [ n ]) values
          else last values
        );
    in
    f [ ] attrList;

in
recursiveMerge (map destinationToConfig destinations)
