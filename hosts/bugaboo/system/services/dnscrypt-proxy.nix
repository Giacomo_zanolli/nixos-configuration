{ config, ... }:

{
  services.dnscrypt-proxy2 = {
    enable = true;
    upstreamDefaults = false;
    settings = rec {
      ipv4_servers = true;
      ipv6_servers = false;
      dnscrypt_servers = true;
      doh_servers = true;
      timeout = 5000;
      keepalive = 30;
      cert_refresh_delay = 120;
      bootstrap_resolvers = [
        "9.9.9.9:53"
        "1.1.1.1:53"
        "8.8.8.8:53"
      ];
      ignore_system_dns = true;
      log_files_max_size = 10; #MB
      log_files_max_age = 7; #days
      block_undelegated = true;

      cache = true;
      cache_size = 4096;
      cache_min_ttl = 2400;
      cache_max_ttl = 86400;
      cache_neg_min_ttl = 60;
      cache_neg_max_ttl = 600;
      static = {
        # These DNS stamps contain the info to reach the server.
        # The specification is here https://dnscrypt.info/stamps-specifications
        # To view and edit them, use https://dnscrypt.info/stamps (this page 
        # also works when accessed from archive.org)
        #
        # Example configs can be found in the DNScrypt public resolvers list
        # https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md

        # 76.76.2.5
        "controld-DoH-1".stamp = "sdns://AgcAAAAAAAAACTc2Ljc2LjIuNQAUZnJlZWRucy5jb250cm9sZC5jb20LL3VuY2Vuc29yZWQ";
        # 76.76.10.5
        "controld-DoH-2".stamp = "sdns://AgcAAAAAAAAACjc2Ljc2LjEwLjUAFGZyZWVkbnMuY29udHJvbGQuY29tCy91bmNlbnNvcmVk";
        # 2606:1a40::5
        "controld-DoH-ip6-1".stamp = "sdns://AgcAAAAAAAAADlsyNjA2OjFhNDA6OjVdABRmcmVlZG5zLmNvbnRyb2xkLmNvbQsvdW5jZW5zb3JlZA";
        # 2606:1a40:1::5
        "controld-DoH-ip6-2".stamp = "sdns://AgcAAAAAAAAAEFsyNjA2OjFhNDA6MTo6NV0AFGZyZWVkbnMuY29udHJvbGQuY29tCy91bmNlbnNvcmVk";

        # 9.9.9.9
        "quad9-DoH-1".stamp = "sdns://AgcAAAAAAAAABzkuOS45LjkADWRucy5xdWFkOS5uZXQKL2Rucy1xdWVyeQ";
        # 149.112.112.112
        "quad9-DoH-2".stamp = "sdns://AgcAAAAAAAAADzE0OS4xMTIuMTEyLjExMgANZG5zLnF1YWQ5Lm5ldAovZG5zLXF1ZXJ5";
        # 2620:fe::fe
        "quad9-DoH-ip6-1".stamp = "sdns://AgcAAAAAAAAADVsyNjIwOmZlOjpmZV0ADWRucy5xdWFkOS5uZXQKL2Rucy1xdWVyeQ";
        # 2620:fe::9
        "quad9-DoH-ip6-2".stamp = "sdns://AgcAAAAAAAAADFsyNjIwOmZlOjo5XQANZG5zLnF1YWQ5Lm5ldAovZG5zLXF1ZXJ5";
      };
      # Which servers to use
      server_names = builtins.attrNames static;
      # The following are self-reported in each DNS stamp.
      # Still, better than nothing.
      require_dnssec = true;
      require_nolog = true;
      require_nofilter = true;
    };
  };

  # Needed since it is searched by resolvconf while running wg-quick
  systemd.services.dnscrypt-proxy2 = {
    aliases = [ "dbus-org.freedesktop.resolve1.service" ];
  };

  # Avoid having another DNS resolver running
  services.resolved.enable = false;
  services.stubby.enable = false;

  # Do not use the DNS server provided by DHCP
  networking.dhcpcd.extraConfig = "nohook resolv.conf";
}
