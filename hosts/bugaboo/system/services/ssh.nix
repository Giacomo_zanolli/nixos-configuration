{ config, pkgs, ... }:

{
  # Disable the OpenSSH daemon.
  services.openssh.enable = false;
}
