{ pkgs, config, ... }:

{
  #Android debug bridge
  programs.adb.enable = true;

  users.users.${config.username}.extraGroups = [ "adbusers" ];
}
