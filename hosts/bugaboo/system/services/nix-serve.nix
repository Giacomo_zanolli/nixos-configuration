{ config, flakeInputs, pkgs, ... }:

{
  # Serve the /nix/store of this machine as a binary cache on the network
  # (you still need to sign the outputs for others to trust them)
  services.nix-serve = {
    port = 28398;
    enable = true;
    secretKeyFile = config.sops.secrets.nixcache.path;
    # Until https://github.com/NixOS/nix/issues/7704 gets solved 
    # nix v2.12 affected by CVE-2024-27297
    package = pkgs.nix-serve.override {
      nix = flakeInputs.nixpkgs.legacyPackages.x86_64-linux.nixVersions.nix_2_12;
    };
  };
}
