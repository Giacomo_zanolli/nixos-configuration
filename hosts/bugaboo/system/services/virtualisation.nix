{ config, pkgs, ... }:

{
  virtualisation = {
    libvirtd.enable = true;
    spiceUSBRedirection.enable = true; #Allows USB passthrough to VMs
    docker = {
      enable = true;
      # autoPrune.enable = true;
      # enableOnBoot = false;
    };
  };
  programs.dconf.enable = true;
  environment.systemPackages = with pkgs; [
    virt-manager #GUI for managing the VMs
    virtiofsd #Needed to share directories between the host and the guest OS in the VMs
  ];
  users.users.${config.username}.extraGroups = [ "libvirtd" "docker" ];
}
