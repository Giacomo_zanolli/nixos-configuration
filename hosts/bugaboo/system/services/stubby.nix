{ ... }:

{
  # DNS over TLS (DoT)
  services.stubby = {
    enable = true;
    settings = {
      # These settings are basically from the default ones
      # github.com/getdnsapi/stubby/blob/develop/stubby.yml.example
      log_level = "GETDNS_LOG_NOTICE";
      resolution_type = "GETDNS_RESOLUTION_STUB";
      tls_authentication = "GETDNS_AUTHENTICATION_REQUIRED";
      dns_transport_list = [ "GETDNS_TRANSPORT_TLS" ];
      tls_query_padding_blocksize = 128;
      edns_client_subnet_private = 1;
      round_robin_upstreams = 1;
      idle_timeout = 10000;
      limit_outstanding_queries = 100;
      listen_addresses = [
        "127.0.0.1"
        # due to restrictions within the config file parser,
        # IPv6 address cannot start with `::`
        "0::1"
      ];
    } // {
      upstream_recursive_servers = [
        {
          address_data = "2606:1a40::5";
          tls_auth_name = "uncensored.freedns.controld.com";
        }
        {
          address_data = "2606:1a40:1::5";
          tls_auth_name = "uncensored.freedns.controld.com";
        }
        {
          address_data = "76.76.2.5";
          tls_auth_name = "uncensored.freedns.controld.com";
        }
        {
          address_data = "76.76.10.5";
          tls_auth_name = "uncensored.freedns.controld.com";
        }
      ];
    };
  };

  # Avoid having another DNS resolver running
  services.resolved.enable = false;
  services.dnscrypt-proxy2.enable = false;

  # Do not use the DNS server provided by DHCP
  networking.dhcpcd.extraConfig = "nohook resolv.conf";
}
