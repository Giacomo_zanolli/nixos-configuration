{ config, ... }:

{
  services.clamav = {
    daemon.enable = true;
    updater.enable = true;
  };

  systemd.services.clamav-freshclam = {
    wantedBy = [ "clamav-daemon.service" ];
    before = [ "clamav-daemon.service" ];
  };
}
