{ config, lib, pkgs, modulesPath, ... }:

{
  fileSystems = {
    "/" = {
      device = "rpool/root";
      fsType = "zfs";
    };
    "/boot" = {
      device = "/dev/disk/by-label/efi";
      fsType = "vfat";
      options = [ "nodev" "noatime" ];
    };
    "/nix" = {
      device = "rpool/nix";
      fsType = "zfs";
      options = [ "nodev" "noatime" ];
    };
  };

  swapDevices = [
    {
      device = "/dev/mapper/vg-swap";
      # encrypted = {
      #   enable = true;
      #   label = "swap";
      #   blkDev = "/dev/disk/by-label/cryptswap";
      # };
      #If enabled, can not hibernate. See options manual for more info
      #randomEncryption.enable = true;
    }
  ];
  hardware.enableRedistributableFirmware = true;
  hardware.cpu.intel.updateMicrocode = true;
}
