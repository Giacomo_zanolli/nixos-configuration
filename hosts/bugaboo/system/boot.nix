{ config, pkgs, lib, ... }:

{
  #Legacy boot
  # # Use the GRUB 2 boot loader.
  # boot.loader.grub.enable = true;
  # boot.loader.grub.version = 2;
  # boot.loader.grub.device = "/dev/sda";

  # #UEFI boot with systemd-boot
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;

  #UEFI boot with grub2
  boot = {
    tmp.cleanOnBoot = true;
    kernelParams = [
      "quiet"
      "udev.log_level=3"
      "splash"
      "boot.shell_on_fail" #https://discourse.nixos.org/t/12304#post_2
    ];
    kernelModules = [
      "kvm-intel"
      "amdgpu"
      "fbdev"
    ];

    # kernelPackages = pkgs.linuxPackages_latest;
    # kernelPackages = pkgs.linuxPackages_5_19;
    consoleLogLevel = 0;
    loader = {
      efi.canTouchEfiVariables = true;
      timeout = 10;
      grub =
        let
          amberTheme = pkgs.fetchFromGitHub {
            #Downloads https://github.com/<owner>/<repo>/archive/<rev>.tar.gz
            owner = "Jacksaur";
            repo = "CRT-Amber-GRUB-Theme";
            rev = "1db01fb6f87e4b93ba333c89fe3dfd229184820b";
            sha256 = "6v2S6ZnsagxAx3EN8Z3lzIhyKwsOCuV3dsqsl8Cn/nQ=";
          };
        in
        {
          enable = true;
          useOSProber = false;
          efiSupport = true;
          device = "nodev";
          theme = amberTheme;
          splashImage = amberTheme + "/background.png";
          configurationLimit = 50; #How many old NixOS generations to store
          #users.${config.username}.hashedPassword = "grub.pbkdf2.sha512.10000.3AABFDE6AFC6607BB92C6B1EF92A301C80C0DADBC2996E6090111DEB1471F7945DC5E760D27EE77F9BDE80B5D8D2C7CAF11D423A30AF0D31350F0EFC432091A6.FA8F9C94847B142202F3EB991017D1FA9AD50B63D33708B91A442985FD3E915687058AF892833AFC1863B7E5C636278BE90AD0373D87211AE25230C03D4DEB8B";
        };
    };
    initrd = {
      availableKernelModules = [
        "xhci_pci"
        "ahci"
        "usb_storage"
        "rtsx_usb_sdmmc"
        #optional?
        "ata_piix"
        "uhci_hcd"
        "ehci_pci"
        "sd_mod"
        "sr_mod"
      ];
      kernelModules = [ "dm-snapshot" "dm-cache" ];
      luks.devices."cryptroot" = {
        device = "/dev/disk/by-label/cryptroot";
        preLVM = true;
      };
    };
    plymouth = {
      enable = false;
      themePackages = builtins.attrValues pkgs.plymouth_themes;
      theme = "deus_ex";
    };
    binfmt.emulatedSystems = [ "aarch64-linux" ]; #Allows to cross-compile for raspberry
  };
}
