{ config, pkgs, ... }:

{
  users =
    {
      users =
        let
          username = config.username;
        in
        {
          ${username} = {
            isNormalUser = true;
            uid = 1000;
            name = username;
            hashedPasswordFile = config.sops.secrets."${username}-passwd".path;
            extraGroups = [
              "wheel"
              "network_manager"
              # "keys" #Needed to be able to access sops secrets
            ];
          };
          root = {
            #This renders password authentication impossible for the root user
            #see https://discourse.nixos.org/t/13235
            hashedPassword = "!";
          };
        };
      mutableUsers = false;
    };
}
