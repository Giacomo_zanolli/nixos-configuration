{ config, pkgs, ... }:

{
  fonts = {
    fontDir.enable = true;
    packages = with pkgs;[
      inconsolata
      dejavu_fonts
      ubuntu_font_family
      source-code-pro
      source-sans-pro
      source-serif-pro
      overpass
      libre-baskerville
      julia-mono
      hack-font
      comic-relief
    ];
  };
}
