{ lib, ... }:
{
  networking.hostId = "2c871e6a";
  boot = {
    initrd.postMountCommands =
      let
        scriptContents = builtins.readFile ./boot_script.sh;
      in
      lib.mkAfter scriptContents;
    supportedFilesystems.zfs = true;
    zfs.allowHibernation = true; #Set to true IF AND ONLY IF swap is NOT on ZFS
    zfs.forceImportRoot = false;
  };
}
