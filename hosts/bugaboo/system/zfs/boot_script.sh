#! /usr/bin/env sh

# #Rolling snapshot example https://openzfs.github.io/openzfs-docs/man/8/zfs-rename.8.html
# pool=rpool/root
# zfs destroy -r $pool@7daysago
# zfs rename -r $pool@6daysago @7daysago
# zfs rename -r $pool@5daysago @6daysago
# zfs rename -r $pool@4daysago @5daysago
# zfs rename -r $pool@3daysago @4daysago
# zfs rename -r $pool@2daysago @3daysago
# zfs rename -r $pool@yesterday @2daysago
# zfs rename -r $pool@today @yesterday
# zfs snapshot -r $pool@today

#clone/promote https://openzfs.github.io/openzfs-docs/man/8/zfs-rename.8.html
#-p1=rpool/root
#-p2=rpool/cartago
#-p3=pool/project/legacy
#Make snaphot of the system (blank)
#-zfs create $p1
# -------- populate /$p1 with data
#Make dirty snapshot
#-zfs snapshot $p1@saveme
#Clone it in p2
#-zfs clone $p1@saveme $p2
#Remove p2 (dirty) dependency from p1 (blank)
#-zfs promote $p2
# #Restore changes from p2 (dirty) into
# DO NOT USE THIS, else you destroy the blank snapshot!
# zfs rename $p1 $p3
# zfs rename $p2 $p1
# #once the legacy version is no longer needed, it can be destroyed
# zfs destroy $p3

# #https://unix.stackexchange.com/questions/619931
# #https://stackoverflow.com/questions/40659016
# echo "Delete the snapshot"
# zfs destroy rpool/root@cartago || true
# echo "Create the snapshot"
#-zfs snapshot rpool/root@cartago
#-zfs clone rpool/root@cartago
# echo "List the snapshots"
# zfs list -t snapshot
# #zfs snapshot rpool/root@cartago_$(date -d"1 January 2022 12:00 AM" +%s)
if zfs rollback -r rpool/root@blank; then
  echo "It's a new day for /"
else
  echo "/ was NOT RESTORED!! Be careful in there"
  sleep 3
fi
# echo "List the snapshots"
# zfs list -t snapshot
# sleep 10
