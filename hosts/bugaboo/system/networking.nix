{ config, pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    cacert
  ];

  networking = {
    hostName = config.hostname;
    useDHCP = true;
    wireless = {
      enable = true;
      userControlled.enable = true;
      secretsFile = config.sops.secrets.networking.path;
      networks = {
        Starlink = {
          pskRaw = "ext:psk_raw_starlink";
          priority = 40;
          extraConfig = ''
            #This line tells to connect on 5Ghz channel 56 (5.28Ghz), which is the one
            #I set on the access point.
            #See https://askubuntu.com/questions/1058622 for other frequencies (or just 
            #multiply by 1000 the frequency and add the ones +- the channel width)
          
            #freq_list=5250 5260 5270 5280 5290 5300 5310
          '';
          # see option documentation for networking.wireless.environmentFile for sops
        };

        MYChevrolet = {
          pskRaw = "ext:psk_mychevrolet";
          priority = 50;
        };
      };
    };

    firewall = {
      enable = true;
      pingLimit = "--limit 10/minute --limit-burst 5";
      allowedTCPPorts = [
        # 22
        # 80
        # 443
        # If nix-serve is enabled, open its port
        (with config.services.nix-serve; lib.mkIf enable port)
      ];
      #allowedUDPPorts = [ ... ];
    };
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    #For changing/spoofing MAC see https://nixos.wiki/wiki/Wpa_supplicant

    hosts = {
      #"192.168.1.4" = [ "cetus" "cetus.home" ];
    };
  };
}
