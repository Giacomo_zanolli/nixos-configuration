{ config, ... }:

{
  sops = {
    defaultSopsFile = ../../../.sops.yaml;

    age = {
      keyFile = "/nix/persist/safe/sops_key";
      generateKey = false;
      #See https://github.com/Mic92/sops-nix/issues/167
      sshKeyPaths = [ ];
    };

    #See https://github.com/Mic92/sops-nix/issues/167
    gnupg.sshKeyPaths = [ ];

    secrets = let username = config.username; in {
      "${username}-passwd" = {
        neededForUsers = true;
        key = "${username}-passwd";
        sopsFile = ../../../secrets/bugaboo/users.yaml;
      };

      networking = {
        format = "dotenv";
        sopsFile = ../../../secrets/bugaboo/networking.env;
      };

      nixcache = {
        format = "binary";
        sopsFile = ../../../secrets/bugaboo/cache-priv-key.pem;
      };

      openvpn = {
        key = "openvpn-credentials";
        sopsFile = ../../../secrets/bugaboo/vpn.yaml;
      };
    };
  };
}
