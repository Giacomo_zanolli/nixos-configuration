{ config, lib, ... }: {
  environment.persistence =
    let
      safe = "/nix/persist/safe";
      unsafe = "/nix/persist/unsafe";
    in
    {
      ${safe} = {
        users.${config.username} = {
          directories = [
            "documents"
            "notes"
            "pictures"
            "projects"
            "work"
            ".android"
            { directory = ".gnupg"; mode = "0700"; }
            { directory = ".ssh"; mode = "0700"; }
            ".local/share/cinny"
            ".local/share/in.cinny.app"
          ];
          files = [
            ".rcloneignore"
            ".zsh_history"
          ];
        };
        hideMounts = true;
      };

      ${unsafe} = {
        users.${config.username} = {
          directories = [
            ".cache/BraveSoftware"
            ".cache/keepassxc"
            ".cargo"
            ".config/BraveSoftware"
            ".config/Logseq"
            ".config/Signal"
            ".config/keepassxc"
            ".gradle"
            ".local/share/direnv"
            ".logseq"
            "downloads"
            "movies"
          ];
        };
        directories = [
          "/var/lib/docker"
          "/var/lib/bluetooth"
          "/var/cache/restic"
          "/var/cache/restic-backups-offsite-2"
          "/root/.cache/restic"
          "/var/lib/nixos"
          (lib.mkIf config.services.clamav.daemon.enable config.services.clamav.updater.settings.DatabaseDirectory)
        ];
        hideMounts = true;
      };
    };
}
