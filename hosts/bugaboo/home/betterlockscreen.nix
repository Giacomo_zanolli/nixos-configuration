{ pkgs, config, ... }:

{
  home-manager.users.${config.username}.home.packages = with pkgs; [
    betterlockscreen
  ];
  systemd.services = {
    "lock" = {
      enable = true;
      description = "Locks X session with betterlockscreen";
      serviceConfig = {
        User = config.username;
        #ExecStart = "/etc/profiles/per-user/${config.username}/bin/betterlockscreen --lock";
        ExecStart = "${pkgs.betterlockscreen}/bin/betterlockscreen --lock";
        ExecStartPost = "/run/current-system/sw/bin/sleep 1"; #Ensure screen is locked
        Environment = "DISPLAY=:0";
      };
      before = [ "sleep.target" ];
      wantedBy = [ "sleep.target" ];
    };
  };
}
