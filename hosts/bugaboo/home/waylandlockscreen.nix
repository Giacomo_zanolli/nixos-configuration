{ pkgs, config, ... }:

{
  home-manager.users.${config.username}.home.packages = with pkgs; [
    swaylock
  ];
  systemd.services = {
    "lock-screen" = {
      description = "Locks wayland session with swaylock";
      script = ''
        echo "Deciding which image to show"
        IMG=$(ls ~/.wallpapers/lockscreen | sort -R | tail -1)
        
        echo "Locking the screen"
        ${pkgs.swaylock}/bin/swaylock -i ~/.wallpapers/lockscreen/$IMG -f -s fill --color 000000 -F -e
      '';
      # Adds a delay before the suspend.target to ensure the lockscreen has had
      # time to display properly.
      postStart = "sleep 2";
      serviceConfig = {
        Type = "forking";
        User = config.username;
      };
      environment = {
        SHELL = "/run/current-system/sw/bin/bash";
        XDG_RUNTIME_DIR =
          let
            uid = builtins.toString config.users.users.${config.username}.uid;
          in
          "/run/user/${uid}";
        WAYLAND_DISPLAY = "wayland-1";
      };
      before = [ "sleep.target" ];
      wantedBy = [ "suspend.target" ];
    };
  };
}
