#
{ config, pkgs, ... }:

{
  environment.pathsToLink = [ "/share/zsh" ];
  users.users.${config.username}.shell = pkgs.zsh;
  programs.zsh.enable = true;
  home-manager.users.${config.username} = {
    programs.zsh = {
      enable = true;
      autosuggestion.enable = true;
      syntaxHighlighting.enable = true;
      dotDir = ".config/zsh";
      shellAliases = {
        ll = "ls -l";
      };
      history = {
        size = 40000;
        ignoreSpace = false;
      };
    };
  };
}
