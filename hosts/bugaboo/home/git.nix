{ pkgs, ... }: {
  programs.git = {
    enable = true;
    userName = "Giacomo Zanolli";
    userEmail = "giacozanolli+gitlab@gmail.com";
    signing = {
      key = "E78812EE21BB74A007334233A4C83AFE1EEB9385";
      signByDefault = true;
    };
    difftastic.enable = true;
    attributes = [ "*.pdf diff=pdf" ];
    extraConfig = {
      init.defaultBranch = "master";
      tag.gpgSign = true;
    };
  };
}
