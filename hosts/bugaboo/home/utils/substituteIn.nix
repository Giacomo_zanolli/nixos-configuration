/* SubstituteIn allows to easily replace/interpolate multiple values in a file.
  This is particularly useful for theming or applying configurations that depend
  on variables available in nix.

  Example:
  You have a file with the following content
  ```
  set recolor true
  set recolor-keephue true

  set default-bg            "${bg}" #Background
  set default-fg            "${fg}" #Foreground
  ```

  You could have something like this

  `home-manager.users.username.home.file."./config/zathura/zathurarc".text=`
  `  substituteIn {`
  `    file = ./path/to/file;`
  `    substitutions = {`
  `      bg = "#000000";`
  `      fg = "#ffffff";`
  `    };`
  `  };`

  , then it produces a file in which ${bg} and
  ${fg} have been properly substituted.

  By default, the interpolation syntax is "${variableName}", but this can
  be changed using the substitutionPrefix and substitutionSuffix arguments.

  For example, if substitutionPrefix is set to "[" and substitutionSuffix
  is set to "]", then only values enclosed in square brackets that correspond
  to an attribute defined in substitutions are substituted.
*/

{ substitutions, file, substitutionPrefix ? "\${", substitutionSuffix ? "}" }:
with builtins;
let
  #List with all the members of the set 'substitutions' in alphabetical order
  subNames = attrNames substitutions;
  #List with all the values of the set 'substitutions' in the same order as the attribute names
  subValues = attrValues substitutions;
  #List with the actual strings we need to find in the text to replace them
  subNamesPatterns = map (x: substitutionPrefix + x + substitutionSuffix) subNames;
  #String with the contents of the file on the path of the variable 'file'
  fileContent = readFile file;
in
#In the string with the contents of the file, replace each pattern with the corresponding value
replaceStrings subNamesPatterns subValues fileContent
