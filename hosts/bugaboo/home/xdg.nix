{ ... }:

{
  xdg = {
    mime.enable = true;
    mimeApps = {
      enable = true;
      defaultApplications = {
        #Documents
        "application/pdf" = [ "org.pwmt.zathura.desktop" "brave-browser.desktop" ];
        #Images
        "image/png" = [ "org.kde.gwenview.desktop" "feh.desktop" ];
        "image/jpeg" = [ "org.kde.gwenview.desktop" "feh.desktop" ];
        "image/svg+xml" = [ "org.kde.gwenview.desktop" ];
        "image/gif" = [ "org.kde.gwenview.desktop" ];
        #Text
        "application/json" = [ "org.kde.kwrite.desktop" ];
        "text/plain" = [ "org.kde.kwrite.desktop" ];
        #Videos
        "video/mpeg" = [ "mpv.desktop" ];
        #Directories
        "inode/directory" = [ "pcmanfm.desktop" ];
        #Web
        "text/html" = [ "brave-browser.desktop" ];
        "x-scheme-handler/http" = [ "brave-browser.desktop" ];
        "x-scheme-handler/https" = [ "brave-browser.desktop" ];
        #Other
        "x-scheme-handler/unknown" = [ "org.kde.kwrite.desktop" "brave-browser.desktop" ];
        "x-scheme-handler/about" = [ "org.kde.kwrite.desktop" "brave-browser.desktop" ];
      };
    };

    desktopEntries = {
      feh = {
        name = "Feh";
        comment = "A fast and light image viewer";
        genericName = "Image viewer";
        exec = "feh --auto-zoom --scale-down --start-at %U";
        terminal = false;
        categories = [ "Office" "Viewer" ];
        mimeType = [ "image/jpeg" "image/png" "image/svg+xml" ];
        type = "Application";
        noDisplay = true;
      };
    };
  };
  #see also xresources.extraConfig and xresources.properties for theming
}
