{ fetchurl }:
let base = ".wallpapers"; desktop_dir = "${base}/desktop"; lockscreen_dir = "${base}/lockscreen"; in
{
  desktop_bg = {
    classic = {
      "${desktop_dir}/1.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/UbZ4uRFX-kQ/download"
          "https://web.archive.org/web/20221112211541if_/https://images.unsplash.com/photo-1651336015997-8d0b866121b3"
          # Here is where I'd put another backup URL... if I had one
        ];
        name = "UbZ4uRFX-kQ.jpg";
        sha256 = "0nmhp3bfdw9drksmcy2b2jnmf8v8d040p2w49xy11w5kyb4qdnry";
      };
      "${desktop_dir}/2.jpg".source = fetchurl {
        urls = [
          "https://i.imgur.com/7yekXlf.jpg"
          "https://web.archive.org/web/20221112211807if_/https://i.imgur.com/7yekXlf.jpg"
        ];
        name = "yt_kdZYhfHYHtY.jpg";
        sha256 = "sha256-tSt9m+O1Ew45rx7VZZXVym7XVgkMN0VcOyc1dQGD6Mk=";
      };
      "${desktop_dir}/3.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/Pj3RGcWQveA/download"
          "https://web.archive.org/web/20220813032904if_/https://images.unsplash.com/photo-1619103233774-8e23c97d7dd1"
          # Here is where I'd put another backup URL... if I had one
        ];
        name = "Pj3RGcWQveA.jpg";
        sha256 = "sha256-F+MAgZ32gRYKyyRjmVtu3DMiIBox1/fHtkeNmcIm6Bg=";
      };
      "${desktop_dir}/4.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/MhvrEJSNDkA/download"
          "https://web.archive.org/web/20230122201526im_/https://images.unsplash.com/photo-1673263229991-e966a4a06b7a"
        ];
        name = "MhvrEJSNDkA.jpg";
        sha256 = "sha256-x8m8E6v8KJn+Yr90Lpmv43p0PJMxUsPbwjJNlqAkQoQ=";
      };
      "${desktop_dir}/5.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/PnJehKB3U8w/download"
          "https://web.archive.org/web/20230131091818/https://images.unsplash.com/photo-1673263229990-a70c8a128c3d"
        ];
        name = "PnJehKB3U8w.jpg";
        sha256 = "sha256-34OxTxECzf63MBamfxwSwLquvoauNIxc1qEq9Mvm8Xw=";
      };
      "${desktop_dir}/6.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/zLHdaumntAo/download"
          "https://web.archive.org/web/20230131091639/https://images.unsplash.com/photo-1610028711738-0593b2110c7b"
        ];
        name = "zLHdaumntAo.jpg";
        sha256 = "sha256-RCIe1h0hCPJ7A1Kk3jTI/r1wubMWa3BufeGsUUVQwHk=";
      };
      "${desktop_dir}/7.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/FrlCwXwbwkk/download"
          "https://web.archive.org/web/20230205235243if_/https://images.unsplash.com/photo-1488831861984-179da3647265"
        ];
        name = "FrlCwXwbwkk.jpg";
        sha256 = "sha256-Jvi3yDpAheEwEI+BOqvVRjGdaOaXaGsYsLSa7Wzh7UQ=";
      };
      "${desktop_dir}/8.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/6DtlyJna_Tw/download"
          "https://web.archive.org/web/20231224165156/https://images.unsplash.com/photo-1703016445159-2d933b1196f5"

        ];
        name = "6DtlyJna_Tw.jpg";
        sha256 = "sha256-xJmxrVeOOueNdFhRN2oV5SboVkH4hqEisjGDe284eF8=";
      };
      "${desktop_dir}/9.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/N8ucHGuZtUs/download"
        ];
        name = "N8ucHGuZtUs.jpg";
        sha256 = "sha256-O+rMmiwj8wA6/G7N7kvg8osyx+1WSLUw/gCtDKVXblk=";
      };
      "${desktop_dir}/10.jpg".source = fetchurl {
        urls = [
          "https://images.pexels.com/photos/13205711/pexels-photo-13205711.jpeg"
        ];
        name = "13205711.jpg";
        sha256 = "sha256-Sza3tovxa3B1V+s/BoPwMnvj2jjTXli+OUSbhPJEQmk=";
      };
      "${desktop_dir}/11.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/xiozl0b3ztA/download"
          "https://web.archive.org/web/20240917104913/https://unsplash.com/photos/xiozl0b3ztA/download"
        ];
        name = "xiozl0b3ztA.jpg";
        sha256 = "sha256-bqg3ukc/lbP4rdR82+Sxqv8S0tGmG4jQ6ENce/MVYTY=";
      };
      "${desktop_dir}/12.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/kJiVF5HTobs/download"
          "https://web.archive.org/web/20240917121433/https://unsplash.com/photos/kJiVF5HTobs/download"
        ];
        name = "kJiVF5HTobs.jpg";
        sha256 = "sha256-MkInHRJMBXLRHnDrTzHDXSGrknrN3UjZhwL51auzbf8=";
      };
      "${desktop_dir}/13.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/5S4ciibnbZo/download"
          "https://web.archive.org/web/20240917121136/https://unsplash.com/photos/5S4ciibnbZo/download"
        ];
        name = "5S4ciibnbZo.jpg";
        sha256 = "sha256-OOSK+cY38dUKYHprxqX84neyPq1UtQDetHEUQACv6DM=";
      };
      "${desktop_dir}/14.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/Bjfq0C7oaeU/download"
          "https://web.archive.org/web/20240917120948/https://unsplash.com/photos/Bjfq0C7oaeU/download"
        ];
        name = "Bjfq0C7oaeU.jpg";
        sha256 = "sha256-UwNefY1F9dHGMrnImK/AYYepyNwSN087ZlbHkLk1d7Q=";
      };
      "${desktop_dir}/15.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/S_A_x61vbEk/download"
          "https://web.archive.org/web/20240917123131/https://unsplash.com/photos/S_A_x61vbEk/download"
        ];
        name = "S_A_x61vbEk.jpg";
        sha256 = "sha256-BH3EWrwsuuFv7VI09otGqSJ0NpZhEY8A8syHKzyjwt0=";
      };
    };
    halloween = {
      "${desktop_dir}/halloween_1.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/JwOrT2YNIYg/download"
          "https://web.archive.org/web/20221112212111if_/https://images.unsplash.com/photo-1603616678867-1fa182a9b02b"
        ];
        name = "JwOrT2YNIYg.jpg";
        sha256 = "sha256-5l688/sFUxe5zd25AJCjENQHNR1NVH+dtOJNsYaf12M=";
      };
    };
    fall = {
      "${desktop_dir}/fall_1.jpg".source = fetchurl {
        urls = [
          "https://i.imgur.com/P4Nzucn_d.png?maxwidth=1920"
          "https://web.archive.org/web/20221112212205if_/https://i.imgur.com/P4Nzucn_d.png?maxwidth=1920"
        ];
        name = "P4Nzucn_d.jpg";
        sha256 = "sha256-huRv+VDApkIKaryY7Pss4L00yq/GDFWcSXKqzh/EJHw=";
      };
      "${desktop_dir}/fall_2.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/zrT1tjnxJKQ/download"
          "https://web.archive.org/web/20221113095319if_/https://images.unsplash.com/photo-1507967669805-c23336beea5b"
        ];
        name = "zrT1tjnxJKQ.jpg";
        sha256 = "sha256-SbkKWaz/ezimHYunTt/3XHG73Ki97MdH00NnZybjDKM=";
      };
      "${desktop_dir}/fall_3.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/JPfpKFEIixs/download"
        ];
        name = "JPfpKFEIixs.jpg";
        sha256 = "sha256-t60thGE0QUOUCpwp2S62zU+fkID3rxNSipAf9gw3Kqc=";
      };
    };
    christmas = {
      "${desktop_dir}/christmas_1.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/HnzTTyA_GQ8/download"
          "https://web.archive.org/web/20241205162907if_/https://images.unsplash.com/photo-1605110058825-d3c20f3306d7"
        ];
        name = "HnzTTyA_GQ8.jpg";
        sha256 = "sha256-bXkilii+odRgqwDO7lpyXXQS4WRBD0sReXVEbTY56AA=";
      };
      "${desktop_dir}/christmas_2.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/QZKxCWniHM8/download"
          "https://web.archive.org/web/20241205163000if_/https://images.unsplash.com/photo-1640443455751-461cb6ea2f80"
        ];
        name = "QZKxCWniHM8.jpg";
        sha256 = "sha256-6jfinJA0znPeD7LmPa7JfxLK9yzbAJ3AOu0Fn8zpu7A=";
      };
      "${desktop_dir}/christmas_3.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/-jD6CM6F4fc/download"
          "https://web.archive.org/web/20170710152808if_/https://images.unsplash.com/photo-1482304512605-d38212893111"
        ];
        name = "-jD6CM6F4fc.jpg";
        sha256 = "sha256-cjYXEsTZWkE7irKSQHxo9OB0jnXk1gkXbgEu8zbCBHA=";
      };
      "${desktop_dir}/christmas_4.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/-Qf9JKLysUg/download"
          "https://web.archive.org/web/20230225033202if_/https://images.unsplash.com/photo-1512474932049-78ac69ede12c"
        ];
        name = "-Qf9JKLysUg.jpg";
        sha256 = "sha256-Lj2Gn+5AF4iLhrP9TdTKNymTknijM5rmjKuAcGgogjU=";
      };
      "${desktop_dir}/christmas_5.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/HRcpw5H01Lc/download"
          "https://web.archive.org/web/20241205163801if_/https://images.unsplash.com/photo-1543598098-622a5e218f43"
        ];
        name = "HRcpw5H01Lc.jpg";
        sha256 = "sha256-/Jh/RJShmEu5SVOqKja1oNV4wiVVgJpnjVgFbPqHZ/g=";
      };
    };
    winter = {
      "${desktop_dir}/winter_1.jpg".source = fetchurl {
        urls = [
          "https://web.archive.org/web/20230223042934/https://unsplash.com/photos/efuwb5eBDrI/download"
          "https://unsplash.com/photos/efuwb5eBDrI/download"
        ];
        name = "efuwb5eBDrI.jpg";
        sha256 = "sha256-2N3EIl30MB1mXc3F0lPEF3Xi48UmuytE/IrifO/3xiw=";
      };
      "${desktop_dir}/winter_2.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/xAgvgQpYsf4/download"
          "https://web.archive.org/web/20221220153148/https://unsplash.com/photos/xAgvgQpYsf4/download"
        ];
        name = "xAgvgQpYsf4.jpg";
        sha256 = "sha256-VBo2plsySinTyTD/1EIZBum+CM9BZuy7g7hdh0+E7GA=";
      };
      "${desktop_dir}/winter_3.jpg".source = fetchurl {
        urls = [
          "https://web.archive.org/web/20230228044030im_/https://images.unsplash.com/photo-1581196607303-95c00f31c676"
          "https://unsplash.com/photos/LHm2nLdtC9g/download"
        ];
        name = "LHm2nLdtC9g.jpg";
        sha256 = "sha256-Jn9/+93rxAoTrqfeUU77HmOxPuMVqREhnC4zi1dH2+s=";
      };
    };
    spring = {
      "${desktop_dir}/spring_1.jpg".source = fetchurl {
        urls = [
          "https://unsplash.com/photos/-SO3JtE3gZo/download"
          "https://web.archive.org/web/20230518214415if_/https://images.unsplash.com/photo-1506260408121-e353d10b87c7"
        ];
        name = "SO3JtE3gZo.jpg";
        sha256 = "sha256-ACdeGWxVlsDUFM/yqAhaUXGRlnUDsE5AWzuoRd2EiC8=";
      };
      "${desktop_dir}/spring_2.jpg".source = fetchurl {
        urls = [
          "https://web.archive.org/web/20230225034402/https://unsplash.com/photos/iqeG5xA96M4/download"
          "https://unsplash.com/photos/iqeG5xA96M4/download"
        ];
        name = "xAgvgQpYsf4.jpg";
        sha256 = "sha256-N8eVe086xMBcakrSSl72GP+GneNZSVmFNNNKqhsm9ZA=";
      };
    };
  };
  lockscreen_bg = {
    "${lockscreen_dir}/1.jpg".source = fetchurl {
      urls = [
        "https://unsplash.com/photos/fKs7E71wfyA/download"
      ];
      name = "fKs7E71wfyA.jpg";
      sha256 = "sha256-eAktj75Q01Osc92MKKwh2ix3cQg+v5aBJ0AeNxIP4nU=";
    };
    "${lockscreen_dir}/2.jpg".source = fetchurl {
      urls = [
        "https://unsplash.com/photos/N4x3uaEFKB4/download"
      ];
      name = "N4x3uaEFKB4.jpg";
      sha256 = "sha256-Lub0wOw/UhmeEMwIjIxZxe76jBEedg2/m4dGZMtnPzg=";
    };
    "${lockscreen_dir}/3.jpg".source = fetchurl {
      urls = [
        "https://unsplash.com/photos/9zXMb-E8pI0/download"
      ];
      name = "9zXMb-E8pI0.jpg";
      sha256 = "sha256-NekUPiUUBdVwUdk6j/eIrqIFyyAQpL7aCkUA6xgBa/0=";
    };
  };
}
