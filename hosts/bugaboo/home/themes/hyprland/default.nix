{ pkgs, config, lib, flakeInputs, ... }:
with lib;
with builtins;
{
  config = mkIf (config.sys.theme == "hyprland") {
    services.pipewire.enable = true;
    home-manager.users.${config.username} = {
      imports = [
        ./modules/eww
        ./modules/wofi/wofi.nix
        flakeInputs.hyprcursor-phinger.homeManagerModules.hyprcursor-phinger
      ];
      programs.hyprcursor-phinger.enable = true;

      modules.eww.enable = true;

      xdg.configFile."hypr".source = ./configs/hypr;

      home = {
        file =
          let
            substituteIn = import ../../utils/substituteIn.nix;
          in
          {
            ".config/dunst/dunstrcc".source = ../../config/dunst/dunstrc;
            ".config/zathura/zathurarc".text =
              substituteIn {
                file = ../bspwm/configs/zathura/zathurarc;
                substitutions = import ../bspwm/colors.nix;
              };
          };
        packages = with pkgs; [
          brightnessctl
          swww
          hyprpicker
          wofi
          wl-clipboard
          cliphist
          xdg-desktop-portal-hyprland
          pavucontrol
        ];
        pointerCursor = {
          name = "phinger-cursors-dark";
          package = pkgs.phinger-cursors;
          gtk.enable = true;
          x11.enable = true;
        };
      };
      gtk = {
        enable = true;
        theme = {
          name = "Arc-Dark";
          package = pkgs.arc-theme;
        };
      };

    };

    fonts.packages = lib.mkAfter [
      (import ./modules/eww/font { inherit pkgs lib; })
    ];

    #services.swhkd.enable = true;
    programs.hyprland.enable = true;
    programs.xwayland.enable = true;

    xdg.portal = {
      enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };

    services = {
      displayManager = {
        enable = true;
        defaultSession = "hyprland";
        sddm.enable = true;
      };
      # xserver = {
      #   enable = true;
      #   displayManager = {
      #     gdm.enable = true;
      #   };
      # };
    };

    # environment.sessionVariables = {
    #   CLUTTER_BACKEND = "wayland";
    #   XDG_SESSION_TYPE = "wayland";
    #   QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    #   MOZ_ENABLE_WAYLAND = "1";
    #   QT_QPA_PLATFORM = "wayland;xcb";
    #   GDK_BACKEND = "wayland";
    #   _JAVA_AWT_WM_NONREPARENTING = "1";
    #   XCURSOR_SIZE = "24";
    #   NIXOS_OZONE_WL = "1";
    # };
    environment.systemPackages = with pkgs; [
      qt6.qtwayland
      grim
      brightnessctl
      playerctl
    ];
    #https://github.com/NixOS/nixpkgs/issues/143365
    security.pam.services.swaylock = { };
  };
}
