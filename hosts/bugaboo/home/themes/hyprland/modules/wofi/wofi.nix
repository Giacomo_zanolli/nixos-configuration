{ lib, config, pkgs, ... }:
{
  home.packages = with pkgs; [
    wofi
  ];
}
