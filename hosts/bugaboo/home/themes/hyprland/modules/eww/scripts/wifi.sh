#! /usr/bin/env sh

if iwgetid -r >/dev/null 2>/dev/null; then
    icon=""
    ssid=$(iwgetid -r)
    status="Connected to ${ssid}"
else
    icon=""
    status="offline"
fi

echo "{\"icon\": \"${icon}\", \"status\": \"${status}\"}"
