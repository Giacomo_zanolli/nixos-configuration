{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.modules.eww;
in
{
  options.modules.eww = { enable = mkEnableOption "eww"; };

  config = mkIf cfg.enable {
    # theres no programs.eww.enable here because eww looks for files in .config
    # thats why we have all the home.files

    # eww package
    home.packages = with pkgs; [
      eww
      pamixer
      brightnessctl
    ];

    # configuration
    home.file = {
      ".config/eww/eww.scss".source = ./eww.scss;
      ".config/eww/eww.yuck".source = ./eww.yuck;
      ".config/eww/scripts".source = ./scripts;
      ".config/eww/widgets".source = ./widgets;
    };
  };
}
