#! /usr/bin/env bash

# Read the CPU usage data from /proc/stat
read cpu user nice system idle iowait irq softirq steal guest guest_nice </proc/stat

# Calculate the total CPU usage time
cpu_total=$((user + nice + system + idle + iowait + irq + softirq + steal))

# Calculate the idle CPU usage time
cpu_idle=$idle

# Wait for one second to measure CPU usage during this interval
sleep 1

# Read the CPU usage data again after waiting for one second
read cpu user nice system idle iowait irq softirq steal guest guest_nice </proc/stat

# Calculate the total CPU usage time again
cpu_total2=$((user + nice + system + idle + iowait + irq + softirq + steal))

# Calculate the idle CPU usage time again
cpu_idle2=$idle

# Calculate the percentage of non-idle CPU usage during this interval
cpu_usage_percentage=$((100 * (cpu_total2 - cpu_total - (cpu_idle2 - cpu_idle)) / (cpu_total2 - cpu_total)))

# Round off to nearest integer using printf command.
printf "%.0f\n" "$cpu_usage_percentage"
