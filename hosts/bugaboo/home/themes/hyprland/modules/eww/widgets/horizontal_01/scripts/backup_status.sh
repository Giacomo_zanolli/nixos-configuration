#! /usr/bin/env bash

OK_ICON=""
RUNNING_ICON=""
ERROR_ICON=""
PARTIAL_ICON=""
UNKNOWN_ICON=""
STATUS_FILE="/run/restic_status_offsite"
EXPECTED_BACKUP_FREQUENCY_MINUTES=60

# Validate and sanitize input values
if ! [[ "$EXPECTED_BACKUP_FREQUENCY_MINUTES" =~ ^[0-9]+$ ]]; then
  echo "Invalid expected backup frequency value: $EXPECTED_BACKUP_FREQUENCY_MINUTES"
  exit 1
fi

# Convert to seconds for ease of computation
EXPECTED_BACKUP_FREQUENCY_SECONDS=$(($EXPECTED_BACKUP_FREQUENCY_MINUTES * 60))
+
# Store the start time of the script
START_TIME=$(date +%s)

#Utility function to avoid sending repeatedly the same error message
PREVISOUS_ERROR="nay"
notifyError() {
  if [[ $PREVISOUS_ERROR != "$1" ]]; then
    PREVISOUS_ERROR="$1"
    notify-send --urgency critical --app-name "Backup watcher" "$1" >/dev/null 2>/dev/null
  fi
}

NONE_VALUE=-1
TIMED_OUT_ON=$NONE_VALUE
LAST_BACKUP_STARTED_ON=$NONE_VALUE
LAST_BACKUP_COMPLETE_ON=$NONE_VALUE
WHERE_WE_IN_ERROR_CONDITION=false
FREQ_AND_MARGIN=$(($EXPECTED_BACKUP_FREQUENCY_SECONDS * 2))

while true; do
  NOW=$(date +%s)
  # Compute the time since the start of the script
  RUN_TIME=$(($NOW - $START_TIME))

  # If there is no status file
  if [[ ! -r "$STATUS_FILE" ]]; then
    # (we use double the frequency since the backup may take a while)
    MAX_WAIT_TIME=$(($EXPECTED_BACKUP_FREQUENCY_SECONDS * 2))
    # If we are still waiting for the first backup
    if [[ $RUN_TIME -lt $MAX_WAIT_TIME ]]; then
      RUN_TIME_MINUTES=$(($RUN_TIME / 60))
      STATUS="Waiting for the first backup (for $RUN_TIME_MINUTES minutes)"
      ICON=$UNKNOWN_ICON
    # Else we timed out waiting for the first backup
    else
      if [[ TIMED_OUT_ON -eq $NONE_VALUE ]]; then
        TIMED_OUT_ON=$NOW
      fi
      TIMED_OUT_ON_DURATION=$((($NOW - $TIMED_OUT_ON) / 60))
      STATUS="Timed out waiting for first backup ($TIMED_OUT_ON_DURATION minutes ago)"
      ICON=$ERROR_ICON

      notifyError "Error during the backup: Timed out waiting for first backup."
    fi
  else
    # Check that `$STATUS_FILE` has not been stale for more than `FREQ_AND_MARGIN` seconds
    FILE_MODIFIED_UNIX_TS=$(stat --format=%Y "$STATUS_FILE")
    if [[ $(($NOW - $FILE_MODIFIED_UNIX_TS)) -gt $FREQ_AND_MARGIN ]]; then
      HUMAN_READABLE=$(date --date "@$FILE_MODIFIED_UNIX_TS")
      STATUS="Status file has not been updated since $HUMAN_READABLE"
      ICON="$ERROR_ICON"

      notifyError "Error during the backup: Status file has not been updated."
    else
      STATUS_FILE_CONTENT=$(cat "$STATUS_FILE")
      case "$STATUS_FILE_CONTENT" in
      Preparing | Running)
        LAST_BACKUP_COMPLETE_ON=$NONE_VALUE
        if [[ LAST_BACKUP_STARTED_ON -eq $NONE_VALUE ]]; then
          LAST_BACKUP_STARTED_ON=$NOW
        fi
        BACKUP_DURATION=$(($NOW - $LAST_BACKUP_STARTED_ON))
        STATUS="Backup running ($BACKUP_DURATION seconds)"
        ICON="$RUNNING_ICON"
        ;;
      Done)
        if [[ LAST_BACKUP_COMPLETE_ON -eq $NONE_VALUE ]]; then
          LAST_BACKUP_COMPLETE_ON=$NOW
        fi
        DONE_MINUTES_AGO=$((($NOW - $LAST_BACKUP_COMPLETE_ON) / 60))
        STATUS="Backup complete ($DONE_MINUTES_AGO minutes ago)"
        ICON="$OK_ICON"
        ;;
      Failed)
        notifyError "Error during the backup: Backup failed"
        STATUS="Something went wrong"
        ICON="$ERROR_ICON"
        ;;
      *)
        notifyError "Error during the backup: Something went wrong, the file has unknown content"
        STATUS="Something went wrong"
        ICON="$ERROR_ICON"
        ;;
      esac
      case "$STATUS_FILE_CONTENT" in
      Preparing | Running)
        # Nothing to do here
        ;;
      *)
        LAST_BACKUP_STARTED_ON=$NONE_VALUE
        ;;
      esac
    fi
  fi

  # Echo out the result so that eww can display the widget
  printf '{"status":"%s", "icon":"%s"}\n' "$STATUS" "$ICON"

  sleep 10
done
