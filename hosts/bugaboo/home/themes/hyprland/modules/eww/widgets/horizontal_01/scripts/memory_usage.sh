#! /usr/bin/env bash

MEMORY=$(free | grep Mem)
TOTAL=$(echo $MEMORY | awk '{print $2}')
USED=$(echo $MEMORY | awk '{print $3}')

PERCENT_USED=$(((100 * USED) / TOTAL))

echo "${PERCENT_USED}"
