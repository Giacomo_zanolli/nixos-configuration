#! /usr/bin/env bash

# Format of the information displayed
# Eg. {{ artist }} - {{ album }} - {{ title }}
# See more attributes here: https://github.com/altdesktop/playerctl/#printing-properties-and-metadata
FORMAT="{{ title }}"

PLAYERCTL_STATUS=$(playerctl status 2>/dev/null)
EXIT_CODE=$?

if [ $EXIT_CODE -eq 0 ]; then
  STATUS=$PLAYERCTL_STATUS
else
  STATUS="No Music Detected"
fi

if [ "$STATUS" = "Playing" ]; then
  TITLE=$(playerctl metadata --format "$FORMAT")
else
  TITLE="Paused"
fi

echo "{
  \"status\": \"${STATUS}\",
  \"title\": \"${TITLE}\"
}
"
