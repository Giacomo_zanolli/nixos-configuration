#! /usr/bin/env sh

# Loop through all thermal zones and check if any of them represent the CPU
for zone in /sys/class/thermal/thermal_zone*; do
  type=$(cat $zone/type)
  if [[ $type == *"CPU"* || $type == *"pch_skylake"* ]]; then
    # Read the CPU temperature from this thermal zone
    temp=$(cat $zone/temp)

    # Round the temperature to the nearest integer
    # (adding 500 makes the truncation behave as the rounding)
    temp_rounded=$(((temp + 500) / 1000))

    # Output the temperature and exit
    echo "${temp_rounded}"
    exit 0
  fi
done

# If no thermal zone represents the CPU, output an error message and exit with an error code.
echo "Unable to determine CPU temperature." >&2
exit 1
