#! /usr/bin/env bash

#Inspired by https://github.com/aristocratos/btop/blob/471c68f4b03196d2247ed361d56b14661fb177c2/src/linux/btop_collect.cpp#L537

for dir in /sys/class/power_supply/*; do
	if [ -d "$dir" ]; then
		type=$(cat "$dir/type")
		if [ $type = "Battery" -o $type = "UPS" ]; then
			if [ $(cat "$dir/present") == "1" ]; then
				if [ -f "$dir/energy_now" ]; then
					energy_now=$(cat "$dir/energy_now")
				elif [ -f "$dir/charge_now" ]; then
					energy_now=$(cat "$dir/charge_now")
				else
					energy_now="-1"
				fi

				if [ -f "$dir/energy_full" ]; then
					energy_full=$(cat "$dir/energy_full")
				elif [ -f "$dir/charge_full" ]; then
					energy_full=$(cat "$dir/charge_full")
				else
					energy_full="1"
				fi

				charging=false
				status=$(cat "$dir/status")
				if [[ $status = "Charging" || $status = "charging" ]]; then
					charging=true
				fi

				charge_percentage=$((100 * $energy_now / $energy_full))
			fi
		fi
	fi
done

if [ "$charge_percentage" -gt "90" ]; then
	icon=""
elif [ "$charge_percentage" -gt "65" ]; then
	icon=""
elif [ "$charge_percentage" -gt "35" ]; then
	icon=""
elif [ "$charge_percentage" -gt "15" ]; then
	icon=""
else
	icon=""
fi

echo "{\"percent\":\"$charge_percentage\",\"icon\":\"$icon\",\"charging\":\"$charging\",\"status\":\"$status\"}"
