#! /usr/bin/env nix-shell
#! nix-shell -i bash -p fontforge python3 scour

# Function to optimize SVG files and preserve directory structure
optimize_svg() {
  local src_dir="$1"
  local dest_dir="$2"
  mkdir -p "$dest_dir"

  # loop over files and directories in the source directory
  for item in "$src_dir"/*; do
    local item_basename="$(basename "$item")"
    local dest_item="$dest_dir/${item_basename%.svg}_optimized.svg"

    if [[ -d "$item" ]]; then
      # if item is a directory, recursively call the function
      optimize_svg "$item" "$dest_dir/$item_basename"
    elif [[ -f "$item" && "${item_basename##*.}" = "svg" ]]; then
      # if item is an SVG file, optimize it and save it to the destination directory
      scour -i "$item" -o "$dest_item" \
        --quiet \
        --remove-metadata \
        --enable-viewboxing \
        --enable-id-stripping \
        --enable-comment-stripping \
        --shorten-ids \
        --protect-ids-noninkscape \
        --strip-xml-space \
        --indent=none

      local input_size=$(stat -c %s "$item")
      local output_size=$(stat -c %s "$dest_item")
      # Use the original file if the optimized file is larger
      if ((output_size >= input_size)); then
        cp "$item" "$dest_item"
        #echo "Using original file for $item"
      fi
    fi
  done
}

# Check if source directory was provided as an argument
if [[ $# -lt 1 ]]; then
  echo "Please provide a source directory as an argument"
  exit 1
fi

# Check if destination directory was provided as an argument
if [[ $# -lt 2 ]]; then
  echo "Please provide a destination directory as the second argument"
  exit 1
fi

DEST="$2"
mkdir -p $DEST

echo "Optimizing the SVGs"
# Call the optimize_svg function with source and destination directories
optimize_svg "$1" "$DEST"

echo "Done"
