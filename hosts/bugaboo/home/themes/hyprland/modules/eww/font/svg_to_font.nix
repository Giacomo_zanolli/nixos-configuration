{ stdenv
, fetchFromGitHub
}:

stdenv.mkDerivation rec {
  pname = "svg_to_font";
  version = "1.0.0";

  buildInputs = [ ];

  src = fetchFromGitHub {
    owner = "civts";
    repo = "svg_to_font";
    rev = "v1.0.0";
    sha256 = "sha256-CMpBWVVPe/4lbE9l1xDBpLGc+F0c7hfqQonYwpIn/AA=";
  };

  installPhase = ''
    mkdir -p $out
    cp svg_to_font.py $out/svg_to_font.py
  '';
}
