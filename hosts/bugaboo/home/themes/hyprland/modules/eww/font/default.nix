{ lib, pkgs }:

let
  name = "UsefulReindeer";
  svg_to_font = pkgs.callPackage ./svg_to_font.nix { };
in
pkgs.stdenvNoCC.mkDerivation {
  pname = name;
  version = "0.0.1";

  src = ./src;

  buildInputs = with pkgs;
    [
      scour
      python3
      fontforge
      svg_to_font
    ];
  dontConfigue = true;

  buildPhase = ''
    # Remove the nix-shell shebang
    sed -i '/^#!/d' optimize.sh
    ./optimize.sh svgs optimized_svgs

    echo "Creating the font"
    python ${svg_to_font}/svg_to_font.py optimized_svgs ${name}
  '';

  installPhase = ''
    install -Dm644 ${name}.otf -t $out/share/fonts/${name}
  '';

  meta = with lib; {
    maintainers = with maintainers; [ mikoim raskin ];
    platforms = platforms.all;
  };
}
