#! /usr/bin/env bash

set -e

printf "running hyprland custom init script"

printf "wallpaper"
IMG=$(ls ~/.wallpapers/desktop | sort -R | tail -1) && swww img ~/.wallpapers/desktop/$IMG

printf "clipboard for text"
wl-paste --type text --watch cliphist store &
disown

printf "bars and panels"
eww daemon && eww open statusbar

swww-daemon &
disown

printf "clipboard for images"
wl-paste --type image --watch cliphist store &
disown
