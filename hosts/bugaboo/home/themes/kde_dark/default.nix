{ pkgs, config, lib, ... }:
with lib;
with builtins;
{
  config = mkIf (config.sys.theme == "plasma") {
    home-manager.users.${config.username}.home = {
      file = {
        #Gtk theming
        ".config/gtk-3.0/settings.ini".source = ./configs/gtk/settings.ini;
        ".config/gtk-4.0/settings.ini".source = ./configs/gtk/settings.ini;
        #Kde theming and config
        ".config/plasma-org.kde.plasma.desktop-appletsrc".source = ./configs/kde/plasma-org.kde.plasma.desktop-appletsrc;
        ".config/kdedefaults/kdeglobals".source = ./configs/kde/kdedefaults/kdeglobals;
        ".config/kdedefaults/kscreenlockerrc".source = ./configs/kde/kdedefaults/kscreenlockerrc;
        ".config/kdedefaults/plasmarc".source = ./configs/kde/plasmarc;
        ".config/plasmashellrc".source = ./configs/kde/plasmashellrc;
        ".config/kdedefaults/kwinrc".source = ./configs/kde/kdedefaults/kwinrc;
        ".config/kdedefaults/kcminputrc".source = ./configs/kde/kdedefaults/kcminputrc;
        ".config/kdeglobals".source = ./configs/kde/kdeglobals;
      };
    };
    services.xserver = {
      displayManager.sddm.enable = true;
      desktopManager.plasma5.enable = true;
    };
  };
}
