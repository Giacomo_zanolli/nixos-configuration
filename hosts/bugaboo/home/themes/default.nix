{ pkgs, lib, ... }:
with lib;
with builtins;
{
  options.sys = {
    theme = mkOption {
      type = types.enum [ "plasma" "bspwm" "hyprland" ];
      description = ''
        This is the theme to be used by the system.
        Each theme specifies one or more desktop environments and the theme.
      '';
      default = "bspwm";
    };
  };
  imports = [ ./bspwm ./kde_dark ./hyprland ];
}
