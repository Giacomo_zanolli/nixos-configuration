#This fetches the file
#https://github.com/marioortizmanero/polybar-pulseaudio-control/blob/master/pulseaudio-control.bash
#(for the version indicated in rev)

{ pkgs }:

pkgs.fetchFromGitHub
  {
    owner = "marioortizmanero";
    repo = "polybar-pulseaudio-control";
    rev = "v3.1.0";
    sha256 = "sha256-jMuAi4W91hjv6H7tCffDSIn7aqhoIC0DAoKy1IFygJ4=";
  } + "/pulseaudio-control.bash"
