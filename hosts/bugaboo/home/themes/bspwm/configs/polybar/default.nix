{ pkgs, ... }:

{
  home = {
    file = {
      #Polybar
      ".config/polybar/launch.sh".source = ./launch.sh;
      ".config/polybar/get_temp.sh".source = ./get_temp.sh;
      ".config/polybar/audio_control.sh".source = import ./audio_control.nix {
        inherit pkgs;
      };
      ".config/polybar/config.ini".source = ./config.ini;
    };
    packages = with pkgs; [
      pavucontrol
      polybar
    ];
  };
}
