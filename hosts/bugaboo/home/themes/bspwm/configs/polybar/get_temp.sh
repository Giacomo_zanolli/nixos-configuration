#! /bin/sh

for f in /sys/class/thermal/thermal_zone*; do
  type=$(cat $f/type)
  if [ "$type" == "x86_pkg_temp" ] ; then
    sensor=$f
  fi
done

while true; do
  printf "$(cat ${sensor}/temp | sed 's/\(.\)..$/°C/')\n"
  sleep 2
done
