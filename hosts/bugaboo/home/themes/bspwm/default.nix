{ pkgs, config, lib, ... }:
with lib;
with builtins;
{
  config = mkIf (config.sys.theme == "bspwm") {
    home-manager.users.${config.username} = {
      imports = [
        ./configs/polybar
      ];
      home = {
        file =
          let
            substituteIn = import ../../utils/substituteIn.nix;
          in
          {
            #BSPWM
            ".config/bspwm/bspwmrc".source = ./configs/bspwmrc;
            ".config/picom.conf".source = ./configs/picom.conf;
            ".config/dunst/dunstrcc".source = ../../config/dunst/dunstrc;
            #Rofi
            ".config/rofi/config.rasi".text = substituteIn {
              file = ./configs/rofi/config.rasi;
              substitutions = import ./colors.nix;
            };
            #X11 start script
            ".xprofile".source = ./configs/.xprofile;
            #Feh
            ".fehbg".source = ./configs/.fehbg;
            #Zathura
            ".config/zathura/zathurarc".text =
              substituteIn {
                file = ./configs/zathura/zathurarc;
                substitutions = import ./colors.nix;
              };
          };
        packages = with pkgs; [
          brightnessctl
          feh
          picom
          dconf
          (
            rofi.override {
              plugins = [ rofi-emoji ];
            }
          )
          rofi-power-menu
        ];
        pointerCursor = {
          name = "phinger-cursors";
          package = pkgs.phinger-cursors;
          gtk.enable = true;
          x11.enable = true;
        };
      };
      gtk = {
        enable = true;
        theme = {
          name = "Arc-Dark";
          package = pkgs.arc-theme;
        };
      };
    };
    services.xserver = {
      displayManager.gdm.enable = true;
      windowManager.bspwm.enable = true;
      libinput = {
        enable = true;
        touchpad = {
          scrollMethod = "twofinger";
        };
      };
    };
  };
}
