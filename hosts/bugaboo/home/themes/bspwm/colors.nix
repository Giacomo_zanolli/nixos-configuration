{
  #Primary background
  background = "#151518";
  #Secondary background
  background-alt = "#1F1F23";
  #Primary Foreground
  foreground = "#F8F8F2";
  #Secondary Foreground
  foreground-alt = "#3E5B7A";
}
