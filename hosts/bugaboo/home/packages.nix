{ pkgs, ... }:

with pkgs; [
  firefox
  brave
  #cinny-desktop

  kdePackages.okular
  kdePackages.kate
  kdePackages.gwenview
  libreoffice
  #kubectl

  blueman
  keepassxc
  sxhkd
  pcmanfm
  logseq
  signal-desktop

  shfmt #Needed for foxundermoon.shell-format
  nixpkgs-fmt #Needed for jnoortheen.nix-ide
  libnotify #De facto standard to send notifications
  playerctl
  pulseaudio-ctl
  (mpv.override {
    scripts = [ mpvScripts.mpris ];
  })
  zathura
  rclone
  xcolor
  monitor
  ffmpeg
  yt-dlp
  openvpn
  dig
  b3sum

  gimp
  smartmontools
  hdparm
  nixos-generators
  gptfdisk
  gparted
  squirreldisk
  nix-serve
  # apktool
  rust-petname
  dig
  aria2
  memtester
  nsxiv
  gparted
  librecad
  # screenkey

  # cpulimit
  # mitmproxy
  # kdenlive
  # openjdk8
  rust-petname
  wireshark

  lolcat
  # lynx
  # sl
  # sshfs
  # nmap
  # mitmproxy
  # kdenlive
  wireshark
  # lsof
  # pmd 
  rust-petname
  aria2
  # freecad
  # polkit_gnome
  # hw-probe
  arandr
  poppler_utils
  # netcat
  # ipcalc
  nsxiv
  # smartmontools #For SMART disk errors
  # gptfdisk #For gdisk

  deploy-rs
  # #bashtop
  # #midori
  # #qemu
  # kcharselect
  # #libreoffice
  # colorpicker
  # #latte-dock
  # kdeconnect
  # #flutter
  # #remmina
  # #Conky
  # docker-compose
  # #Kde theme engine
  # rofi
  # kitty
  # xfce.thunar
  # lxappearance

  # https://github.com/ogham/exa
  # iftop
  asciiquarium
  figlet
  cbonsai
  xsnow
  neo
  pipes
  rain.sh
  cool-retro-term
  lolcat
  cmatrix
  tty-clock
  vitetris
  # gotop
  # termdown
  # sl
  # spotify-tui
  ripgrep

  # #curl parrot.live
  # #https://github.com/jmhobbs/terminal-parrot


  # #To test
  # #https://celluloid-player.github.io/
  # #topgrade
  # #https://github.com/romkatv/powerlevel10k
  # https://github.com/Warinyourself/lightdm-webkit-theme-osmos
  # https://github.com/karlstav/cava
  # https://gitlab.com/dwt1/shell-color-scripts
  # https://github.com/o2sh/onefetch
  # https://github.com/kamadorueda/alejandra

  #gsettings-desktop-schemas

  # fish
  # ffmpeg
  # auto-cpufreq
  # #gimp
  # #inkscape
  # ipc-bench
  tokei #much faster than sloccount, with more languages and written in rust 🧡
  sops
  libsForQt5.filelight
  croc

  wofi-emoji
  wofi
]
