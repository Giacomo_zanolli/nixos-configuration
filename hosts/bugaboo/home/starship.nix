{
  home.file = {
    ".config/starship.toml".source = ./config/starship/starship.toml;
  };
  programs.starship.enable = true;
  programs.zsh.initExtra = ''eval "$(starship init zsh)"'';
}
