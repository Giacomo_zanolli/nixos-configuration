{ pkgs, config, lib, ... }:

{
  imports = [
    ./waylandlockscreen.nix
    # ./openvpn.nix
    ./vscode.nix
    ./zsh.nix
  ];

  # Trusted root certificates (root certificate authorities)
  security.pki.certificateFiles = [ ../../../secrets/cetus/k8s/ca.crt ];

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    users.${config.username} = {
      programs.home-manager.enable = true;

      imports = [
        ./xdg.nix
        ./direnv.nix
        ./starship.nix
        ./git.nix
      ];

      home =
        let
          wallpapers = import ./wallpapers.nix { fetchurl = pkgs.fetchurl; };
        in
        {
          file = wallpapers.desktop_bg.winter // wallpapers.lockscreen_bg // {
            ".config/sxhkd/sxhkdrc".source = ./config/sxhkd/sxhkdrc;
            ".config/swhkd/swhkdrc".source = ./config/sxhkd/sxhkdrc;
            ".config/mpv/mpv.conf".source = ./config/mpv/mpv.conf;
            ".config/clipit/clipitrc".source = ./config/clipit/clipitrc.conf;
            # ".config/BraveSoftware/Brave-Browser/Local State".source = ./config/brave/local_state.json;
            # ".config/BraveSoftware/Brave-Browser/Default/Preferences".source = ./config/brave/preferences.json;
          };
          packages = import ./packages.nix { inherit pkgs; };
          sessionVariables = {
            BROWSER = "brave";
            TERMINAL = "alacritty";
            TERM = "alacritty";
          };
          stateVersion = "24.11";
        };

      programs.gpg = {
        enable = true;
        settings = {
          no-emit-version = true;
          with-fingerprint = true;
        };
      };

      programs.alacritty.enable = true;

      services = let onX11 = config.sys.theme != "hyprland"; in {
        #Screenshots
        flameshot.enable = onX11;
        #Notifications
        dunst = {
          enable = true;
          configFile = "/home/${config.username}/.config/dunst/dunstrcc";
        };
        parcellite = {
          enable = onX11;
          package = pkgs.clipit;
        };
        udiskie.enable = false;
        #kdeconnect.enable = true;
      };

      xsession.numlock.enable = true;
    };
  };
}
