{ pkgs, config, ... }: {
  home-manager.users.${config.username} = {
    programs.vscode = {
      enable = true;
      package = pkgs.vscodium;
      extensions = with pkgs.vscode-extensions; [
        bungcip.better-toml
        ms-azuretools.vscode-docker
        jnoortheen.nix-ide
        esbenp.prettier-vscode
        foxundermoon.shell-format
        redhat.vscode-yaml
        signageos.signageos-vscode-sops
      ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        #Note, this downloads from the following url
        #"https://${publisher}.gallery.vsassets.io/_apis/public/gallery/publisher/${publisher}/extension/${name}/${version}/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage"

        {
          name = "yuck";
          publisher = "eww-yuck";
          version = "0.0.3";
          sha256 = "sha256-DITgLedaO0Ifrttu+ZXkiaVA7Ua5RXc4jXQHPYLqrcM=";
        }
      ];
      #See https://github.com/nix-community/home-manager/issues/2798
      mutableExtensionsDir = false;
      profiles.default.userSettings = pkgs.lib.importJSON ./config/vscode/settings.json // {
        "shellformat.path" = "/etc/profiles/per-user/${config.username}/bin/shfmt";
        "sops.defaults.ageKeyFile" = "/home/${config.username}/.sops/sops_key";
      };
    };
  };
}
