{
  programs.zsh.initExtra = ''eval "$(direnv hook zsh)"'';
  programs.direnv.enable = true;
  programs.direnv.nix-direnv.enable = true;
  #https://github.com/nix-community/nix-direnv
}
