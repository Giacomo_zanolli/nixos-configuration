{ config, lib, ... }:
with builtins; let
  dirListing = readDir ../../../secrets/${config.hostname}/vpn/configs/ovpn;
  fileNames = attrNames dirListing;
  mapper = name:
    let nameClean = replaceStrings [ ".ovpn" ] [ "" ] name; in
    {
      ${nameClean} = {
        config = " config " + ../../../secrets/${config.hostname}/vpn/configs/ovpn + ("/" + name);
        autoStart = false;
      };
    };
  configs = map mapper fileNames;
  mergedConfig = foldl' (x: y: x // y) { } configs;
in
{
  services.openvpn.servers = mergedConfig;
  environment.etc."ovpn".source = ../../../secrets/${config.hostname}/vpn/openvpn_credentials.txt;
}
