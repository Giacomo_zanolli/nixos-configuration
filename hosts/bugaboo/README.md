# NixOs configuration

## Setup on a new machine

Launch the live nixos installer on the new machine (USB or CD). In the shell, run the following:

1. `sudo su` to get a root shell.
1. If needed, `loadkeys <language-code>` to change the keyboard layout (the default one is english).
1. <details>
   <summary>Expand this section if you need help conencting to the Internet (else skip this step)</summary>
    
   1. `ping -c 1 google.com` (or ping `1.1.1.1`) to check that you are indeed offline.
    
   1. View available network interfaces with `ip link`.
    
   1. Bring up the desired interface `ip link set wlan0 up`.

   1. Scan the available networks with `iw wlan0 scan | grep SSID`.

   1. Write the configuration `wpa_passphrase 'ExampleWifiSSID' 'ExampleWifiPassword' > /etc/wpa_supplicant/wpa_supplicant.conf`.

   1. Check that the connection succeeds with `wpa_supplicant -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf` (you should
      see CTRL-EVENT-CONNECTED).

   1. Run wpa_supplicant as a daemon by adding the -B flag: `wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant. conf`

   1. Get an ip `dhcpcd -i wlan0`.

   1. Retry the `ping`.

   </details>

1. `curl -L -o setup.sh https://is.gd/NRZtPF` to fetch the automatic partitioning script for `bugaboo` (my laptop) or `https://is.gd/Xs8bko` for `cetus` (my raspberry).
1. `chmod +x setup.sh` to make it executable.
1. `./setup.sh` to run it.
1. Follow the instuctions on screen:
   1. Specify the root password
   1. Choose the passphrase for the encrypted root logical volume
   1. Choose the disk to partition (e.g., 'sda')
   1. That's it, just wait for the script to finish.
      It will restart the system automatically.
1. That's it! The system is ready to use.

### Sops

If you use [sops-nix](https://github.com/Mic92/sops-nix), these are the steps I took (following the official [example](https://github.com/Mic92/sops-nix/blob/c5dab21d8706afc7ceb05c23d4244dcb48d6aade/README.md?plain=1#L48)):

1. Install `sops` on your system
1. Added `~/.config/sops` to the directories that are persisted by impermanence
1. In a shell with the `age` package available, generate the privte key with `mkdir -p ~/.config/sops/age/; age-keygen -o ~/.config/sops/age/keys.txt`
1. The public key is shown in the output of the previous command, but you can also get it with `age-keygen -y ~/.config/sops/age/keys.txt`
1. Create a `.sops.yaml` file in the root of the project with this content

```yaml
keys:
  # Replace this public key with yours
  - &your_name age1a8486uepsupw5he8swfqmmpz04zzv3shf8qyjuaead8d32d9fa0m7y9gef
creation_rules:
  - path_regex: secrets/[^/]+\.yaml$
    key_groups:
      - age:
          - *your_name
```

1. Get the public key(s) of the other machine(s) that will need to access the secrets. To do this, you can either:

- Create a new age key on the machine(s) and take the public key
- Obtain a public key from an ssh key with
  `nix-shell -p ssh-to-age --run 'ssh-keyscan example.com | ssh-to-age'` or `nix-shell -p ssh-to-age --run 'echo "ssh-ed25519 public key" | ssh-to-age'`

1. Now add the key in the `.sops.yaml` file to give them access to the secrets, like this

   ```yaml
   keys:
     - &your_name age1a8486uepsupw5he8swfqmmpz04zzv3shf8qyjuaead8d32d9fa0m7y9gef
     - &other_machine anotherAgeKey
   creation_rules:
     - path_regex: secrets/[^/]+\.yaml$
       key_groups:
         - age:
             - *your_name
             - *other_machine
   ```

1. You can start creating and editing files with sops.

1. If you are in vscode, consider using the extension `signageos.signageos-vscode-sops` (at the time of writing, version 0.7.1) and
   add to your vscode `settings.json`

   - `"sops.configPath": ".sops.yaml"` to tell it to look at the .sops.yaml file and
   - `"sops.defaults.ageKeyFile" = "/path/to/your/sops/keys.txt"`

1. To encrypt an exiting file

   - If it is yaml
     you can use a command similar to this `sops --encrypt --age $(cat $SOPS_AGE_KEY_FILE |grep -oP "public key: \K(.*)") --encrypted-regex '^(data|stringData)$' --in-place ./secret.yaml` (took the command from [this video from Techno Tim](https://youtu.be/1BquzE3Yb4I?t=551))

   - If it is another supported format, look at the docs

   - Else, you can encrypt the whole file as a block of binary data (command adapted from [this video from Techno Tim](https://youtu.be/1BquzE3Yb4I?t=1334s))
     `RECIPIENT_PUBLIC_KEY=$(cat ~/.sops/sops_key| grep -oP "public key: \K(.*)"); sops --encrypt --age $RECIPIENT_PUBLIC_KEY --config .sops.yaml --in-place /path/to/file/to/encrypt`. If you want to have more than one recipient, put the public keys in the --age argument, separated by a comma (',').

## Updating the config

1. Edit these configuration files
1. Run `nixos-rebuild test --use-remote-sudo` or `nixos-rebuild switch --use-remote-sudo`.

   A word of warning since I spent days troubleshooting this:  
   Do not use `sudo nixos-rebuild switch --flake`, as it does not work. You can also not use `sudo nixos-rebuild switch --flake /etc/mnt/nixos#` or `sudo nixos-rebuild switch --flake /etc/mnt/nixos#bugaboo`. You can however use `sudo nixos-rebuild switch --flake .#'` from inside the /etc/nixos folder (and also outside of it if you specify the path instead of the '.' I suppose; but I have not tested it).

## Troubleshooting:

1. Error: `/boot does not look like a valid EFI partition`
   Fix: run `modprobe efivars` immediately before nixos-install.

1. Error: `access to /mnt/nix/store/<hash>-nmd source/flake.nix is forbidden in restricted mode`
   Fix: run the `nixos-install` command in a `nix-shell` with the pacakge `nixUnstable`

1. Wrong screen resolution.
   Fix: you can use `xrandr` and `arandr`

1. Sops-nix does not find the key file.
   Fix: ensure that the keyfile is on a path that is mounted when sops tries to read it. If, for example, you use a keyfile managed by impermanence, it will not have been mounted yet.

## TODOs:

Things I still would like to tackle before making this my daily driver.

- [x] Scripts:

  - [x] Integrate first_run script in the setup script

- [x] Configs:

  - [x] Modular config for easy theme switching

- [x] GRUB themes:

  - https://github.com/Patato777/dotfiles/tree/main/grub/themes/virtuaverse

  - https://github.com/nobreDaniel/dotfile

- [x] Secrets

  - Sops-nix

    - https://github.com/akkesm/dotfiles
    - https://github.com/chessai/nixos-configs
    - https://github.com/Misterio77/nix-config
    - https://github.com/NickCao/flakes
    - https://github.com/dlo9/nixos-config (see README)
    - https://github.com/seandheath/dotfiles
    - https://github.com/GTrunSec/nixos-flk

- [x] External monitors

  - https://wiki.archlinux.org/title/Xrandr
  - https://linuxconfig.org/how-to-configure-your-monitors-with-xrandr-in-linux
  - The solution I found is to use arandr every time I plug a monitor with bspwm. Hyprland handles external monitors automatically

- [] GPU

- [] Cooling fans

- [] CI pipeline with hydra of some type

  - https://github.com/NickCao/flakes

- [] Sxhkd helper

  https://www.reddit.com/r/bspwm/comments/aejyze/tip_show_sxhkd_keybindings_with_fuzzy_search/

  - [] find/build a helper widget for sxhkd, like [this](https://youtu.be/1QQps1qTgG4?t=221)

    - https://www.reddit.com/r/bspwm/comments/aejyze

    - https://github.com/fiskhest/sxhkd-helper-menu

- [x] Cursor
      https://www.reddit.com/r/bspwm/comments/v3e3ot/how_in_the_heck_do_you_set_your_cursor_size/

  - https://www.opendesktop.org/p/1366182
  - https://www.opendesktop.org/p/1356095
  - https://www.opendesktop.org/p/1358330
  - https://www.opendesktop.org/p/1197198
  - https://www.opendesktop.org/p/1295073

- [] Application launcher

  - [] Rofi

    - customize with this example library https://github.com/adi1090x/rofi

    - another exmple https://github.com/oldwomanjosiah/dots/blob/main/.config/rofi/center.rasi#L30

- [] Polybar
  https://github.com/iosmanthus/nixos-config/search?q=polybar
  https://github.com/ztlevi/dotty-nix/blob/76d9c61c54ae1f673d7c38cb76d7bc1bb024de3e/modules/themes/flat-remix.nix
  https://github.com/tomcur/nixos-configuration/blob/52ed0159c8afdce7b962a387efa888688830b332/config/home/cfg/polybar/default.nix
  https://github.com/tomcur/nixos-configuration/search?q=polybar
  https://github.com/wochap/nix-config/search?q=polybar
  https://github.com/iosmanthus/nixos-config/search?q=polybar

- [] Power menu handler

  - [] rofi power menu (see [here](https://github.com/karosamu/dotfiles/tree/dotfiles/bspwm))

- [x] Notifications

  - [x] [dusnt](https://github.com/dunst-project/dunst)
  - [] Theme dunst
    https://github.com/ericmurphyxyz/archrice/tree/master/.config/dunst

- [x] Screenshots

  - [x] flameshot

- [x] VSCode

  - [x] extensions
    - https://models.aminer.cn/codegeex/

- [] Git

  - [] git configuration name and email

  - [] git configuration GPG keys

    - https://github.com/danielfullmer/nixos-config/blob/master/home/default.nix

  - [] git configuration dependant on the folder (nix shell, nix env,[dir-env](https://github.com/direnv/direnv), [nix-direnv](https://github.com/nix-community/nix-direnv), [lorri](https://github.com/nix-community/lorri)?)

- [x] Partitions

  Resources I used (in decreasing order of relevance for my usecase)

  - https://grahamc.com/blog/erase-your-darlings
  - https://blog.lazkani.io/posts/nixos-on-encrypted-zfs
  - https://openzfs.github.io/openzfs-docs/Getting%20Started/NixOS/Root%20on%20ZFS/0-overview.html
  - https://xeiaso.net/blog/paranoid-nixos-2021-07-18
  - https://cnx.srht.site/blog/butter/index.html
  - https://elis.nu/blog/2020/05/nixos-tmpfs-as-root

- [x] Clam antivirus

  - [] The setup is not perfectly reproducible, since you need to manually run `sudo freshclam` once to initialize the database [issue](https://github.com/NixOS/nixpkgs/issues/187992)

- [x] Backup

  - ~~https://github.com/borgbackup/borg~~
  - https://github.com/restic/restic

  ### Backblaze setup

  1. Create an account on [backblaze.com](https://www.backblaze.com)
  1. Go to the account page
  1. In the section `account`>`My Settings`>`Enabled Products`, ensure that `B2 Cloud Storage` is the only option enabled.
  1. Go to the `B2 Cloud Storage`>`Buckets` panel
  1. Create a new bucket, say `bucket123`, this will be the bucket we write the restic backups to.

     1. There is no need to make the bucket public, so keep it private.
     1. Do enable the default encryption that backblaze offers.
     1. After creation, in the bucket lifecycle settings, ensure it is set to "keep all versions of the file".
     1. In the bucket Object Lock settings, turno on object locking for a duration that you deem appropriate.

  1. Now we need to create a new Application Key to give restic access only to this bucket. We could do this through the settings in `Account`>`App Keys`, but the UI does not offer permissions that are granular enough for us. Therefore we will use the API. The backblaze API docs are [here](https://www.backblaze.com/b2/docs/b2_authorize_account.html).
  1. Go to the backblaze settings and generate a new master key. You will need its secret value.
  1. Create an authorization session (found this link [here](https://www.backblaze.com/b2/docs/calling.html)):
     endpoint: `https://api.backblazeb2.com/b2api/v2/b2_authorize_account`
     headers:
     - Authorization: `Basic 12345` (where 12345 is the result of `echo "K1_KEY_ID:K1_KEY" | base64`)
       `curl https://api.backblazeb2.com/b2api/v2/b2_authorize_account -u "APPLICATION_KEY_ID:APPLICATION_KEY"`
  1. Create a key through the [b2_create_key](https://www.backblaze.com/b2/docs/b2_create_key.html) endpoint with these capabilities: `[ "listBuckets", "listFiles", "readFiles", "writeFiles" ]` (if you wanted, you should be able to remove the `listBuckets` capability, provided that in the NixOS config `services.restic.backups.<name>.initialize` is set to false). The Bucket ID can be found in the `B2 Cloud Storage`>`Buckets` page. `validDurationInSeconds` can be omitted to specify a key that never expires. `namePrefix` can also be omitted.
     The final request should look like this:

     ```bash
     curl -H "Authorization: ${ACCOUNT_AUTHORIZATION_TOKEN}" \
       -d "{
             \"accountId\":  \"${ACCOUNT_ID}\", \
             \"capabilities\":  ${CAPABILITIES}, \
             \"keyName\":  \"${KEY_NAME}\", \
             \"bucketId\":  \"${BUCKET_ID}\" \
         }" \
       "${API_URL}/b2api/v2/b2_create_key"
     ```

     Note the key ID and the key from the response, put them in the restic config.
     In my case, this means adding them to a file in `secretes/bugaboo/restic/<backup_name>.env`

  1. Re-generate a new master key

- [] Misc

  - [] https://github.com/abhilash26/searchbang

  - [] `curl https://wttr.in`

  - [x] https://github.com/lbgists/rain.sh

  - [x] linter https://github.com/kamadorueda/alejandra

  - [] https://unix.stackexchange.com/questions/632879

  - [] use lxappearance to apply theme and cursor

  - [] https://www.jwz.org/xscreensaver

  - [] cmus or ncmpcpp

  - [] shell, see nerdfonts.com ![](https://www.nerdfonts.com/assets/img/nerd-fonts-powerline-extra-terminal.png)

  - [x] https://github.com/Swordfish90/cool-retro-term

  - [] https://github.com/NvChad/NvChad

  - [x] boot.binfmt.emulatedSystems (x86_64-windows) ([video](https://youtu.be/0uixRE8xlbY?t=809))

  - [] https://fig.io/

  - [] Projects config

    - [] a python project
    - [] a java project
    - [x] a flutter project
    - [] a node project

  - [] [Ferdi](https://github.com/getferdi/ferdi)

- [] Hardening

  - https://github.com/konstruktoid/hardening (see especially the bats tests)
  - [Lynis](https://cisofy.com/downloads/lynis/)
  - [] https://madaidans-insecurities.github.io/guides/linux-hardening.html
  - [] Auditing
    - logs with [trillian](https://transparency.dev/#trillian)
  - [x] Secure DNS
    - [x] DNS over TLS (DoT) with stubby -> secure, but easier to block since it uses its own port, 853
    - [x] DNS over HTTPS (DoH) -> looks exactly like HTTPS traffic.
      - knot resolver
        `services.networking.kresd`
        https://www.knot-resolver.cz/
      - DNScrypt proxy

- [] Optional

  - [] Wayland

    - [] [compositor](https://www.reddit.com/r/unixporn/comments/tupqhj/hyprland_to_that_one_guy_who_wanted_me_to_make_a/)
      - bar
        - eww
          To get a unicode character given its unicode code point, use `nix-shell -p wl-clipboard --run 'echo -n -e "\uAAA" | wl-copy'` (where AAA is the code point, like "\ue036")
          - [] Include the icons in the font you use as the default one, otherwise you ease web browser fingerprinting since they can access the list of installed fonts and `UsefulReindeer` is quite unique 😉.
            https://github.com/flick0/dotfiles/tree/dreamy
            https://libreddit.garudalinux.org/img/1b9a77lsddoa1.png
            https://libreddit.garudalinux.org/img/mvh8ghc5ocoa1.png
            https://libreddit.garudalinux.org/r/unixporn/comments/11upz0c/neovim_astronvim_v3/
            https://libreddit.garudalinux.org/r/unixporn/comments/11u6pzp/i3wm_catppuccin_mocha_3/

- [] Browser
  - [] Searx
    - [] persist config cookie
      ```text
      language	en-US
      locale	en
      autocomplete	duckduckgo
      image_proxy	1
      method	POST
      safesearch	1
      theme	simple
      results_on_new_tab	0
      doi_resolver	oadoi.org
      simple_style	auto
      center_alignment	0
      disabled_plugins
      tokens
      maintab	on
      enginetab	on
      enabled_plugins	searx.plugins.oa_doi_rewrite,searx.plugins.vim_hotkeys
      query_in_title	1
      disabled_engines	wikipedia__general,lingva__general,qwant__general,qwant images__images,dailymotion__videos
      categories	general
      infinite_scroll	1
      enabled_engines	gitlab__it,unsplash__images
      ```
- [] https://github.com/untitaker/quickenv
- [] https://github.com/deviantfero/wpgtk

- [] merge vm-build.sh in rebuild.sh
- [] Investigate on [this](https://www.reddit.com/r/NixOS/comments/ni331w/comment/gyzz4uu)
- [] anbox (Android in a box)
- [] https://healthchecks.io

- [] https://github.com/nix-community/lanzaboote
  https://github.com/NickCao/flakes/blob/master/nixos/local/configuration.nix

- [] pokemons
  https://gitlab.com/phoneybadger/pokemon-colorscripts
  https://gitlab.com/phoneybadger/pokemon-colorscripts
  https://github.com/HRKings/pokemonsay-newgenerations
  https://github.com/yannjor/krabby

- restic https://github.com/NickCao/flakes/blob/master/nixos/local/configuration.nix#L211

  - [] https://felschr.com/blog/nixos-restic-backups/ (available in the [web archive](https://web.archive.org/web/20221209151134/https://felschr.com/blog/nixos-restic-backups))

  ```bash
    # cd to the path to backup
    cd /nix/persist/safe
    # generate the list of the files to include
    rg --files --hidden > /tmp/i
    # create the repo
    restic --repository-file /run/secrets/restic-repo -p /run/secrets/restic-password init
    # make the first backup
    restic --repository-file /run/secrets/restic-repo -p /run/secrets/restic-password --files-from-verbatim /tmp/i backup
  ```

  Option 2:

  ```
    #https://gitlab.com/felschr/nixos-config/-/blob/9fe366f34ce717340f3d791cdaf330ca5a71c3fd/services/restic/home-pc.nix#L102
    SRC=/nix/persist/safe/home/${config.username}/documents/Fonts
    SRC=$(realpath $SRC)
    DEST=/run/backupme
    rm -rf $DEST
    mkdir -p $DEST
    # Link the files, but respecting .gitignores since restic does not offer this option yet
    # See https://github.com/restic/restic/issues/1514
    ${pkgs.rsync}/bin/rsync \
    --archive --delete \
    --filter=':- .gitignore' \
    --link-dest=$SRC \
    $SRC/ $DEST
  ```

  memory usage during backup `~370MB`, on the receiving end the server kept consuming always `~14MB`
  first backup time: 8min, 47sec
  cpu usage: periodic small bursts of 1 second every ~5 seconds

  second backup, with nothing changed: 35 seconds, ~300MB of RAM
  cpu usage: high for these 30 seconds

  TODO: could add

  ```
    # # https://gitlab.com/felschr/nixos-config/-/blob/9fe366f34ce717340f3d791cdaf330ca5a71c3fd/modules/restic.nix
    # serviceConfig = {
    #   CPUWeight = 25;
    #   MemoryHigh = "50%";
    #   MemoryMax = "75%";
    #   IOWeight = 50;
    #   IOSchedulingClass = "idle";
    #   IOSchedulingPriority = 7;
    # };
  ```

- LUKS key management https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Key_management

- disk secure erasure
  https://web.archive.org/web/20190326142043/http://forum.notebookreview.com/threads/secure-erase-hdds-ssds-sata-nvme-using-hdparm-nvme-cli-on-linux.827525/

Future:

- https://github.com/NateBrune/silk-guardian
- https://objective-see.org/products/dnd.html
- https://github.com/dekuNukem/daytripper
- buskill

# https://aldoborrero.com/posts/2023/01/15/setting-up-my-machines-nix-style/
