# Devon

Server for my personal website

## Installing

1. Boot into the minimal_kexec installer (see `hosts/minimal_kexec`), ssh into it.
1. Copy the script `install.sh` from this directory, run it.

# Troubleshooting

- "Cannot connect to the Docker daemon at unix:///run/user/0/docker.sock. Is the docker daemon running?"
  Run `ln -sn /run/docker.sock /run/user/0/docker.sock`, then it works
