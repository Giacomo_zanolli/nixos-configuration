{ pkgs, ... }:

{
  boot = {
    supportedFilesystems = {
      zfs = true;
      vfat = true;
    };
    kernelParams = [
      "net.ifnames=0" # use predictable network interface names starting with eth0
    ];
    consoleLogLevel = 0;
    loader = {
      timeout = 1;
      grub = {
        enable = true;
        devices = [ "/dev/sda" ];
      };
    };
    tmp = {
      useTmpfs = true;
      cleanOnBoot = true;
    };
    initrd.kernelModules = [
      "dm-snapshot"
      "dm-cache"
      "nvme"
      "ata_piix"
      "uhci_hcd"
      "sd_mod"

      # Virtio (QEMU, KVM etc.) support.
      "virtio_net"
      "virtio_pci"
      "virtio_mmio"
      "virtio_blk"
      "virtio_scsi"
      "virtio_balloon"
      "virtio_console"
    ];
  };
}
