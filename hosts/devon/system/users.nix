{
  users = {
    users = {
      root = {
        hashedPassword = "!";
        openssh.authorizedKeys.keys = [
          (import ../../common/ssh.nix).bugabooPublicKey
        ];
      };
    };
    mutableUsers = false;
  };
}
