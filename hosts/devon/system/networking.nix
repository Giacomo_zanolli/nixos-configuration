{ config, ... }:

{
  # Use predictable interface names starting with eth0
  boot.kernelParams = [ "net.ifnames=0" ];

  networking = {

    firewall = {
      enable = true;
      allowedTCPPorts = [
        22 #ssh
      ];
      allowedUDPPorts = [ ];
    };
    hostName = config.hostname;

    wireless.enable = false;
    useDHCP = false;
    hostId = "df76a13f";
    defaultGateway = "207.180.228.1";
    # Use Quad9's DNS server
    nameservers = [ "9.9.9.9" "8.8.8.8" "1.1.1.1" ];
    interfaces.eth0 = {
      ipv4.addresses = [
        {
          address = "207.180.228.112";
          prefixLength = 24;
        }
      ];
      ipv6.addresses = [
        {
          address = "2a02:c207:2110:9610::1";
          prefixLength = 64;
        }
      ];
    };
  };
}
