{ lib, ... }:

{
  boot = {
    initrd.postMountCommands =
      let
        scriptContents = ''
          if zfs rollback -r rpool/root@blank; then
            echo "It's a new day for /"
          else
            echo "/ was NOT RESTORED!! Be careful in there"
          fi
        '';
      in
      lib.mkAfter scriptContents;
    supportedFilesystems.zfs = true;
    zfs.allowHibernation = true; #Set to true IF AND ONLY IF swap is NOT on ZFS
    zfs.forceImportRoot = false;
  };
}
