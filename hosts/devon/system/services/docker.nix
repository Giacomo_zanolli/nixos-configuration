{ pkgs, ... }:

{
  virtualisation = {
    docker = {
      enable = true;
      autoPrune.enable = true;
      rootless = {
        enable = true;
        setSocketVariable = true;
      };
    };
  };
  users.users.root.extraGroups = [ "docker" ];
  systemd.services.my-website = {
    description = "Launch the website";
    wantedBy = [ "multi-user.target" ];
    after = [ "docker.service" ];
    serviceConfig = {
      ExecStart = "${pkgs.docker}/bin/docker compose -f /root/website/docker-compose.yaml up";
      Restart = "always";
    };
  };
}
