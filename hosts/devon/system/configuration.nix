{ pkgs, ... }:

{
  imports = [
    ./boot.nix
    ./hardware-configuration.nix
    ./impermanence.nix
    ./networking.nix
    ./packages.nix
    ./services/docker.nix
    ./services/minio.nix
    ./services/ssh.nix
    ./users.nix
    ./zfs.nix
  ];

  virtualisation.docker = {
    enableOnBoot = true;
    autoPrune.enable = true;
  };

  time.timeZone = "Europe/Rome";

  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    package = pkgs.nixVersions.stable;
  };
  documentation.nixos.enable = false;

  system.stateVersion = "24.11";
}

