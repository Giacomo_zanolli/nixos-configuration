#! /usr/bin/env nix-shell
#! nix-shell -i bash -p git nixUnstable

#The above lines allow us to run the following code as a bash script,
#inside an environment created by nix-shell where git and nix flakes are
#available.

nixosHostName=devon
boot_partition_label=boot

colorPrint() {
  echo -e "$(tput setaf 6)$1$(tput sgr0)"
}

errorPrint() {
  echo -e "$(tput setaf 1)$1$(tput sgr0)"
}

#If the previous command failed, exits
checkSuccess() {
  if [ $? -ne 0 ]; then
    errorPrint "Something went wrong"
    exit 1
  fi
}

prompt_confirm() {
  while true; do
    read -r -p "${1:-Continue?} [y/n]: " reply
    case $reply in
    [yY][eE][sS] | [yY])
      return 0
      ;;
    [nN][oO] | [nN])
      return 1
      ;;
    *) printf " \033[31m %s \n\033[0m" "invalid input" ;;
    esac
  done
}

#Select device to format
lsblk -o NAME,MAJ:MIN,RM,SIZE,RO,TYPE,LABEL,MOUNTPOINTS
while
  colorPrint "-- Choose the disk to format"
  read disk
  regex="^${disk}\+\s\+[0-9]\+:[0-9]\+\s\+[0-9]\+\s\+[0-9]\+\([,.][0-9]\+\)\?\w\s\+[0-9]\+\s\+disk\b"
  ! (lsblk | grep -q $regex)
do
  colorPrint "-- Invalid disk ($disk)"
  lsblk -o NAME,MAJ:MIN,RM,SIZE,RO,TYPE,LABEL,MOUNTPOINTS
done

colorPrint "You are about to choose disk $disk"
colorPrint
lsblk | grep $regex
colorPrint
colorPrint "The contents of the disk are about to be completely erased. Are you sure you want to proceed?"
if ! prompt_confirm; then
  errorPrint "Operation cancelled, terminating"
  exit
fi
colorPrint "Are you ABSOLUTELY sure this is the right one?"
if ! prompt_confirm; then
  errorPrint "Operation cancelled, terminating"
  exit
fi

devpath="/dev/${disk}"

colorPrint "Unmounting disk"
umount --recursive $devpath 2>/dev/null

colorPrint "Deleting everything from the drive"
sgdisk --zap-all $devpath

colorPrint "Partitioning the drive"
sgdisk \
  --new 1:1MiB:+3MiB --typecode 1:EF02 \
  --new 2:0:+2G --typecode 2:8300 --change-name 2:"$boot_partition_label" \
  --new 3:0:0 --typecode 3:8300 \
  $devpath
checkSuccess
sgdisk -C $devpath
checkSuccess
parted $devpath set 1 bios_grub on
checkSuccess
parted $devpath disk_set pmbr_boot on

partprobe

udevadm settle --timeout=5 --exit-if-exists=${devpath}1
udevadm settle --timeout=5 --exit-if-exists=${devpath}2
udevadm settle --timeout=5 --exit-if-exists=${devpath}3

colorPrint "Formatting the boot partition"
mkfs.vfat -F32 -n "$boot_partition_label" ${devpath}2

colorPrint "Creating ZFS pool"
zpool create -f \
  -O mountpoint=none \
  -O compression=lz4 \
  -o ashift=12 \
  -O acltype=posixacl \
  -O xattr=sa \
  rpool ${devpath}3
checkSuccess

colorPrint "Creating root dataset (ZFS)"
zfs create -o mountpoint=legacy rpool/root
checkSuccess
colorPrint "Creating the /nix dataset"
zfs create -o mountpoint=legacy rpool/nix
checkSuccess

colorPrint "Mounting root dataset to /mnt"
mount -t zfs rpool/root /mnt
checkSuccess
colorPrint "Mounting nix dataset to /mnt/nix"
mkdir /mnt/nix
mount -t zfs rpool/nix /mnt/nix
checkSuccess
colorPrint "Mounting boot to /mnt/boot"
mkdir /mnt/boot
mount /dev/disk/by-label/$boot_partition_label /mnt/boot
checkSuccess

colorPrint "Cloning config"
git clone https://gitlab.com/Giacomo_zanolli/nixos-configuration.git /mnt/etc/nixos
checkSuccess

colorPrint "Creating persistent directories"
persistentPath=/nix/persist
mkdir -p /mnt$persistentPath
mkdir -p /mnt$persistentPath/unsafe

colorPrint "Moving config to final destination in ${persistentPath}/config"
nixosConfigFolder=$persistentPath/config
mntNixosFolder=/mnt$nixosConfigFolder
mkdir -p /mnt$persistentPath
mv /mnt/etc/nixos $mntNixosFolder
colorPrint "Creating symlink in /etc"
ln -s $nixosConfigFolder /mnt/etc/nixos
colorPrint "Creating snapshot of blank root"
zfs snapshot rpool/root@blank

colorPrint "Fixing permissions on config (just for install)"
rm /mnt/etc/nixos
cp -r $mntNixosFolder /mnt/etc/nixos
chown -R root:root /mnt/etc/nixos

colorPrint "Installing the OS"

# Install the OS, using also the binary cache of bugaboo if available
nixos-install --no-root-password \
  --root /mnt \
  --flake /mnt/etc/nixos#$nixosHostName

installationReturnValue=$?

if [ $installationReturnValue -ne 0 ]; then
  echo "----------Unsuccessful installation----------"
  echo "Exiting"
  exit 1
fi

colorPrint "Ensuring all data has been written to the disks"
sync
colorPrint "Unmounting everything"
umount -R /mnt
colorPrint "Exporting the pool"
zpool export rpool

colorPrint "-----------Everything done! Rebooting-----------"

#shutdown -r now
