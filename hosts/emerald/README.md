# Storage disk setup:

1. Identify the disk in `lsblk` (say it's sdb)
1. Use `gdisk` -or whatever- to create a GPT partition table with one partition
1. Encrypt the disk `cryptsetup --type luks2 luksFormat /dev/sdb1`
1. Label the disk `cryptsetup config /dev/sdb1 --label c-leading-stoat`
1. Open encrypted disk `cryptsetup open /dev/sdb1 leading-stoat`
1. Format btrfs `mkfs.btrfs --data dup --label btrfs-test /dev/mapper/leading-stoat`
1. Add the backup volume to btrfs `mkdir /btrfs`
1. `mount /dev/mapper/leading-stoat /btrfs/`
1. `btrfs subvolume create /btrfs/backup`
1. `umount /btrfs`
1. `rmdir /btrfs`

# Minio setup:

1.  Create the new bucket
1.  Create an access key with this key access policy (change the bucket name appropriately):

    ```json
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": ["s3:PutObject", "s3:DeleteObject", "s3:GetObject"],
          "Resource": ["arn:aws:s3:::BUCKETNAME/*"]
        },
        {
          "Effect": "Allow",
          "Action": ["s3:GetBucketLocation", "s3:ListBucket"],
          "Resource": ["arn:aws:s3:::BUCKETNAME"]
        }
      ]
    }
    ```
