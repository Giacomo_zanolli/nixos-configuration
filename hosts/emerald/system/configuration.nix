{ pkgs, ... }:

{
  imports = [
    ./boot.nix
    ./btrfs.nix
    ./btrfs_data.nix
    ./hardware-configuration.nix
    ./impermanence.nix
    ./init/initrd.nix
    ./networking.nix
    ./packages.nix
    ./sops.nix
    ./ssh.nix
    ./users.nix
    ./services/docker.nix
    ./services/godns.nix
  ];

  time.timeZone = "Europe/Rome";

  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    package = pkgs.nixVersions.stable;
  };
  documentation.nixos.enable = false;

  system.stateVersion = "24.11";
}

