{ config, pkgs, ... }:

{
  systemd.services.decrypt-data-disk =
    let
      subvolName = "backup";
      mountPoint = "/backup";
      device = "leading-stoat";
      encryptedPrefix = "c-";
    in
    {
      description = "Decrypt encrypted data disk on startup and mount the btrfs subvolume";

      before = [ "multi-user.target" "docker.service" ];
      wantedBy = [ "multi-user.target" ];
      after = [
        "dev-disk-byx2dlabel${encryptedPrefix}${device}.device"
        "run-secrets.d.mount" #Secrets are available
      ];

      script = ''
        # Wait for disks to come online.
        # This should be unnecessary given the 'after' directive, but better be sure.
        echo "Waiting for ${device} to come online"
        DEVICE_TIMEOUT=30
        ELAPSED=0

        while ! test -e "/dev/disk/by-label/${encryptedPrefix}${device}"; do
          if [ "$ELAPSED" -ge "$DEVICE_TIMEOUT" ]; then
            echo "Timeout reached: ${device} did not come online after $DEVICE_TIMEOUT seconds."
            exit 0
          fi
          sleep 5
          echo "Still waiting"
          ELAPSED=$((ELAPSED + 5))
        done

        echo "Device ${device} is now online"

        if [ -f /dev/mapper/${device} ]; then
          echo "Device ${device} is already present, skipping it"
        else
          echo "Opening ${device}"
          cryptsetup open \
            --key-file=${config.sops.secrets."crypt-${device}".path} \
            "/dev/disk/by-label/${encryptedPrefix}${device}" ${device}
        fi

        echo "Mounting subvol ${subvolName} of ${device}"

        mkdir -p ${mountPoint}
        mount -t btrfs \
          -o nodev,noexec,noatime,subvol=backup,compress=zstd \
          /dev/mapper/${device} ${mountPoint}
      '';

      preStop = ''
        echo "Syncing data to disk, just in case"
        sync
        echo "Unmounting ${subvolName}"
        umount ${mountPoint}
        echo "Closing the disk"
        cryptsetup close ${device}
      '';

      path = with pkgs;[
        cryptsetup
        btrfs-progs
        util-linux
      ];

      serviceConfig = {
        Type = "oneshot";
        TimeoutStopSec = "90";
        RemainAfterExit = "yes";
      };
    };
}
