{ config, ... }:

{
  sops = {
    defaultSopsFile = ../../../.sops.yaml;

    age = {
      keyFile = "/persist/.sops.key";
      generateKey = false;
      #See https://github.com/Mic92/sops-nix/issues/167
      sshKeyPaths = [ ];
    };

    #See https://github.com/Mic92/sops-nix/issues/167
    gnupg.sshKeyPaths = [ ];

    secrets = {
      crypt-leading-stoat = {
        format = "binary";
        sopsFile = ../../../secrets/emerald/leading-stoat.key;
      };
    };
  };
}
