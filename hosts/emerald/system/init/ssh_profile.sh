#! /bin/bash

# Disable Ctrl+C, Ctrl+Z, Ctrl+\, and Ctrl+D
trap '' SIGINT SIGTSTP SIGQUIT SIGTTOU

echo '
          █   █ █▀▀ █   █▀▀ ▄▀▀▄ █▄ ▄█ █▀▀   █▀▄ ▄▀▄ ▄▀▀ █ █
          █   █ █▀▀ █   █   █  █ █ ▀ █ █▀▀   █▀▄ █▄█ █   █▀▄
          ▀▄▀▄▀ █▄▄ █▄▄ █▄▄ ▀▄▄▀ █   █ █▄▄   █▄▀ █ █ ▀▄▄ █ █
'
echo "You are on emerald"

# Adapted from /nix/store/zk6311z4nb30g4nwbahdyrg4hbf0551l-extra-utils/bin/cryptsetup-askpass

# Wait for /crypt-ramfs/device to appear
ABSENT=0
DOTS=""
DOTS_N=0
while [ "$ABSENT" == 0 ]; do
  DOTS_N=$((DOTS_N + 1))
  DOTS="$DOTS."
  if [ $DOTS_N -eq 4 ]; then
    DOTS_N=0
    DOTS=""
  fi
  printf "Waiting for LUKS to ask for the passphrase%s\r" "$DOTS"
  sleep 1

  if [ ! -e /crypt-ramfs/device ]; then
    ABSENT=0
  else
    ABSENT=1
  fi
done
printf "\n"

# Read which encrypted device we are trying to unlock
device=$(cat /crypt-ramfs/device)

# Check it is the one we expect
if [ "$device" != "/dev/disk/by-label/cryptroot" ]; then
  printf "Something is off here. The script asked for the passphrase to %s" "$device"
  printf "Terminating"
  poweroff
  exit 1
fi

# Get the passphrase and check it
PASSPHRASE_CORRECT=1
MAX_LEN=500 #Hope I'll never need to have a passphrase longer than this
while [ $PASSPHRASE_CORRECT != 0 ]; do

  printf "Enter passphrase for %s: " "$device"
  read -r -s -n $MAX_LEN passphrase
  echo

  if [ -z "$passphrase" ]; then
    echo "Passphrase cannot be empty"
    continue
  fi

  if ! echo "$passphrase" | grep -q '^[[:alnum:]]*$'; then
    echo "Only alphanumeric chars"
    continue
  fi

  printf "%s" "$passphrase" | cryptsetup open --test-passphrase "$device" -d -
  PASSPHRASE_CORRECT=$?
done

# Set this to let the other cryptsetup call get the passphrase
rm /crypt-ramfs/device
printf "%s" "$passphrase" >/crypt-ramfs/passphrase
unset passphrase

printf "Ready to boot\n"

printf "

                         ▄███████████████▄
                         █████████████████
                         ████████████▀  ██
                         ██▀▀██████▀  ▄███
                         ██▄  ▀██▀  ▄█████
                         ████▄    ▄███████
                         ██████▄▄█████████
                         ▀███████████████▀


"

exit
