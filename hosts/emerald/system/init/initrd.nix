{ config, pkgs, lib, ... }:

{
  boot.initrd = {
    kernelModules = [
      "dm-snapshot" # For cryptsetup
      "dm-cache" # For cryptsetup
      "ext4" # Since the ssh keys are on an ext4 file system
      "r8169" # For the network interface
      "aesni_intel" # Hardware accelerated cryptoraphy
      "cryptd" # Hardware accelerated cryptoraphy
    ];
    availableKernelModules = [
      "xhci_pci" #usb
      "usb_storage" #usb
      "ahci"
      "rtsx_usb_sdmmc"
      #optional?
      "ata_piix"
      "uhci_hcd"
      "ehci_pci"
      "sd_mod"
      "sr_mod"
    ];

    luks = {
      devices."cryptroot" = {
        device = "/dev/disk/by-label/cryptroot";
        preLVM = true;
        fallbackToPassword = true;
      };
      reusePassphrases = false;
    };

    network = {
      enable = true;
      ssh = {
        enable = true;
        port = 2222;
        ignoreEmptyHostKeys = true;
        authorizedKeys = config.users.users.root.openssh.authorizedKeys.keys;
        extraConfig = ''
          Banner /etc/ssh/sshd-banner
          ForceCommand /root/.profile/bin/ssh_profile
        '';
      };
    };

    preLVMCommands = lib.mkBefore (builtins.readFile ./initrd.sh);

    extraFiles = {
      "/root/.profile".source = pkgs.writeShellApplication {
        name = "ssh_profile";
        runtimeInputs = with pkgs; [ bash ];
        text = builtins.readFile ./ssh_profile.sh;
      };
      "/etc/ssh/sshd-banner".source = pkgs.writeTextFile {
        name = "ssh_banner";
        text = builtins.readFile ./ssh_banner.txt;
      };
    };
  };
}
