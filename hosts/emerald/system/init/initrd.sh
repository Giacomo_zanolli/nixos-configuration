#! /bin/sh

echo "Creating /tmp_keys"
mkdir -p /tmp_keys
echo "Creating /etc/ssh"
mkdir -p /etc/ssh

# Waits for a filesystem entity (file, directory, symlink) to appear in the specified location.
# Useful as it may be necessary to wait a bit for devices to be initialized.
# See: https://github.com/NixOS/nixpkgs/issues/98741
wait_for() {
  P=$1
  I=0

  [ ! -e $P ]
  ABSENT=$?

  echo "Waiting for $P to appear"
  while [ $ABSENT -eq 0 ]; do
    [ ! -e $P ]
    ABSENT=$?

    I=$((I + 1))
    MAX=60
    echo -n -e "Attempt $I/$MAX\r"

    if [ $I -gt $MAX ]; then
      echo -e "\nTimed out waiting for $P"
      exit 1
    fi
    sleep 1
  done

  echo -n -e "                                                    \r"

  echo -e "Found $P"
}

wait_for /tmp_keys
wait_for /dev/disk/by-label/initk

echo "Mount partition with the hostkeys, and link them in"
mount /dev/disk/by-label/initk /tmp_keys

echo "Checking key files presence and copying them"
KEY_FILES="ssh_host_ed25519_key ssh_host_rsa_key"
for F in ${KEY_FILES}; do
  PRIVATE_KEY_FILE="/tmp_keys/$F"
  # If the file does not exist
  if [[ ! -e "$PRIVATE_KEY_FILE" ]]; then
    echo "Missing private key $PRIVATE_KEY_FILE in root of initk partition"
  fi
  # Initrd is in RAM and the swap is encrypted. This should be reasonably safe
  cp $PRIVATE_KEY_FILE /etc/ssh
  PUBLIC_KEY_FILE="${PRIVATE_KEY_FILE}.pub"
  if [[ ! -e "$PUBLIC_KEY_FILE" ]]; then
    echo "Missing public key $PUBLIC_KEY_FILE in root of initk partition"
  fi
  # Initrd is in RAM and the swap is encrypted. This should be reasonably safe
  cp $PUBLIC_KEY_FILE /etc/ssh
done

wait_for /dev/disk/by-label/cryptroot

echo 'If you cannot reach this machine, maybe you are missing the network driver in the kernel modules'
