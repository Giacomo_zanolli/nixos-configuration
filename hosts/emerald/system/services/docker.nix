{ pkgs, config, ... }:

{
  virtualisation = {
    docker = {
      enable = true;
      autoPrune.enable = true;
    };
  };
  users.users.root.extraGroups = [ "docker" ];
  systemd.services.my-web-services = {
    description = "Launch the web services";
    wantedBy = [ "multi-user.target" ];
    after = [
      "docker.service"
      "sops-nix.service"
      "decrypt-data-disk.service"
    ];
    script = ''
      mkdir -p /backup/minio
      chown -R 1001:1001 /backup/minio
      ${pkgs.docker}/bin/docker compose -f /etc/my-web-services/docker-compose.yaml up
    '';
    serviceConfig = {
      Restart = "always";
      EnvironmentFile = config.sops.secrets.myweb.path;
    };
  };

  environment.etc."my-web-services/docker-compose.yaml" = {
    source = ./docker-compose.yaml;
  };

  sops.secrets.myweb = {
    format = "dotenv";
    sopsFile = ../../../../secrets/emerald/services/web-services.env;
  };
}
