{ config, pkgs, ... }:

{
  systemd.timers = {
    "godns" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*:0/2"; # every 2 minutes
        RandomizedDelaySec = "1m";
        Unit = "godns.service";
      };
    };
  };

  systemd.services.godns = {
    description = "Golang Dynamic DNS Client";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      Type = "simple";
      User = config.users.users.godns.name;
      Group = config.users.groups.godns.name;

      NoNewPrivileges = true;
      SystemCallArchitectures = "native";
      RestrictAddressFamilies = [ "AF_UNIX" "AF_INET" "AF_INET6" "AF_NETLINK" ];
      RestrictNamespaces = true;
      RestrictRealtime = true;
      RestrictSUIDSGID = true;
      ProtectControlGroups = true;
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      LockPersonality = true;
      PrivateTmp = true;
      PrivateDevices = true;
      PrivateUsers = true;
      RemoveIPC = true;
    };
    script = ''
      ${pkgs.godns}/bin/godns -c ${config.sops.secrets."godns-config.json".path}
    '';
  };

  sops.secrets = {
    "godns-config.json" = {
      key = ""; # The whole file
      format = "json";
      owner = config.users.users.godns.name;
      sopsFile = ../../../../secrets/emerald/services/godns/config.json;
    };
  };

  users = {
    users = {
      godns = {
        isSystemUser = true;
        group = config.users.groups.godns.name;
      };
    };
    groups = {
      godns = { };
    };
  };
}
