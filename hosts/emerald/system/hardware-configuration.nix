{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.availableKernelModules = [
    "ata_piix"
    "uhci_hcd"
    "virtio_pci"
    "virtio_scsi"
    "sd_mod"
  ];

  fileSystems = {
    "/" = {
      device = "/dev/mapper/vg-btrfspart";
      fsType = "btrfs";
      options = [
        "subvol=root"
        "compress=zstd"
        "noautodefrag"
        "space_cache=v2"
        "ssd"
        "discard"
      ];
    };
    "/boot" = {
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
      options = [ "nodev" "noatime" ];
    };
    "/nix" = {
      device = "/dev/mapper/vg-btrfspart";
      fsType = "btrfs";
      options = [
        "subvol=nix"
        "compress=zstd"
        "noautodefrag"
        "space_cache=v2"
        "ssd"
        "discard"
        "nodev"
        "noatime"
      ];
    };
    "/persist" = {
      device = "/dev/mapper/vg-btrfspart";
      fsType = "btrfs";
      options = [
        "subvol=persist"
        "compress=zstd"
        "noautodefrag"
        "space_cache=v2"
        "ssd"
        "discard"
        "nodev"
        "noatime"
      ];
      neededForBoot = true;
    };
  };

  swapDevices = [
    {
      device = "/dev/disk/by-label/swap";
    }
  ];

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.enableRedistributableFirmware = true;
}
