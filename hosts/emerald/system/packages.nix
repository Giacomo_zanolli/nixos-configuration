{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    wget
    git
    btop
  ];

  programs.bash = {
    completion.enable = true;
    enableLsColors = true;
  };
}
