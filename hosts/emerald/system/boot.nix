{ pkgs, ... }:

{
  boot = {
    supportedFilesystems = {
      btrfs = true;
      vfat = true;
    };
    kernelParams = [
      "net.ifnames=0" # use predictable network interface names starting with eth0
    ];
    consoleLogLevel = 0;
    loader = {
      efi.canTouchEfiVariables = true;
      timeout = 3;
      grub = {
        enable = true;
        useOSProber = false;
        efiSupport = true;
        device = "nodev";
        configurationLimit = 50;
      };
    };
    tmp = {
      useTmpfs = true;
      cleanOnBoot = true;
    };
    initrd.kernelModules = [
      "nvme"
      "ata_piix"
      "uhci_hcd"
      "sd_mod"

      # Virtio (QEMU, KVM etc.) support.
      "virtio_net"
      "virtio_pci"
      "virtio_mmio"
      "virtio_blk"
      "virtio_scsi"
      "virtio_balloon"
      "virtio_console"
    ];
  };
}
