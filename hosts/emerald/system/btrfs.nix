{ lib, ... }:

{
  boot.initrd.postDeviceCommands =
    let
      days = "7";
    in
    lib.mkAfter ''
      TMPDIR=/btrfs_tmp
      mkdir $TMPDIR
      mount /dev/mapper/vg-btrfspart $TMPDIR
      if [[ -e $TMPDIR/root ]]; then
        echo "Deleting old root subvols"
        btrfs subvolume delete $TMPDIR/root/srv
        btrfs subvolume delete $TMPDIR/root/var/lib/portables
        btrfs subvolume delete $TMPDIR/root/var/lib/machines
        btrfs subvolume delete $TMPDIR/root/var/tmp

        echo "Creating read only snapshot of old root"
        btrfs subvolume create $TMPDIR/old_roots || true
        TIMESTAMP=$(date --date="@$(stat -c %Y $TMPDIR/root)" "+%Y-%m-%-d_%H:%M:%S")
        # btrfs subvolume snapshot -r $TMPDIR/root "$TMPDIR/old_roots/$TIMESTAMP"


        mv $TMPDIR/root "$TMPDIR/old_roots/$TIMESTAMP"
        btrfs property set -ts "$TMPDIR/old_roots/$TIMESTAMP" ro true

        echo "Restoring root to blank state"
        btrfs subvolume create $TMPDIR/root
      fi

      echo "Deleting snapshots older than ${days} days"
      for i in $(find $TMPDIR/old_roots/ -maxdepth 1 -mtime +${days}); do
        echo "Cleaning snapshot $i"
        rm -rf "$i"
      done
      
      echo "Cleaning up"
      umount $TMPDIR
      rmdir $TMPDIR

      echo "Done. It's a new day for /"
    '';
  boot.supportedFilesystems.btrfs = true;
  services.btrfs.autoScrub = {
    enable = true;
    interval = "weekly";
  };
}
