{
  environment.persistence = {
    "/persist/unsafe" = {
      hideMounts = true;
      directories = [
        "/var/lib/docker"
        "/var/lib/nixos"
        "/var/log"
        "/etc/nixos" # Nixos config
      ];
      files = [
        "/etc/ssh/ssh_host_rsa_key"
        "/etc/ssh/ssh_host_rsa_key.pub"
        "/etc/ssh/ssh_host_ed25519_key"
        "/etc/ssh/ssh_host_ed25519_key.pub"
      ];
    };
  };
}
