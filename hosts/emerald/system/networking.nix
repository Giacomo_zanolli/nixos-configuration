{
  # Use predictable interface names starting with eth0
  boot.kernelParams = [ "net.ifnames=0" ];

  networking = {
    hostName = "emerald";
    hostId = "c86be69b";
    wireless.enable = false;

    firewall = {
      enable = true;
      allowedTCPPorts = [
        22 #ssh
      ];
      allowedUDPPorts = [ ];
    };
  };
}
