#! /usr/bin/env nix-shell
#! nix-shell -i bash -p git nix sops

#The above lines allow us to run the following code as a bash script,
#inside an environment created by nix-shell where git and nix flakes are
#available.

nixosHostName=cetus
# These are the partition labels we will use.
# In the configs, we use /dev/disk/by-label/ to refer to these.
# It is therefore important that they are unique across all the
# disks that are connected to the computer.
boot_partition_label="nixboot"
swap_partition_label="nixswap"
initrd_keys_partition_label="initk"

{ # Having all the code inside these braces prevents execution if the script was partially downloaded
  # Stop if a command fails
  set -e

  colorPrint() {
    echo -e "$(tput setaf 6)$1$(tput sgr0)"
  }

  errorPrint() {
    echo -e "$(tput setaf 1)$1$(tput sgr0)"
  }

  bannerPrint() {
    echo -e "$(tput setaf 3)$1$(tput sgr0)"
  }

  colorPrint "Welcome. this script will let you set up a new machine with NixOS.\n"

  colorPrint "Building for the following hostname: $nixosHostName"

  #Ensure we are running as root
  if [ $(id -u) -ne 0 ]; then
    errorPrint "Please run as root (you can use 'sudo su' to get a shell)"
    exit 1
  fi

  if [ ! -d /sys/firmware/efi/ ]; then
    errorPrint "It seems you did not boot via UEFI. Exiting"
    exit 1
  fi

  if [ $(modprobe --dry-run efivars) -ne 0 ]; then
    errorPrint "It seems you did not boot via UEFI (could not load efivars). Exiting"
    exit 1
  fi

  RANDOMNESS=$(cat /proc/sys/kernel/random/entropy_avail)
  if [ $RANDOMNESS -lt 256 ]; then
    echo "Not enough entropy in the system. Plug some drives, generate some network traffic, use the keyboard"
    exit 1
  fi

  prompt_confirm() {
    while true; do
      read -r -p "${1:-Continue?} [y/n]: " reply
      case $reply in
      [yY][eE][sS] | [yY])
        echo "y"
        return 0
        ;;
      [nN][oO] | [nN])
        echo "n"
        return 0
        ;;
      *) ;;
      esac
    done
  }

  colorPrint "Enter the path to the file containing the SOPS provate key for cetus"
  read -e SOPS_KEYFILE_PATH
  if [ ! $(grep "^AGE-SECRET-KEY-" $SOPS_KEYFILE_PATH) ]; then
    errorPrint "This does not look like a SOPS keyfile"
    exit 1
  fi
  if ! grep -q "age1sca3mh5axwrpsadnnmcz2742t95se8ejqlwpac74rf8mugwqd9wq6379z7" "$SOPS_KEYFILE_PATH"; then
    errorPrint "This does not look like the keyfile for cetus"
    exit 1
  fi
  TMP_SOPS_PATH=/run/cetus_sops_keyfile
  cp $SOPS_KEYFILE_PATH $TMP_SOPS_PATH
  chown root:root $TMP_SOPS_PATH
  chmod 400 $TMP_SOPS_PATH

  colorPrint "Getting disk encryption key"
  curl -L -o sops_encr https://gitlab.com/Giacomo_zanolli/nixos-configuration/-/raw/main/secrets/cetus/luks_os.key
  colorPrint "Opening disk encryption key"
  #Could use SOPS_AGE_KEY_FILE=$TMP_SOPS_PATH, but it seems to be broken
  mkdir -p /root/.config/sops/age
  rm /root/.config/sops/age/keys.txt || true
  ln -s $TMP_SOPS_PATH /root/.config/sops/age/keys.txt
  #Check that we can decrypt the files
  sops --decrypt sops_encr >/dev/null
  luksPassword=$(sops --decrypt sops_encr)

  #Select device to format
  lsblk -o NAME,MAJ:MIN,RM,SIZE,RO,TYPE,LABEL,MOUNTPOINTS
  while
    colorPrint "-- Choose the disk to format"
    read disk
    regex="^${disk}\+\s\+[0-9]\+:[0-9]\+\s\+[0-9]\+\s\+[0-9]\+\([,.][0-9]\+\)\?\w\s\+[0-9]\+\s\+disk\b"
    ! (lsblk | grep -q $regex)
  do
    colorPrint "-- Invalid disk ($disk)"
    lsblk -o NAME,MAJ:MIN,RM,SIZE,RO,TYPE,LABEL,MOUNTPOINTS
  done

  colorPrint "You are about to choose disk $disk"
  colorPrint
  lsblk | grep $regex
  colorPrint
  colorPrint "The contents of the disk are about to be completely erased. Are you sure you want to proceed?"
  if [ "$(prompt_confirm)" != "y" ]; then
    errorPrint "Operation cancelled, terminating"
    exit
  fi
  colorPrint "Are you ABSOLUTELY sure this is the right one?"
  if [ "$(prompt_confirm)" != "y" ]; then
    errorPrint "Operation cancelled, terminating"
    exit
  fi

  WIPE_DISK=$(prompt_confirm "Do you also want to securely wipe the hard disk? This takes quite a long time")

  colorPrint "Checking if any existing partition labels conflicts with ours"
  PARTITION_LABELS=($boot_partition_label $swap_partition_label $initrd_keys_partition_label)

  # Get the list of all disks (excluding the specified disk)
  OTHER_DISKS=$(lsblk -nd -o NAME | grep -v "$(basename "$disk")")
  for OTHER_DISK in $OTHER_DISKS; do
    readarray -t CURR_PARTITIONS <<<$(lsblk -o LABEL /dev/$OTHER_DISK | tail -n +2 | grep -v '^$')
    for CURR_PART_LABEL in "${CURR_PARTITIONS[@]}"; do
      if [[ " ${PARTITION_LABELS[@]} " =~ " $CURR_PART_LABEL " ]]; then
        errorPrint "Partition with label '$CURR_PART_LABEL' found on disk '/dev/$OTHER_DISK'."
        exit 1
      fi
    done
  done
  colorPrint "No conflict found"

  colorPrint "--- Hic sunt leones. From here, we start partitioning ---"

  #Give the user a little time to ctrl+c
  sleep 2

  devpath="/dev/${disk}"

  colorPrint "Unmounting the selected disk"
  umount --recursive $devpath 2>/dev/null || true

  colorPrint "Wiping the disk"
  wipefs -af $devpath
  zpool labelclear -f $devpath || true
  blkdiscard -f $devpath

  if [ $WIPE_DISK = "y" ]; then
    colorPrint "Securely deleting the contents of the disk. This should take a while"
    shred --verbose --random-source=/dev/urandom -n4 $devpath
  fi

  colorPrint "---                Alea iacta est                     ---"
  colorPrint "---             Previous data is gone                 ---"

  colorPrint "The disk will be partitioned as follows:"
  colorPrint "|----------------------------------------------------------------------------------------|"
  colorPrint "|$boot_partition_label|$initrd_keys_partition_label| LUKS encrypted                                                           |"
  colorPrint "|512MB  |2MB  |                                                                          |"
  colorPrint "|----------------------------------------------------------------------------------------|"
  colorPrint "|///////|/////| LVM                                                                      |"
  colorPrint "|///////|/////|                                                                          |"
  colorPrint "|----------------------------------------------------------------------------------------|"
  colorPrint "|///////|/////| ZFS pool                                                         |swap   |"
  colorPrint "|///////|/////|                                                                  |8GB    |"
  colorPrint "|----------------------------------------------------------------------------------------|"
  colorPrint "|///////|/////| /         | /nix                                                 |///////|"
  colorPrint "|///////|/////| (amnesic) | (store and persistence)                              |///////|"
  colorPrint "|----------------------------------------------------------------------------------------|"

  colorPrint "Partitioning the drive"
  sgdisk -C \
    -n 1:0:+512MiB --typecode 1:ef00 --change-name 1:$boot_partition_label \
    -n 2:0:+2MiB --typecode 2:8300 --change-name 2:$initrd_keys_partition_label \
    -n 3:0:0 --typecode 3:8309 --change-name 3:luks \
    $devpath

  # Update kernel partition table
  partprobe

  #Mark the first partition as bootable
  parted $devpath -- set 1 esp on

  sync

  colorPrint "Formatting the EFI partition"
  mkfs.fat -F32 -n $boot_partition_label ${devpath}1

  colorPrint "Formatting the initrd ssh keys partition"
  shred --zero --random-source=/dev/urandom -n4 ${devpath}2
  mkfs.ext4 ${devpath}2
  e2label ${devpath}2 $initrd_keys_partition_label

  sync
  partprobe

  colorPrint "Configuring LUKS2"
  luksPart=${devpath}3
  echo -n $luksPassword | cryptsetup --type luks2 luksFormat $luksPart -d -
  colorPrint "LUKS2 has been configured on the root partition."
  echo -n $luksPassword | cryptsetup open $luksPart lvmpart -d -
  cryptsetup config $luksPart --label cryptroot

  colorPrint "Setting up LVM"
  pvcreate --dataalignment 4096 /dev/mapper/lvmpart
  vgcreate vg /dev/mapper/lvmpart
  lvcreate -L 8G vg -n swap
  lvcreate -l +100%FREE vg -n poolpart

  colorPrint "Creating new ZFS pool"
  zpool create -f \
    -O mountpoint=none \
    -O compression=lz4 \
    -o ashift=12 \
    -O acltype=posixacl \
    -O xattr=sa \
    raspool /dev/mapper/vg-poolpart

  colorPrint "Creating root dataset (ZFS)"
  zfs create -o mountpoint=legacy raspool/root
  colorPrint "Mounting root dataset to /mnt"
  mount -t zfs raspool/root /mnt

  colorPrint "Creating the /nix dataset"
  zfs create -o mountpoint=legacy raspool/nix
  mkdir /mnt/nix
  mount -t zfs raspool/nix /mnt/nix

  colorPrint "Mounting efi to /mnt/boot"
  mkdir -p /mnt/boot/efi
  mount /dev/disk/by-label/$boot_partition_label /mnt/boot

  colorPrint "Mounting initrd keys partition"
  tmp_keys_mountpoint=/run/initrd_tmp_keys
  mkdir $tmp_keys_mountpoint
  mount /dev/disk/by-label/$initrd_keys_partition_label $tmp_keys_mountpoint
  colorPrint "Generating the SSH keys for initrd"
  ssh-keygen -t ed25519 -a 200 -N "" -f $tmp_keys_mountpoint/ssh_host_ed25519_key
  ssh-keygen -t rsa -b 8192 -N "" -f $tmp_keys_mountpoint/ssh_host_rsa_key
  sync
  umount $tmp_keys_mountpoint

  colorPrint "Creating swap"
  mkswap -L $swap_partition_label /dev/mapper/vg-swap
  colorPrint "Activating swap"
  swapon /dev/mapper/vg-swap

  colorPrint "Ensuring no previous configs exist"
  rm -r /mnt/etc/nixos/ &>/dev/null || true

  colorPrint "Cloning config"
  git clone https://gitlab.com/Giacomo_zanolli/nixos-configuration.git /mnt/etc/nixos

  colorPrint "Creating persistent directories"
  persistentPath=/nix/persist
  mkdir -p /mnt$persistentPath/unsafe

  nixosConfigFolder=$persistentPath/config
  colorPrint "Moving config to final destination in ${nixosConfigFolder}"
  mntNixosFolder=/mnt$nixosConfigFolder
  mkdir -p /mnt$persistentPath
  mv /mnt/etc/nixos $mntNixosFolder
  colorPrint "Creating symlink in /etc"
  ln -s $nixosConfigFolder /mnt/etc/nixos
  colorPrint "Creating snapshot of blank root"
  zfs snapshot raspool/root@blank

  colorPrint "Copying SOPS key"
  cp $TMP_SOPS_PATH /mnt$persistentPath/.sops.key
  # /run is on tmpfs, but whatever, better be safe
  shred --remove --verbose --random-source=/dev/urandom -n4 $TMP_SOPS_PATH

  colorPrint "Fixing permissions on config (just for the installation)"

  # The installer expects the configs to be in /mnt/etc/nixos with no symlinks
  # and owned by root (at least, according to my tests so far)
  rm /mnt/etc/nixos
  cp -r $mntNixosFolder /mnt/etc/nixos
  chown -R root:root /mnt/etc/nixos

  colorPrint "Load efivars kernel module"
  modprobe efivars

  bannerPrint "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
  bannerPrint "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
  bannerPrint "                         ====         :::::::     :::::::             "
  bannerPrint "                        ======         :::::::   :::::::              "
  bannerPrint "                         ======         ::::::: :::::::               "
  bannerPrint "                          ======         :::::::::::::                "
  bannerPrint "                           ======         :::::::::::                 "
  bannerPrint "                  ======================== :::::::::        =         "
  bannerPrint "                 =========================== :::::::       ====       "
  bannerPrint "                ============================= :::::::     ======      "
  bannerPrint "                          :::::::              :::::::   ======       "
  bannerPrint "                         :::::::                ::::::: ======        "
  bannerPrint "                        :::::::                  ::::: ======         "
  bannerPrint "                       :::::::                    ::: ==============  "
  bannerPrint "          ::::::::::::::::::                       : ================ "
  bannerPrint "           :::::::::::::::: =                       =================="
  bannerPrint "            :::::::::::::: ===                     =======            "
  bannerPrint "                  ::::::: =====                   =======             "
  bannerPrint "                 :::::::  ======                 =======              "
  bannerPrint "                :::::::    ======               =======               "
  bannerPrint "                ::::::      ====== :::::::::::::::::::::::::::::      "
  bannerPrint "                 ::::        ====== :::::::::::::::::::::::::::       "
  bannerPrint "                  ::        ======== :::::::::::::::::::::::::        "
  bannerPrint "                           ==========         :::::::                 "
  bannerPrint "                          ============         :::::::                "
  bannerPrint "                         ======   ======         :::::::              "
  bannerPrint "                        ======     ======         :::::::             "
  bannerPrint ""
  bannerPrint "-------------------------INSTALLING THE OPERATING SYSTEM------------------------"
  bannerPrint ""
  bannerPrint ""
  bannerPrint "          Kindly, do NOT power off the machine nor stop the installation"
  bannerPrint ""
  bannerPrint ""
  bannerPrint "--------------------------------------------------------------------------------"
  bannerPrint ""
  bannerPrint ""
  bannerPrint ""

  nixos-install --no-root-password \
    --root /mnt \
    --flake /mnt/etc/nixos#$nixosHostName

  installationReturnValue=$?

  if [ $installationReturnValue -ne 0 ]; then
    echo "----------Unsuccessful installation----------"
    echo "Exiting"
    exit 1
  fi

  colorPrint "Backing up LUKS header in $persistentPath/luks_header_cryptroot.backup"
  cryptsetup luksHeaderBackup /dev/disk/by-label/cryptroot \
    --header-backup-file /mnt$persistentPath/luks_header_cryptroot.backup

  colorPrint "Ensuring all data has been written to the disks"
  sync

  colorPrint "Unmounting everything"
  umount -R /mnt

  swapoff /dev/mapper/vg-swap || true

  zpool export raspool || true

  colorPrint "Closing the encrypted partition"
  cryptsetup close vg-swap || true
  cryptsetup close vg-poolpart || true
  cryptsetup close lvmpart || true

  colorPrint "-----------Everything done! Powering off-----------"

  shutdown now
}
