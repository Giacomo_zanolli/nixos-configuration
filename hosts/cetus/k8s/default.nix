{ pkgs, ... }:

let
  kubeConfigLocation = "/etc/rancher/k3s/k3s.yaml";
in
{
  services.k3s = {
    enable = true;
    extraFlags = "--disable traefik";
  };

  environment.systemPackages = with pkgs; [
    kubectl
    openiscsi
  ];

  system.activationScripts = {
    k8s-config-kubectl = {
      text =
        ''
          # Link the k3s config so kubectl sees it
          mkdir -p /root/.kube
          ln -sfn ${kubeConfigLocation} /root/.kube/config
        '';
    };
  };

  sops.secrets."k8s-markhor.key" = {
    key = "markhor-age-key";
    sopsFile = ../../../secrets/cetus/k8s/markhor_secret.yaml;
  };

  environment.etc = {
    "bootstrap_k8s.sh".source = ./bootstrap-k8s.sh;
  };

  systemd.services.k8s-bootstrap-check = {
    description = "Ensure k8s has been bootstrapped correctly";
    after = [ "k3s.service" "network.target" ];
    wants = [ "k3s.service" "network.target" ];
    wantedBy = [ "multi-user.target" ];

    serviceConfig = {
      Type = "simple";
      RemainAfterExit = "yes";
      TimeoutStartSec = "1200";
    };

    environment = {
      KUBECONFIG = "${kubeConfigLocation}";
    };
    path = with pkgs; [
      nix
      kubectl
    ];
    script = ''
      nix shell nixpkgs#git nixpkgs#kubectl --command '/etc/bootstrap_k8s.sh'
    '';
  };

  systemd.tmpfiles.rules = [
    # github.com/longhorn/longhorn/issues/2166
    "L+ /usr/local/bin - - - - /run/current-system/sw/bin/"
  ];

  services.openiscsi = {
    enable = true;
    name = "iqn.2023-10.com.example:storage.target001";
  };

  environment.persistence = {
    "/nix/persist/unsafe" = {
      directories = pkgs.lib.mkAfter [
        "/var/lib/kubelet"
        "/var/lib/rancher/k3s"
        "/etc/rancher"
      ];
    };
  };
}
