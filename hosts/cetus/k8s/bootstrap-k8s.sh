echo "Bootstrap k8s script v1"
echo "Checking if the cluster was already initialized"

kubectl get applications.argoproj.io argocd-apps -n argocd

if [ $? -eq 0 ]; then
  echo "The cluster is already installed. Nothing to do here"
  exit 0
fi

# Cleanup from previous attempts
kubectl delete ns markhor
kubectl delete ns argocd

set -e

MARKHOR_MANIFESTS_URL=https://raw.githubusercontent.com/markhork8s/markhor/main/manifests
NIXOS_CONFIG_REPO=https://gitlab.com/Giacomo_zanolli/nixos-configuration/-/raw/main/hosts/cetus/k8s/bootstrap_manifests

echo "Markhor not found, proceeding with the installation"

echo "Creating the markhor namespace"
kubectl get namespace markhor || kubectl create namespace markhor

echo "Creating the markhor CRDs and roles"
kubectl apply -f $MARKHOR_MANIFESTS_URL/crds/MarkhorSecret_crd.yaml
kubectl apply -f $MARKHOR_MANIFESTS_URL/serviceaccount.yaml
kubectl apply -f $MARKHOR_MANIFESTS_URL/role.yaml
kubectl apply -f $MARKHOR_MANIFESTS_URL/rolebinding.yaml

echo "Creating the markhor age secret"
TMP_MARKHOR_SECRET=$(mktemp)
chmod 0600 $TMP_MARKHOR_SECRET
echo "apiVersion: v1
kind: Secret
metadata:
  name: markhor-age-secret
  namespace: markhor
stringData:
  age_keys.txt: REPLACEME
" >$TMP_MARKHOR_SECRET
sed -i "s/REPLACEME/$(</run/secrets/k8s-markhor.key)/" $TMP_MARKHOR_SECRET
kubectl apply -f $TMP_MARKHOR_SECRET
shred $TMP_MARKHOR_SECRET

kubectl apply -f $NIXOS_CONFIG_REPO/markhor/config_map.yaml

echo "Creating the markhor deployment"
kubectl apply -f $NIXOS_CONFIG_REPO/markhor/deployment.yaml

echo "Installing argocd"
kubectl get namespace argocd || kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

echo "Configuring argocd"
kubectl apply -f $NIXOS_CONFIG_REPO/argocd/known_hosts.yaml
kubectl apply -f $NIXOS_CONFIG_REPO/argocd/repos.yaml
sleep 1
echo "Deleting the pods in the argocd namespace to ensure the new configmap is applied"
kubectl delete pods -n argocd --all

echo "Adding core apps"
kubectl apply -f $NIXOS_CONFIG_REPO/argocd/core_proj.yaml
kubectl apply -f $NIXOS_CONFIG_REPO/argocd/apps.yaml

echo "All done"
