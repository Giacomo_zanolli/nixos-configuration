{ config, pkgs, lib, ... }:

{
  #UEFI boot with grub2
  boot = {
    tmp.cleanOnBoot = true;
    kernelParams = [
      "quiet"
      "udev.log_level=3"
      "splash"
      "boot.shell_on_fail" #https://discourse.nixos.org/t/12304#post_2
    ];
    kernelModules = [
      "kvm-intel"
      "fbdev"
    ];

    consoleLogLevel = 0;
    loader = {
      efi.canTouchEfiVariables = true;
      timeout = 3;
      grub = {
        enable = true;
        useOSProber = false;
        efiSupport = true;
        device = "nodev";
        configurationLimit = 50; #How many old NixOS generations to store
        users.${config.username}.hashedPassword = "grub.pbkdf2.sha512.10000.3AABFDE6AFC6607BB92C6B1EF92A301C80C0DADBC2996E6090111DEB1471F7945DC5E760D27EE77F9BDE80B5D8D2C7CAF11D423A30AF0D31350F0EFC432091A6.FA8F9C94847B142202F3EB991017D1FA9AD50B63D33708B91A442985FD3E915687058AF892833AFC1863B7E5C636278BE90AD0373D87211AE25230C03D4DEB8B";
      };
    };
  };
}
