{ ... }:

{
  virtualisation.docker = {
    enableOnBoot = true;
    autoPrune.enable = true;
  };
}
