{ config, lib, pkgs, modulesPath, ... }:

{
  fileSystems = {
    "/" = {
      device = "raspool/root";
      fsType = "zfs";
    };
    "/boot" = {
      device = "/dev/disk/by-label/nixboot";
      fsType = "vfat";
      options = [ "nodev" "noatime" ];
    };
    "/nix" = {
      device = "raspool/nix";
      fsType = "zfs";
      options = [ "nodev" "noatime" ];
    };
  };

  swapDevices = [
    {
      device = "/dev/mapper/vg-swap";
    }
  ];
  hardware.enableRedistributableFirmware = true;
  hardware.cpu.intel.updateMicrocode = true;
}
