{ config, pkgs, ... }:

{
  users = {
    users = {
      root = {
        # # This renders password authentication impossible for the root user.
        # # See https://discourse.nixos.org/t/13235
        # hashedPassword = "!";
        hashedPasswordFile = config.sops.secrets."root-passwd".path;
        openssh.authorizedKeys.keys = [
          (import ../../common/ssh.nix).bugabooPublicKey
        ];
      };
    };
    mutableUsers = false;
  };
}
