{ config, pkgs, ... }:

let
  poolName = "alzey";
in
{
  systemd.services.decrypt-disks =
    let
      datasets = [
        {
          name = "backup";
          mountPoint = "/backup";
        }
        {
          name = "media";
          mountPoint = "/media";
        }
      ];
      devices = [
        "relaxing-gobbler"
        "stable-marmot"
      ];

      encryptedPrefix = "c-";
      concatInString = { f, list }:
        builtins.concatStringsSep "\n" (map f list);
    in
    {
      description = "Decrypt encrypted devices on startup and mount the ZFS datasets";

      before = [ "multi-user.target" ];
      wantedBy = [ "restic-rest-server.service" "jellyfin.service" ];

      # TODO: this may be a bit cleaner than `after`, but did not have the occasion 
      # to test this yet
      #
      # unitConfig = {
      #   RequiresMountsFor = "/dev/disk/by-label/c-stable-marmot /dev/disk/by-label/c-relaxing-gobbler /nix /run/secrets";
      # };

      after = builtins.map (name: "dev-disk-byx2dlabel${encryptedPrefix}${name}.device") devices ++ [
        "zfs.target" #Since /nix is under ZFS, we need that first
        "run-secrets.d.mount" #Secrets are available
      ];

      script =
        let
          getDevicePath = deviceName: "/dev/disk/by-label/${encryptedPrefix}${deviceName}";

          waitForDevice = name:
            ''
              echo "Waiting for ${name} to come online"
              DEVICE_TIMEOUT=30
              ELAPSED=0

              while ! test -e "${getDevicePath name}"; do
                if [ "$ELAPSED" -ge "$DEVICE_TIMEOUT" ]; then
                  echo "Timeout reached: ${name} did not come online after $DEVICE_TIMEOUT seconds."
                  exit 0
                fi
                sleep 5
                echo "Still waiting"
                ELAPSED=$((ELAPSED + 5))
              done

              echo "Device ${name} is now online."
            '';

          cryptOpen = name:
            '' 
              if [ -f /dev/mapper/${name} ]; then
                echo "Device ${name} is already present, skipping it"
              else
                echo "Opening ${name}"
                cryptsetup open ${getDevicePath name} ${name} --key-file=${config.sops.secrets."crypt-${name}".path}
              fi
            '';

          mountDataset = { name, mountPoint }:
            let completeName = "${poolName}/${name}"; in
            ''
              echo "Mounting dataset ${completeName}"
              mkdir -p ${mountPoint}
              mount -t zfs \
                -o nodev,noexec \
                ${completeName} ${mountPoint}
            '';
        in
        with builtins; ''
          # Wait for disks to come online
          # This should be unnecessary given the 'after' directive,
          # but better be sure.
          ${concatInString {f=waitForDevice; list=devices;}}
          # LUKS open disk
          ${concatInString {f=cryptOpen; list=devices;}}
          echo "Importing pool ${poolName}"
          zpool import ${poolName}
          # Mount the datasets
          ${concatInString {f=mountDataset; list=datasets;}}
        '';

      preStop =
        let
          unmountDataset = { name, mountPoint }:
            let completeName = "${poolName}/${name}"; in
            ''
              echo "Unmounting dataset ${completeName}"
              umount ${mountPoint}
            '';
          cryptClose = name:
            "cryptsetup close ${name}";
        in
        with builtins;''
          echo "Syncing data to disk, just in case"
          sync
          ${concatInString {f=unmountDataset; list=datasets;}}
          echo "Closing the pool ${poolName}"
          zpool export ${poolName}
          echo "Closing the disks"
          ${concatInString {f=cryptClose; list=devices;}}
        '';

      path = with pkgs;[
        cryptsetup
        zfs
        util-linux
      ];

      serviceConfig = {
        Type = "oneshot";
        TimeoutStopSec = "90";
        RemainAfterExit = "yes";
      };
    };

  services.zfs.autoScrub = {
    enable = true;
    pools = [
      poolName
    ];
    #Every ten days, at 2am
    interval = "10d, 02:00";
  };
}
