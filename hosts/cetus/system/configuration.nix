{ ... }:

{
  imports = [
    ./boot/bootloader.nix
    ./boot/init/initrd.nix
    ./docker.nix
    ../k8s
    ./hardware.nix
    ./gpu.nix
    ./impermanence.nix
    ./misc.nix
    ./networking.nix
    ./nix.nix
    ./packages.nix
    ./ssh.nix
    ./sops.nix
    ./users.nix
    ./zfs.nix
    ./zfs_data.nix

    # Services
    ./services/maintenance/maintenance.nix
    #./services/godns.nix
    ./services/restic_server.nix
  ];
}
