{ config, ... }:

let
  certPath = "restic-server/certificate.crt";
in
{
  services.restic.server = {
    enable = true;
    dataDir = "/backup";
    appendOnly = true;
    listenAddress = "25307";
    privateRepos = true;
    extraFlags = [
      # Enable TLS
      "--tls"
      # TLS certificate
      "--tls-cert"
      "/etc/${certPath}"
      # TLS key
      "--tls-key"
      "${config.sops.secrets.restic-tls-key.path}"
    ];
  };

  environment.etc.${certPath} = {
    source = ../../../../secrets/cetus/services/restic/tls.crt;
    # See https://github.com/NixOS/nixpkgs/blob/b1f87ca164a9684404c8829b851c3586c4d9f089/nixos/modules/services/backup/restic-rest-server.nix#L102
    uid = config.users.users.restic.uid;
    gid = config.users.groups.restic.gid;
  };

  sops.secrets = {
    restic-tls-key = {
      format = "binary";
      sopsFile = ../../../../secrets/cetus/services/restic/tls.key;
      owner = config.users.users.restic.name;
      group = config.users.groups.restic.name;
    };
  };
}
