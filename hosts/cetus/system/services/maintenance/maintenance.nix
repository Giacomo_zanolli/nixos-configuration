{ pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; lib.mkAfter [ ];

  environment.etc = {
    "fresh_nix.sh".source = ./fresh_nix.sh;
    "clean_nix_store.sh".source = ./clean_nix_store.sh;
  };

  services.cron = {
    enable = true;
    systemCronJobs = [
      ''
        # Daily
        0~59 2~6 * * *            root      /etc/fresh_nix.sh
      ''

      ''
        # Weekly
        # 0~59 5 * * wed            root      /etc/...
      ''

      ''
        # Monthly
        0~59 2~6 1~28 * *         root      /etc/clean_nix_store.sh
      ''
    ];
  };
}
