#! /usr/bin/env sh

# This script cleans up unneeded data from the nix store to free some disk space.

echo "Deleting old generations"

# Delete old generations so that their store paths can be freed
nix-env --delete-generations 30d
nix-collect-garbage --delete-older-than 30d

echo "Running the garbage collector"

# Run the garbage collector
nix store gc
