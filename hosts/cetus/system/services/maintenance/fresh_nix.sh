#! /usr/bin/env nix-shell
#! nix-shell -i bash -p git nixUnstable

# This script updates all the packages in the system and activates the new config.
# When the system reboots (or if it crashes), it will restart with the previous config.

cd /etc/nixos

echo "Checking if flake.lock was modified"
# Check if flake.lock appears as unmodified in git
git diff --exit-code flake.lock
IS_FLAKE_LOCK_UNCHANGED=$?
# If flake.lock was modified
if [ $IS_FLAKE_LOCK_UNCHANGED != 0 ]; then
  echo "flake.lock was modified, making that config the default one at boot"
  # In this case, the config is probably a working one,
  # so let's make it more permanent with a nixos-rebuild boot
  nixos-rebuild boot

  # Restore the old flake.lock (just to have a clean git tree)
  git checkout HEAD flake.lock
fi

echo "Updating flake lockfile"
# Update the flake.lock
nix flake update

echo "Activating the updated config as a test one"
# Activate the new config
nixos-rebuild test
