{ config, lib, ... }:

{
  services.jellyfin = {
    enable = true;
    user = "jellyfin";
    group = "jellyfin_g";
    openFirewall = true;
  };

  users = {
    users.jellyfin = {
      group = "jellyfin_g";
      isSystemUser = true;
      extraGroups = [
        "video"
      ];
    };
    groups = {
      jellyfin_g = { };
      # The permissions on the media itself need to be read and write for files and read and execute for directories
      # use the command below to set them recursively
      # chmod -R -x /media; chmod -R ug=rwX /media
      # You could also use the following to give jellyfin read-only access to the media (assuming the owner is not the jellyfin user)
      # chmod -R -x /media; chmod -R u=rwX,g=rX,o=rX /media
      # furthermore, the directory needs to be owned by the jellyfin_g
    };
  };

  environment.persistence =
    let
      libDir = "jellyfin"; # config.systemd.services.jellyfin.serviceConfig.StateDirectory;
      cacheDir = "jellyfin"; # config.systemd.services.jellyfin.serviceConfig.CacheDirectory;
      mkJellyDir = dirName: {
        directory = dirName;
        user = "jellyfin";
        group = "jellyfin_g";
        mode = "u=rwX,g=rX,o=rX";
      };
    in
    {
      "/nix/persist/safe" = {
        hideMounts = true;
        directories = builtins.map mkJellyDir [
          "/var/lib/${libDir}/config"
          "/var/lib/${libDir}/data"
          "/var/lib/${libDir}/root"
        ];
      };
      "/nix/persist/unsafe" = {
        directories = builtins.map mkJellyDir [
          "/var/lib/${libDir}/metadata"
          "/var/lib/${libDir}/transcodes"
          "/var/cache/${cacheDir}"
        ];
      };
    };

  networking.firewall = {
    allowedTCPPorts = [
      8096
    ];
    allowedUDPPorts = [
      8096
    ];
  };
}
