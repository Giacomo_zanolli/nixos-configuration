{ config, pkgs, ... }:

{
  systemd.timers =
    let
      mkTimer = serviceName: {
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = "*:0/5"; # every 5 minutes
          RandomizedDelaySec = "3m";
          Unit = serviceName;
        };
      };
    in
    {
      "godns" = mkTimer "godns.service";
      "godns-2" = mkTimer "godns-2.service";
    };

  systemd.services =
    let
      mkService = { serviceName, secretFile }: {
        description = "Golang Dynamic DNS Client";
        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Type = "simple";
          User = config.users.users.godns.name;
          Group = config.users.groups.godns.name;

          NoNewPrivileges = true;
          SystemCallArchitectures = "native";
          RestrictAddressFamilies = [ "AF_UNIX" "AF_INET" "AF_INET6" "AF_NETLINK" ];
          RestrictNamespaces = true;
          RestrictRealtime = true;
          RestrictSUIDSGID = true;
          ProtectControlGroups = true;
          ProtectHostname = true;
          ProtectKernelLogs = true;
          ProtectKernelModules = true;
          ProtectKernelTunables = true;
          LockPersonality = true;
          PrivateTmp = true;
          PrivateDevices = true;
          PrivateUsers = true;
          RemoveIPC = true;
        };
        script = ''
          ${pkgs.godns}/bin/godns -c ${secretFile}
        '';
      };
    in
    {
      godns = mkService { serviceName = "godns"; secretFile = config.sops.secrets."godns-config.json".path; };
      godns-2 = mkService { serviceName = "godns-2"; secretFile = config.sops.secrets."godns-config-2.json".path; };
    };

  sops.secrets = {
    "godns-config.json" = {
      # See https://github.com/Mic92/sops-nix/issues/120
      key = "one";
      owner = config.users.users.godns.name;
      sopsFile = ../../../../secrets/cetus/services/godns_config.yaml;
    };
    "godns-config-2.json" = {
      # See https://github.com/Mic92/sops-nix/issues/120
      key = "two";
      owner = config.users.users.godns.name;
      sopsFile = ../../../../secrets/cetus/services/godns_config.yaml;
    };
  };

  users = {
    users = {
      godns = {
        isSystemUser = true;
        group = config.users.groups.godns.name;
      };
    };
    groups = {
      godns = { };
    };
  };
}
