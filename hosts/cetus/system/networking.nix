{ config, pkgs, lib, ... }:

{
  networking = {
    hostName = "cetus";
    wireless.enable = false;

    firewall = {
      enable = true;
      allowedTCPPorts = [
        25307 #Restic server
      ];
      allowedUDPPorts = [
        25307 #Restic server
      ];
    };
  };
}
