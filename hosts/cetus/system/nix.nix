{ lib, pkgs, ... }:

{
  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
      trusted-public-keys = lib.mkAfter [
        "bugaboo-nixcache:nR2l8egwFLH3pJOiWHLgRQxHspv/GtUB/SenxnZA34o="
      ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
  };
  system.stateVersion = "24.11";
  documentation.nixos.enable = false;
}
