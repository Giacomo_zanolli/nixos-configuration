{ lib, ... }:

{
  networking.hostId = "a57e1e00";

  boot = {
    initrd.postMountCommands =
      let
        scriptContents = ''
          #! /usr/bin/env sh

          echo "Erasing the filesystem"
          zfs rollback -r raspool/root@blank
        '';
      in
      lib.mkAfter scriptContents;
    supportedFilesystems.zfs = true;
    # zfs.forceImportRoot = true;
  };
}
