{ config, pkgs, ... }:

{
  services.openssh = {
    enable = true;
    # Require public key authentication for better security
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      PermitRootLogin = "yes";
      X11Forwarding = false;
    };
    allowSFTP = true;
  };
  services.fail2ban.enable = true;
}
