{ config, ... }:

{
  sops = {
    defaultSopsFile = ../../../.sops.yaml;

    age = {
      keyFile = "/nix/persist/.sops.key";
      generateKey = false;
      #See https://github.com/Mic92/sops-nix/issues/167
      sshKeyPaths = [ ];
    };

    #See https://github.com/Mic92/sops-nix/issues/167
    gnupg.sshKeyPaths = [ ];

    secrets = {
      root-passwd = {
        neededForUsers = true;
        key = "root-passwd";
        sopsFile = ../../../secrets/cetus/users.yaml;
      };

      crypt-relaxing-gobbler = {
        format = "binary";
        sopsFile = ../../../secrets/cetus/alzey/relaxing-gobbler.key;
      };

      crypt-stable-marmot = {
        format = "binary";
        sopsFile = ../../../secrets/cetus/alzey/stable-marmot.key;
      };
    };
  };
}
