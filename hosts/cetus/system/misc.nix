{ ... }:

{
  time.timeZone = "Europe/Rome";

  powerManagement.cpuFreqGovernor = "ondemand";
}
