# Cetus

This host is a Raspberry PI version 4

## Installation

1. Tell the Raspberry to try to boot from the USB before the SD (or remove the SD). To do so, I suggest you use the official raspberry PI OS flashed onto a microSD with the raspberry pi imager (`rpi-imager` on nixpkgs). From there, `sudo apt-get update; sudo apt-get full-upgrade; sudo raspi-config` -> advanced -> boot order -> USB (relevant docs [here](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#configuration-properties))

1. Get the NixOS SD ISO from [hydra](https://hydra.nixos.org/job/nixos/release-22.11/nixos.sd_image.aarch64-linux). At the time of writing, [this](https://hydra.nixos.org/build/207063508) was the latest build

1. Burn the ISO to a microSD/USB drive (for example with `unzstd image.zst; sudo dd if=image.img of=/dev/sdc status=progress && sync`)

   I used a 4GB drive, but had troubles during installation since it ran out of space when trying to build some packages. Solved that by using my pc as a binary cache for those packages. Using a bigger drive, however, should prevent this problem.

1. Boot the raspberry with the drive plugged in.

   Note that if other USB devices are plugged in when powering on -e.g., a keyboard- the raspberry may fail to boot since it tries to boot from them. Also, ensure that the raspberry is configured to use the USB to boot (see the official [docs](https://www.raspberrypi.com/documentation/computers/configuration.html))

1. `sudo su`

1. If you want to use WiFi to access the Internet:

   1. `wpa_passphrase "SSID" "Passphrase" > a.conf`

   1. `wpa_supplicant -i wlan0 -c a.conf`

   1. `wpa_supplicant -i wlan0 -c a.conf -B`

   1. `dhcpcd -i wlan0`

1. `ping -c 1 google.com` to check you are online

1. Download the partitioning script with `curl -L -o setup.sh https://is.gd/Xs8bko`.

1. Make it executable with `chmod +x setup.sh`.

1. Run it `./setup.sh`

   1. If you want to avoid compiling on the Raspberry, since the drive you booted from could run out of space, you can use another host (e.g., bugaboo) as a binary cache. To do so, ensure bugaboo has `services.nix-serve` running and reacheable and run the `./build_and_sign.sh cetus` script to compile the packages locally, then run the `./setup.sh` on cetus.

### ZFS setup:

storage goes through mirrored vdevs, that are put in a stripe (Raid 10). See [this video](https://www.youtube.com/watch?v=GuUh3bkzaKE&list=PL4b74vD-Uo-N8LsptmTjTcTgpagFNLu-M) on the why!

#### Preliminary notes:

1.  Phisical sector size: if your disk has 4K physical sectors (check the technical reference for your model), use the option ashift=12 ([here](https://openzfs.github.io/openzfs-docs/man/7/zpoolprops.7.html?highlight=ashift) you can see why the value 12, because 2^12=4096) to get better performance and reduced fragmentation. See the ashift reference docs [here](https://openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/Workload%20Tuning.html#alignment-shift-ashift) and [here](https://openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/Workload%20Tuning.html#alignment-shift).

1.  Fragmentation. ZFS pools, as disks, fragment. Discussion on how to solve this has been going on [for a while](https://github.com/openzfs/zfs/issues/3582).
    According to the docs, fragmentation can be reduced for files that are not written sequentially (e.g., torrent downloads) by downloading these files to a directory different from the final destination, copying them to the destination and deleting the old copy [ref](https://openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/Workload%20Tuning.html#bit-torrent).

There does not seem to be a definitive solution to this, except copying the dataset.  
 Recommended actions:

1.  Ensure the proper ahift is applied [ref](https://web.archive.org/web/20160517133549/http://www.racktopsystems.com/dedicated-zfs-intent-log-aka-slogzil-and-data-fragmentation/)

1.  The smaller the chunks of data (you write to the pool) the more fragmented the pool will become over time [ref](https://web.archive.org/web/20160517133549/http://www.racktopsystems.com/dedicated-zfs-intent-log-aka-slogzil-and-data-fragmentation/)

1.  Monitor the fragmentation of the pool with `zpool list alzey -o fragmentation | tail -n 1`. When it reaches 70%, you could start seeing performance hits. To fix this, the best option seems to do, iteratively for each dataset in the pool, a zfs send | zfs recv to another pool. After all the data has been migrated, either wipe out the original pool (`zfs destroy -f alzey`) and recreate it there by sending the data back, or just use the receiving pool from the previous step. If you decide to destroy a dataset/pool, you may need to wait a bit before seeing the fragmentation stats update.
    In order to send a dataset [ref video](https://youtu.be/59iIt3WFCsU?list=PL4b74vD-Uo-N8LsptmTjTcTgpagFNLu-M&t=100), say its name is patypus, do `zfs snapshot alzey/platypus@down` (down is an arbitrary name for the snapshot). Then `zfs send alzey/platypus@down | ssh root@target_machine zfs recv receiving_pool_name/receiving_dataset_name`. Note that receiving_dataset_name must be a dataset name that is NOT already present on the receiving end. The receiving pool and dataset name may coincide with the sending ones, but this is not required.

1.  Free space. Keep the pool free space to at least 10% [ref](https://openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/Workload%20Tuning.html#free-space)

1.  Giving ZFS a whole disk vs. a partition. See the reccomendations [here](https://openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/Workload%20Tuning.html#whole-disks). According to them, and a couple of posts I read on forms, there is little to no overhead in putting ZFS on partitions if the sectors are aligned properly. In my case, I chose ZFS over a partition encrypted with LUKS2. Yes, ZFS has its own baked-in encryption, but LUKS has been around for longer -and I already have some experience managing it-.

#### Setup:

1. Connect the disks to the system

1. Check they are there with `lsblk`

1. If they had anything on them, clean them running as root

   ```bash
   DEVPATH=/path/to/the/disk/to/wipe #e.g., /dev/sdc
   zpool labelclear -f $DEVPATH
   wipefs -af $DEVPATH
   blkdiscard -f $DEVPATH &>/dev/null
   ```

1. Check the health of the drives (e.g., with [seatools](https://www.seagate.com/support/downloads/seatools) for seagate drives)

1. Verify if there are firmware updates available for the drives. Yes, even HDDs have firmware. Check [this page for seagate](https://apps1.seagate.com/downloads/request.html) drives (or [this](https://www.seagate.com/gb/en/support/kb/firmware-updates-for-seagate-products-207931en/))

1. Note the disk name by running `lsblk`. e.g., `/dev/sdc` (note, this can change between reboots)

1. Use the rust-petname command `petname --count 10 | grep -e "^.\{,13\}$"` to generate a nickname for the disk (alternatively, you can use its serial code, present usually on the stickers and in the disk ID). In our case, the petname will be `stable-marmot` and the pool name will be `alzey`.

1. Create the partition to be encrypted with luks `sgdisk --new 1:0:0 --typecode 1:8200 --change-name 1:c-stable-marmot /dev/sdc`

1. Create the luks key, if needed
   <!-- `dd if=/dev/random bs=1K count=1 | base64 | tr --delete '\n' | sed 's/\+/-/g' | sed 's/\//_/g' | sed 's/=//g' > stable-marmot.key` -->

   (64 means 256 bits of entropy, 128 means 512)
   `nix-shell -p openssl --run "openssl rand -hex 128 | tr -d '\n'" > ./secrets/cetus/alzey/stable-marmot.key_unencrypted`
   then encrypt it
   `sops --config .sops.yaml --encrypt ./secrets/cetus/alzey/stable-marmot.key_unencrypted > ./secrets/cetus/alzey/stable-marmot.key`

1. Encrypt the partition: `cryptsetup --type luks2 luksFormat /dev/sdc1 --key-file stable-marmot.key`

1. Open it `cryptsetup open /dev/sdc1 stable-marmot --key-file stable-marmot.key`

1. Configure it `cryptsetup config /dev/sdc1 --label c-stable-marmot`. Note that the length of the label cannot be more than 16

1. Do the same for all the other disks.

1. With all disks plugged into the system (and unmounted), Create the Raid10 pool:

   ```bash
   # Pay attention to the order you put the disks in in the following command.
   # Two subsequent disks should be the same size (because you only get the capacity of the smaller one).
   zpool create POOLNAME \
      -o ashift=12 \ #Optional, see https://youtu.be/59iIt3WFCsU?list=PL4b74vD-Uo-N8LsptmTjTcTgpagFNLu-M&t=687
      mirror \
      /dev/disk/a \ #You can also use /dev/disk/by-id, by-label or whatever. By-id is great for seeing which one failed
      /dev/disk/b \
      /dev/disk/c \
      /dev/disk/d \
      /dev/disk/e \
      /dev/disk/f \
   ```

   This will create a pool which logically looks like this, with three vdevs in Raid0, each of which is made by two disks in Raid1 (mirror).

   ```
   POOLNAME
      ├── mirror-0
      │   ├── a
      │   └── b
      ├── mirror-1
      │   ├── c
      │   └── d
      └── mirror-2
          ├── e
          └── f

   ```

   With this setup, as long as you do not lose two disks from the same vdev, you do not have data loss.

   In this case,

   ```bash
   zpool create alzey \
      -o ashift=12 \
      -O acltype=posixacl \
      -O compression=lz4 \
      -O xattr=sa \
      -O mountpoint=none \
      mirror \
      /dev/mapper/stable-marmot \
      /dev/mapper/relaxing-gobbler
   ```

   1. Verify the success by running `zpool status alzey`

   1. Create each dataset you need (e.g., one named backup) by running: `zfs create -o mountpoint=legacy alzey/backup`

#### Open a previously created pool

1. Cryptsetup open -d keyfile /path/to/encrypted/part mappername
2. zpool import poolname
3. zfs list -r poolname
4. mount -t zfs poolname mountpoint

#### Further Optimizations

1.  You can use an SSD (or part of it) as a cache with the Adaptive Replacement Cache feature. [ref](https://openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/Workload%20Tuning.html#adaptive-replacement-cache).

1.  see also
    - https://www.reddit.com/r/zfs/comments/9cppbb/does_anyone_know_why_simply_destroying_a/
1.  I tried to take some more zfs pool options from [here](https://jrs-s.net/2018/08/17/zfs-tuning-cheat-sheet), but they are for ZFS, not OpenZFS.

### Restic

1. Create the user-password file on cetus /backup
   `nix-shell -p apacheHttpd`-> `htpasswd -B -c /data/.htpasswd civts` (and input the password)

#### Problems:

Manual creation of certificates. For this a solution would be to put it in k3s with cert-manager

Missing TLS client auth, which would be cool
https://github.com/restic/rest-server/issues/73

Have to specify the login credentials in /data/htpasswd, and as of now that needs to be manual. A config option would be of great help
https://github.com/restic/rest-server/issues/139

## Updating

### Method 1: deploy-rs

1. On bugaboo, or any other host that has SSH access to cetus, run `deploy .#cetus` in the directory with the main flake.nix of this repository.

### Method 2: manual

1. SSH into cetus
1. `cd /etc/nixos`
1. `git pull`
1. `nixos-rebuild switch`

### Method 3: utopia

Other people [were able](https://www.youtube.com/watch?v=J4DgATIjx9E&list=PLgknCdxP89ReQzhfKwMYjLdwWsc7us8ns&t=1233s) to make deployments with nix and automatic rollbacks, but I haven't found anything standard enough yet.

## Troubleshooting

1. The NixOS SD image fails to boot. Probably the image was not written correctly/fully to the drive.

1. Installation fails, with the error `unable to download 'http://bugaboo:port/<hash>.narinfo': Server returned nothing (no headers, no data)`.

   1. Look at the nix-serve log. In my case I saw `The program must call nix::initNix() before calling any libstore library functions.`. Searching, I found the culprit was an update from nix 2.12 to nix 2.13 and an [issue](https://github.com/NixOS/nix/issues/7704) had already been filed.

1. Installation fails becuse you get 500 internal error from nix-serve. Then double check that nix-serve can read the nix cache private key correctly.

1. Installation fails and you see errors about `NAR /<hash>-pname fetched from https://bugaboo:28398 is incomplete`.
   This one was very weird. I tried deleting the binary cache on bugaboo and recompiling the packages, but the error persisted. I then tried to close the `nix-serve` process on bugaboo `systemctl stop nix-serve.service`, look at the systemd service `less /etc/systemd/system/nix-serve.service`, then in another shell I create a symlink to the secret `ln -s /run/secrets/nixcache /NIX_SECRET_KEY_FILE` and run the executable it runs in ExecStart: `sudo /nix/store/l39qga3dblpvqckfpg9x4lrb4k16fy00-unit-script-nix-serve-start/bin/nix-serve-start` (once this worked by setting an environment variable instead of the link... `NIX_SECRET_KEY_FILE=/run/secrets/nixcache` whatever). Tried again the installation and everything went fine. Damn, I wish this nix-serve would be more reliable, or at least tell me about the errors.

1. System is extremely slow. Like, it takes minutes for you to be able to type and log in on the tty and commands take way too long to run. You may also see the following errors in `journalctl`:

   - `task txg_sync:399 blocked for more than 120 seconds.`
   - `sd 0:0:0:0: [sda] tag#27 uas_eh_abort_handler 0 uas-tag 7 inflight: CMD IN`

   These problems vanished when I plugged the SSD I was booting from in a docking station, so that power came from it rather than the Raspberry. The following resources helped me:

   1. https://github.com/raspberrypi/linux/issues/5060
   1. https://forums.raspberrypi.com/viewtopic.php?t=326636

# Resources that helped me setting up this machine:

https://github.com/eternaldyce/nixos-setup-rpi/blob/main/configuration.nix
https://github.com/sekunho/dotfiles
https://gist.github.com/bitonic/78529d3dd007d779d60651db076a321a

# TODO:

1. Encrypted rootfs (and swap) with the key supplied in ramfs(initrd) through openssh [ref](https://mazzo.li/posts/hetzner-zfs.html).
   https://mth.st/blog/nixos-initrd-ssh/
   https://tpm2-software.github.io/2020/04/13/Disk-Encryption.html
   https://github.com/tpm2-software/tpm2-tss
   https://github.com/NixOS/nixpkgs/issues/26175
   https://nixos.wiki/wiki/ZFS#Unlock_encrypted_zfs_via_ssh_on_boot

1. ZED notifications, + workaround for when powered off: https://youtu.be/5I2aw_yVcRM?t=1048

1. Kernel compiled with different params for better(?) performance [ref](https://github.com/NixOS/nixpkgs/issues/169457)

## Secret management:

All secrets are managed by sops-nix. The only exception is the initrd ssh key.

### Initrd ssh key

Can't be managed by sops, because provisioning secrets in initrd is not fully supported yet [ref](https://github.com/Mic92/sops-nix#initrd-secrets)

Options:

1. Include the plaintext key in the derivation. This means that both `cetus` and any host on which I want to cross-compile its derivation (bugaboo) have to be able to access it.
   Since I will cross-compile cetus only for the first installation, the repo will keep the version that lets cetus use its secret.
   In order to install, temporairly provision the ssh key manually in the initrd cetus config.
   Still, for this the key would need to be in clear also in the repo -or I'd need to use sops to decrypt it every time-.
   The secrets in boot.initrd seem to suffer of quite some problems:
   - https://github.com/NixOS/nixpkgs/issues/85563
   - https://github.com/NixOS/nixpkgs/issues/132330
   - https://github.com/NixOS/nixpkgs/pull/209156
     As of now, not even Mic92 seems to use it [ref](https://github.com/Mic92/dotfiles/blob/9b76629067bafa543715ed97916b1b6a316171bc/nixos/eve/modules/network.nix#L93).
1. Do as [Mazzoli with his server](https://mazzo.li/posts/hetzner-zfs.html) and leave the key on its own, not managed by nix.
   This means impure derivations.

#### Chose option 2.

1. Tried to boot, got error described [here](https://discourse.nixos.org/t/16146/9), complaining about `/etc/shh/nitrd_ssh_key` not being found. Interesting to see how, as noted in the discourse, the first letter of the file name was truncated with respect to our config.
1. Let's see it clear by adding a new line, `sh` to the `preLVMCommands`, after `echo 'Starting sshd...'`, to get a shell, switching to this new config and rebooting.
1. Got the shell (you don't see any prompt but it works)
1. Do an `ls -l /etc/shh/nitrd_ssh_key` and you will see that the file is there, but it is not a file, it is a symlink to `/nix/store/<hash>-extra-utils/secrets/./etc/ssh/nitrd_ssh_key`. Clearly, it cannot find it since `/nix` is still to be mounted. The key needs to be copied in the initrd config!
1. Give the shell an `exit` command to continue with the boot.
1. Looked at
   https://discourse.nixos.org/t/how-to-unlock-some-luks-devices-with-a-keyfile-on-a-first-luks-device/18949
   https://discourse.nixos.org/t/disk-encryption-on-nixos-servers-how-when-to-unlock/5030
   https://discourse.nixos.org/t/early-boot-remote-decryption/16146
   https://github.com/NixOS/nixpkgs/blob/ac1f5b72a9e95873d1de0233fddcb56f99884b37/nixos/modules/system/boot/stage-1.nix
   https://github.com/NixOS/nixpkgs/blob/ac1f5b72a9e95873d1de0233fddcb56f99884b37/pkgs/build-support/kernel/make-initrd.nix

1. Maybe `boot.initrd.network.ssh.ignoreEmptyHostKeys`
1. With ignoreEmptyHostKeys it seems I am able to build without supplying the key or the path to it, which is great
   Then I suppose openssh will look for the keys in `/etc/ssh/ssh_host_ed25519_key` and `/etc/ssh/ssh_host_rsa_key`

https://git.sr.ht/~mchrist/dotfiles/tree/master/item/nixos/modules/ssh.nix#L26
https://github.com/Mic92/dotfiles/blob/abb50db1fb0f396a1db05fd85326b08aca044ea7/nixos/eve/modules/network.nix#L99

1. `ignoreEmptyHostKeys` is the way! The SSH config gets generated correctly, and all I needed to do was to add some scripts so that the openssh server started by initrd would find the keys in `/etc/ssh/...`.

## Flow:

1. Bootloader starts initrd
1. Initrd waits on SSH for the passwrd to unlock the OS disk
1. Initrd uses the keys on the OS disk to unlock the ZFS storage
1. Boot completes

Now, how do we get on the host:

1. The initd ssh host keys
   The attribute for this is `hostKeys`, and it accepts paths,
   so we need to have an unencrypted copy of the key on the builder.
   At the same time, since we want the OS to be able to update autonomously,
   this path must also be accessible in the OS whne we rebuild
   say, `/nix/persist/keys/initrd.key`
   and this path needs to be valid when we install the OS!

   We could patch the config on the builder and in the OS installer script (can be done automatically with sops)

1. The ZFS keys
   Would be great to manage these with sops
   This would mean creating a systemd service to decrypt them and mount the drives

1. The sops key, this one is easy peasy, we will surely need to copy it during the installation script

Dashboard:
homer
dashy
organizr
prtg (not opensource!!)
graphana + prometheus (heavy)
teleport

deployment the worst time took:

- 2 hours on bugaboo to build the image (recompiled zfs kernel and extre-utils)
- 1 hour on bugaboo to build deploy-rs from scratch

Troubleshooting

- `/usr/local/bin/k3s-killall.sh; systemctl start k3s.service`
