# Minimal Kexec

This is not a host, more like a minimal NixOS image meant to be used with kexec to get to an environment from where you can install NixOS on the actual machine (useful for VPSs).

The main sources of information for this have come from:

- https://nixos.wiki/wiki/Install_NixOS_on_a_Server_With_a_Different_Filesystem
- https://github.com/cleverca22/nix-tests/blob/2ba968302208ff0c17d555317c11fd3f06e947e2/kexec/

## Installation

The next commands will assume you are in this directory with your terminal.

1.  Install on your server whichever other operating system you can -don't worry, it will be gone soon-. Make sure it supports `kexec`.

1.  Edit the `configuration.nix` file. Changing the variables at the top should be enough.

1.  Build the image we will load in the server with `nix-build '<nixpkgs/nixos>' -A config.system.build.kexec_tarball -I nixos-config=./configuration.nix -Q`. Be sure to specify the right path to the config.nix file.

1.  Ensure that you have enough RAM on the server. It needs to be at least as big as the image, plus any more RAM needed for the system to run. During my installation, it peaked at around 3GB.

1.  Get a checksum of the image `sha256sum ./result/tarball/nixos-system-x86_64-linux.tar.xz`

1.  Copy the resulting image to the server with `scp ./result/tarball/nixos-system-x86_64-linux.tar.xz root@YOUR_SERVER_IP:.`. You can now remove the results directory on your laptop `rm -r .results`.

1.  Ssh into your server as root

    1. Check that the image was copied correctly by running `sha256sum ~/nixos-system-x86_64-linux.tar.xz` and comparing with the previous result.
    1. `cd /`
    1. Untar the image `tar -xf ~/nixos-system-x86_64-linux.tar.xz`
    1. "Boot" into it `./kexec_nixos`
    1. Took this text from the nixos.wiki page
       - Wait until the `+ kexec -e` line shows up, then terminate the ssh connection by pressing the following keys one after the other: `RETURN`, `~`, `.`.
       - Wait until you have a ping again.
       - Reconnect with ssh. If previously you used to authenticate with ssh, you should see a warning about the host identification having changed. This is a good sign. Remove your server's previous entry in your laptop's `~/.ssh/known_hosts` and try again.

1.  Note: the image you just booted is configured to reboot automatically after 65 minutes with the `autoreboot` systemd timer. This will make it so that if you messed up the connection settings, the machine will reboot to the previous operating system which is still on disk. Before erasing the disk, we suggest you run `sudo systemctl stop autoreboot.timer` to ensure that the machine will not turn off while you are installing nixos.

1.  Time to proceed with the installation of the system of your choice! Do ensure, however, to use the right network configuration in it. You may use the same for this one to start with.
