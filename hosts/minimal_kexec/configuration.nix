{ lib, pkgs, ... }:

let
  server_ip = "207.180.228.12"; #ip -o -4 addr show dev eth0 | awk '{print $4}' | cut -d'/' -f1
  prefixLength = 24; #ip -o -4 addr show dev eth0 | awk '{print $4}' | cut -d'/' -f2
  gateway = "207.180.228.1"; #ip route | grep default
  autorebootEnabled = true; # see README
  authorized_ssh_keys = [
    (import ../common/ssh.nix).bugabooPublicKey
  ];
in
{
  imports = [
    <nixpkgs/nixos/modules/installer/netboot/netboot-minimal.nix>
    ./kexec.nix
  ];

  boot = {
    supportedFilesystems.zfs = true;
    loader.grub.enable = false;
    kernelParams = [
      "console=ttyS0,115200" # allows certain forms of remote access, if the hardware is setup right
      "panic=30"
      "boot.panic_on_fail" # reboot the machine upon fatal boot issues
      "net.ifnames=0" # use predictable interface names starting with eth0
    ];
  };

  networking = {
    hostName = "kexec";
    defaultGateway = gateway;
    nameservers = [ "9.9.9.9" "1.1.1.1" "8.8.8.8" ];
    interfaces.eth0 = {
      ipv4.addresses = [
        {
          address = server_ip;
          prefixLength = prefixLength;
        }
      ];
    };
  };

  systemd = {
    timers.autoreboot = lib.mkIf autorebootEnabled {
      partOf = [ "autoreboot.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    services.autoreboot = lib.mkIf autorebootEnabled {
      script = "shutdown -r +5";
    };
    services.sshd.wantedBy = lib.mkForce [ "multi-user.target" ];
  };

  users.users.root.openssh.authorizedKeys.keys = authorized_ssh_keys;

  environment.systemPackages = with pkgs; [
    wget
    git
  ];

  nix = {
    package = pkgs.nixVersions.stable;
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  system.stateVersion = "24.11";
}
