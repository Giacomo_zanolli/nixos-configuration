{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprcursor-phinger.url = "github:jappie3/hyprcursor-phinger";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    impermanence.url = "github:nix-community/impermanence";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    sops-nix = {
      url = github:Mic92/sops-nix;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { deploy-rs, home-manager, impermanence, nixpkgs, self, sops-nix, ... } @ inputs:
    let
      config = {
        username = "civts";
      };
    in
    {
      nixosConfigurations = {
        #Laptop
        bugaboo = nixpkgs.lib.nixosSystem {
          specialArgs = { flakeInputs = inputs; };
          system = "x86_64-linux";
          modules = [
            {
              nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (nixpkgs.lib.getName pkg) [
                #"chrome"
              ];
              nixpkgs.config.permittedInsecurePackages = [
                "electron-27.3.11"
              ];
            }
            {
              options = with nixpkgs.lib; {
                username = mkOption {
                  type = types.str;
                  description = "The username to use";
                };
                hostname = mkOption {
                  type = types.str;
                  description = "The hostname of the machine";
                };
              };
              config = config // {
                hostname = "bugaboo";
                sys = rec {
                  theme = "hyprland";
                };
                nixpkgs.overlays = [
                  (import ./pkgs/overlay.nix)
                ];
              };
            }
            ./hosts/bugaboo/system/configuration.nix
            ./hosts/bugaboo/home/themes
            home-manager.nixosModules.home-manager
            ./hosts/bugaboo/home/home-manager.nix
            impermanence.nixosModules.impermanence
            sops-nix.nixosModules.sops
          ];
        };
        #Server 1
        cetus = nixpkgs.lib.nixosSystem {
          specialArgs = { flakeInputs = inputs; };
          system = "x86_64-linux";
          modules = [
            {
              options = with nixpkgs.lib; {
                username = mkOption {
                  type = types.str;
                  description = "The username to use";
                };
              };
              config.username = config.username;
            }
            impermanence.nixosModules.impermanence
            sops-nix.nixosModules.sops
            ./hosts/cetus/system/configuration.nix
          ];
        };
        # Personal Server
        devon =
          let
            p = import inputs.nixpkgs {
              system = "x86_64-linux";
            };
          in
          nixpkgs.lib.nixosSystem {
            specialArgs = { flakeInputs = inputs; };
            system = "x86_64-linux";
            pkgs = p;
            modules = [
              {
                options = with nixpkgs.lib; {
                  hostname = mkOption {
                    type = types.str;
                    description = "The hostname to use";
                  };
                };
                config.hostname = "devon";
              }
              impermanence.nixosModules.impermanence
              sops-nix.nixosModules.sops
              ./hosts/devon/system/configuration.nix
            ];
          };
        # Backup Server
        emerald = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          pkgs = import inputs.nixpkgs {
            system = "x86_64-linux";
          };
          modules = [
            impermanence.nixosModules.impermanence
            sops-nix.nixosModules.sops
            ./hosts/emerald/system/configuration.nix
          ];
        };
      };
      deploy.nodes = {
        cetus = {
          hostname = "cetus";
          sshUser = "root";
          magicRollback = true;
          autoRollback = true;
          profiles.system = {
            user = "root";
            path = deploy-rs.lib.x86_64-linux.activate.nixos self.nixosConfigurations.cetus;
          };
        };
        devon = {
          hostname = "207.180.228.112";
          sshUser = "root";
          magicRollback = true;
          autoRollback = true;
          remoteBuild = true;
          profiles.system = {
            user = "root";
            path = deploy-rs.lib.x86_64-linux.activate.nixos self.nixosConfigurations.devon;
          };
        };
      };
      checks = builtins.mapAttrs (system: deployLib: deployLib.deployChecks self.deploy) deploy-rs.lib;
    };
}
