## Repos

### Flakes

- [divnix/devos](https://github.com/divnix/digga)
- [MatthewCroughan/nixcfg](https://github.com/MatthewCroughan/nixcfg)
- [hlissner/dotfiles](https://github.com/hlissner/dotfiles)
  ![dark themed rice](https://github.com/hlissner/dotfiles/raw/e628719d6d95404c3eb241b6c6ce00ff379b7bfa/alucard/fakebusy.png)
- [wiltaylor/dotfiles](https://github.com/wiltaylor/dotfiles)
- [wnaldheinz/nixos-config](https://github.com/waldheinz/nixos-config)
- [mitchellh/nixos-config](https://github.com/mitchellh/nixos-config)
  ![](https://raw.githubusercontent.com/mitchellh/nixos-config/main/.github/images/screenshot.png)
- ~~[fmrh/nixos](https://github.com/fmrh/nixos)~~
- [felschr/nixos-config](https://gitlab.com/felschr/nixos-config)
- [mudrii/systst](https://github.com/mudrii/systst)
  <details>
    <summary>Forks</summary>
    <ul>
    <li>https://github.com/amerocu/systst</li>
    <li>https://github.com/dvogeldev/systst</li>
    <li>https://github.com/lontivero/systst</li>
    <li>https://github.com/rmourey26/nixos-flakes-mud</li>
    </ul>
  </details>
- [danielfullmer/nixos-config](https://github.com/danielfullmer/nixos-config)
  <details>
    <summary>Forks</summary>
    <ul>
    <li>https://github.com/vulpi/danielfullmer-nixos-config</li>
    </ul>
  </details>
- [ztlevi/dotty-nix](https://github.com/ztlevi/dotty-nix)
- [bobbbay/dotfiles](https://github.com/bobbbay/dotfiles)
- [DAlperin/dotfiles](https://github.com/DAlperin/dotfiles)
- [jordanisaacs/dotfiles](https://github.com/jordanisaacs/dotfiles)
- ~~[papojari/home-manager-config](https://codeberg.org/papojari/home-manager-config)~~
- [nc6/dotnix](https://github.com/nc6/dotnix)
- [tvalladon/nixos_config](https://github.com/tvalladon/nixos_config)
- [xtruder/nix-profiles](https://github.com/xtruder/nix-profiles)
- [DubKoldun/nixconfigs](https://github.com/DubKoldun/nixconfigs)
- [fortuneteller2k/nix-config](https://github.com/fortuneteller2k/nix-config/tree/58498a329b7edc06b3c5624b67b0e92abd4bae32)
  ![](https://github.com/fortuneteller2k/nix-config/raw/master/assets/starcruiser.png)
  ![](https://github.com/fortuneteller2k/nix-config/raw/master/assets/screenshot.png)
- [romatthe/systems](https://github.com/romatthe/systems)
- [tomcur/nixos-configuration](https://github.com/tomcur/nixos-configuration)
- [J3T4R0/flk](https://github.com/J3T4R0/flk)
- [0qq/dotfiles](https://github.com/0qq/dotfiles/tree/6275794cf6e2fe449e0f5127d2211f3c8ea51501)
  ![](https://github.com/0qq/dotfiles/raw/master/screenshots/default.png)
- [wochap/nix-config](https://github.com/wochap/nix-config/tree/341d4037826f4caf845ba1c20222ba942f446861)
  ![](https://camo.githubusercontent.com/462d7f35472811c29a43f5bc66f0f8ec029d7d632b8022cfe7702e8d6159e619/68747470733a2f2f692e696d6775722e636f6d2f46756a304e58482e6a7067)
  <!-- ![](https://i.imgur.com/Fuj0NXH.jpg) -->
- [sioodmy/nixdots](https://github.com/sioodmy/nixdots/tree/794cbe6150557071e782083230fdc84f3bc2246c)
  ![](https://camo.githubusercontent.com/370946a4deae31db93441688736a7a17f5d967c677fdbdeaaabe3959f0bab7cc/68747470733a2f2f6d656469612e646973636f72646170702e6e65742f6174746163686d656e74732f313032303430333434393039323931313138362f313032343334313932353633303834343933392f756e6b6e6f776e2e706e673f77696474683d31313232266865696768743d363331)
- [wegank/nixos-config](https://github.com/wegank/nixos-config)
- [tboerger/nixos-config](https://github.com/tboerger/nixos-config)
- [rouzbeh/nixos](https://github.com/rouzbeh/nixos)
- ~~[viperML/nixos-flakes](https://github.com/viperML/nixos-flakes)~~
- [viperML/dotfiles](https://github.com/viperML/dotfiles)
  ![](https://github.com/viperML/dotfiles/raw/master/misc/img/20221016.png)
- [colemickens/nixcfg](https://github.com/colemickens/nixcfg)
- [iosmanthus/nixos-config](https://github.com/iosmanthus/nixos-config)
- [mschuwalow/nixos-config](https://github.com/mschuwalow/nixos-config)
- [wineee/nixos-config](https://github.com/wineee/nixos-config)
- [AloneER0/NixFiles](https://github.com/AloneER0/NixFiles/tree/37ef9445c26e741c9f900a550472b2283e603a75)
  ![underwater nixos](https://github.com/AloneER0/NixFiles/raw/37ef9445c26e741c9f900a550472b2283e603a75/nixos.png)
- [abdul2906/nixos-system-config](https://github.com/abdul2906/nixos-system-config)
  ![](https://github.com/abdul2906/nixos-system-config/raw/main/assets/workstation_screenshot_hyprland_gen2.png)
- [pupbrained/nix-config](https://github.com/pupbrained/nix-config)
- [FlafyDev/nixos-config](https://github.com/FlafyDev/nixos-config/tree/0204ed24c2311ac039569932fa40b62765f64a5c).
  ![screenshot of this halloween-themed NixOs rice using hyprland and flutter](https://i.redd.it/wwxexdy8p6x91.png)
  Posted on r/unixporn [here](https://www.reddit.com/r/unixporn/comments/yi2zq5) and [here](https://www.reddit.com/r/unixporn/comments/yikand).

  The revision may be [this](https://github.com/FlafyDev/nixos-config/tree/af27a7c7bb81244d5d775663e4f208ed220a249b) one.

  The command for `cbonsai` is `cbonsai --life=80 -l --time=0.008`

- [fedeizzo/nixOs-config](https://github.com/fedeizzo/nixOs-config)
  ![](https://github.com/fedeizzo/nix-dotfiles/raw/master/images/screenshot.png)

- [fufexan/dotfiles](https://github.com/fufexan/dotfiles/tree/55f53d73a3f7757935c53592084abc59e8ad4214)
  ![hyprland catpuccin](https://user-images.githubusercontent.com/36706276/192147190-cf9cf4df-94cb-4a3b-b9d8-137ed0c2538f.png)
  [Video](https://drive.google.com/file/d/1W-bwn3UwbMxReiiNqMmq38noa7Xw0Gj1/preview)

- [sherubthakur/dotfiles](https://github.com/sherubthakur/dotfiles)
  ![System Info](https://github.com/sherubthakur/dotfiles/raw/master/screenshots/tokyonight/sysinfo.png "System Info")
  ![Widgets](https://github.com/sherubthakur/dotfiles/raw/master/screenshots/tokyonight/widgets.png "Wallpaper")
  ![App-Launcher](https://github.com/sherubthakur/dotfiles/raw/master/screenshots/tokyonight/rofi-search.png "App Launcher")
  ![Ricing](https://github.com/sherubthakur/dotfiles/raw/master/screenshots/tokyonight/in-action.png "Ricing in progress")
  ![Locked](https://github.com/sherubthakur/dotfiles/raw/master/screenshots/tokyonight/locked.png "Locked")
  <details>
  <summary>Forks</summary>
  <ul>
    <li>https://github.com/cchalc/sherubthakur-dotfiles</li>
    <li>https://github.com/choyai/dotfiles</li>
    <li>https://github.com/eroglueren/dotfiles-sherubthakur</li>
    <li>https://github.com/langlwns/dotfiles</li>
    <li>https://github.com/Marcus-Arcadius/dotfiles-1</li>
    <li>https://github.com/mrlinuxdude/sherubthakur-dotfiles</li>
    <li>https://github.com/mwilhelmsson/nixos</li>
    <li>https://github.com/vulpi/sherubthakur-dotfiles</li>
  </ul>
  </details>
- [Drish-xD/dotfiles](https://github.com/Drish-xD/dotfiles)
  ![gallery](https://github.com/Drish-xD/dotfiles/raw/master/.github/Screenshot_1.png)
  ![gallery](https://github.com/Drish-xD/dotfiles/raw/master/.github/Screenshot_2.png)
  ![gallery](https://github.com/Drish-xD/dotfiles/raw/master/.github/Screenshot_3.png)
  ![gallery](https://github.com/Drish-xD/dotfiles/raw/master/.github/Screenshot_4.png)
  forks:
  - https://github.com/vulpi/Drish-xD-dotfiles
- [akkesm/dotfiles](https://github.com/akkesm/dotfiles)
- [sherubthakur/dotfiles](https://github.com/sherubthakur/dotfiles/tree/e0c4dafcef28147d7ce94334d6cc418caba945eb)
  ![](https://github.com/sherubthakur/dotfiles/raw/e0c4dafcef28147d7ce94334d6cc418caba945eb/screenshots/dracula/in-action.png)
  ![](https://github.com/sherubthakur/dotfiles/raw/e0c4dafcef28147d7ce94334d6cc418caba945eb/screenshots/dracula/locked.png)
  ![](https://github.com/sherubthakur/dotfiles/raw/e0c4dafcef28147d7ce94334d6cc418caba945eb/screenshots/dracula/rofi-search.png)
  ![](https://github.com/sherubthakur/dotfiles/raw/e0c4dafcef28147d7ce94334d6cc418caba945eb/screenshots/dracula/sysinfo.png)
  ![](https://github.com/sherubthakur/dotfiles/raw/e0c4dafcef28147d7ce94334d6cc418caba945eb/screenshots/dracula/wallpaper.png)
- [ISnortPennies/nixos](https://github.com/ISnortPennies/nixos)
- [redcode-labs/RedNixOS](https://github.com/redcode-labs/RedNixOS)
  ![](https://github.com/redcode-labs/RedNixOS/raw/master/assets/screenshot.png)
- [ImGabe/dotfiles](https://github.com/ImGabe/dotfiles)
  - https://www.reddit.com/r/unixporn/comments/qvbd20/i3gaps_nixosraspberry_pi_4_b_nord_theme
- [notusknot/dotfiles-nix](https://github.com/notusknot/dotfiles-nix/tree/a034dcb6daff31ce50cdbc74a5972b1ef56ef3d7)
  - ![screenshot](https://github.com/notusknot/dotfiles-nix/blob/a034dcb6daff31ce50cdbc74a5972b1ef56ef3d7/pics/screenshot.png)

### Non-flakes

- [Xe/nixos-configs](https://github.com/Xe/nixos-configs)
- [NeQuissimus/DevSetup](https://github.com/NeQuissimus/DevSetup)
- [NiharKod/dots](https://github.com/NiharKod/dots)
  ![](https://github.com/NiharKod/dots/raw/main/riceupdated.png)
- [oyren/dotfiles](https://github.com/oyren/dotfiles)
- [bennofs/etc-nixos](https://github.com/bennofs/etc-nixos)
- [kreativmonkey/nixos-config](https://github.com/kreativmonkey/nixos-config)
- [chris-martin/nixos-config](https://github.com/chris-martin/nixos-config)
- [maxdeviant/dotfiles](https://github.com/maxdeviant/dotfiles)

### Dotfiles

Yes, these are unrelated to NixOs

- [dwt1/dotfiles](https://gitlab.com/dwt1/dotfiles)
- [jdpedersen1/dotfiles](https://github.com/jdpedersen1/dotfiles)
- [BrodieRobertson/dotfiles](https://github.com/BrodieRobertson/dotfiles)
- [ChrisTitusTech/Debian-titus](https://github.com/ChrisTitusTech/Debian-titus)
- [Tanbinislam34/dotfiles](https://gitlab.com/Tanbinislam34/dotfiles)
  ![](https://gitlab.com/Tanbinislam34/dotfiles/-/raw/main/bspwm.png)
- [igmt-official/dotfiles](https://github.com/igmt-official/dotfiles)
  ![](https://user-images.githubusercontent.com/96023410/159257472-71e6eef5-07ac-4bd7-be85-bc52391b981e.png)
- ~~[elai1/dots](https://gitlab.com/elai1/dots/-/tree/main)~~
- [Aylur/dotfiles](https://github.com/Aylur/dotfiles)
- [adi1090x/widgets](https://github.com/adi1090x/widgets)
- [smravec/.dotfiles-hyprland](https://github.com/smravec/.dotfiles-hyprland)
- [JustineSmithies/hyprland-dotfiles](https://codeberg.org/JustineSmithies/hyprland-dotfiles)

### Aggregate

- https://github.com/nix-community/awesome-nix
<!--

### TODO

https://www.reddit.com/r/unixporn/comments/y2gait/gnome_can_anyone_identify_this_status_bar_on_the/
https://www.reddit.com/r/unixporn/comments/y0otiw/hyprland_ironing_out_the_eww_config_in_catppuccin/
https://www.reddit.com/r/unixporn/comments/xxu2pd/sway_hopped_on_the_nixos_train/
https://www.reddit.com/r/unixporn/comments/xz4xc6/hyprland_trying_out_some_catppuccin_colors/
https://www.reddit.com/r/unixporn/comments/xvjpkn/xfce_my_first_rice/
https://www.reddit.com/r/unixporn/comments/xze9jp/bspwm_my_first_bspwm_rice_ig/
https://www.reddit.com/r/unixporn/comments/xwnmz0/hyprland_featuring_custom_startpage/
https://www.reddit.com/r/unixporn/comments/wwtq70/openbox_eww_is_so_cool/
https://www.reddit.com/r/unixporn/comments/wtnv85/bspwm_quixotical/
https://www.reddit.com/r/unixporn/comments/wp7ea0/oc_rofi_rofi_powermenus_added_to_the_repository/
https://www.reddit.com/r/unixporn/comments/wsg1fl/oc_rofi_rofi_applets_a_bunch_of_useful_applets/
https://www.reddit.com/r/unixporn/comments/wp7ea0/oc_rofi_rofi_powermenus_added_to_the_repository/
https://www.reddit.com/r/unixporn/comments/wq8nzm/dwm_nix_and_onedark_is_a_great_match/
https://www.reddit.com/r/unixporn/comments/wjsv8x/bspwm_first_rice/
https://www.reddit.com/r/unixporn/comments/wk2ow0/qtile_first_functional_unixporn_rice/
https://www.reddit.com/r/unixporn/comments/wkiq96/plasma_back_on_nixos/
https://www.reddit.com/r/unixporn/comments/wlowdu/oc_transparency_patches_for_vscodium/
https://www.reddit.com/r/unixporn/comments/wh5zst/bspwm_eww_catppuccinofied_my_last_rice_come_get/
https://www.reddit.com/r/unixporn/comments/wgfmd5/polybar_rofi_cool_menu_by_making_rofi_look_the/
https://www.reddit.com/r/unixporn/comments/wfbafj/awesome_subarash%C4%AB/
https://www.reddit.com/r/unixporn/comments/wa23vk/awesome_just_switched_to_nixos_i_like_my_rice/
https://www.reddit.com/r/unixporn/comments/wb831o/bspwm_another_rice_with_my_custom_theme_called/
https://www.reddit.com/r/unixporn/comments/wda0b8/bspwm_gloomy_and_simple/
https://www.reddit.com/r/unixporn/comments/w5p7s0/hyprland_my_cute_desktop/
https://www.reddit.com/r/unixporn/comments/w6oj22/bspwm_make_bspwm_looks_like_dwm_while_cocking_rice/
https://www.reddit.com/r/unixporn/comments/w0ov58/bspwm_minimal/
https://www.reddit.com/r/unixporn/comments/w3w1i0/kde_plasma_bismuth_my_first_rice_really_happy/
https://www.reddit.com/r/unixporn/comments/w341yh/gnome_i_decided_to_go_full_nord/
https://www.reddit.com/r/unixporn/comments/w17kkj/the_rabbit_hole/
https://www.reddit.com/r/unixporn/comments/vxwfge/kde_simple_yet_beautiful/
https://www.reddit.com/r/unixporn/comments/vy3vzu/i3gaps_first_time_using_i3_not_sure_ifs_common/
https://www.reddit.com/r/unixporn/comments/vqk6m3/qtile_i_guess_catppuccin_is_my_new_favorite/
https://www.reddit.com/r/unixporn/comments/vdiyv6/fvwm_third_try_configuring_fvwm/
https://www.reddit.com/r/unixporn/comments/vcb72b/bspwm_minimal_gentoo_nord_theme/
https://www.reddit.com/r/unixporn/comments/v9dw91/bspwm_darkmy_daily_drive/
https://www.reddit.com/r/unixporn/comments/ujp70m/third_times_the_charm_sway_catppuccin/
https://www.reddit.com/r/unixporn/comments/ulxt92/neovim_bring_forth_your_prettiest_neovim_rice_for/
https://www.reddit.com/r/unixporn/comments/ugjcgk/comment/i6zzglr/
https://www.reddit.com/r/unixporn/comments/ueko4r/sway_swayos_for_designproductivity_freaks/
https://www.reddit.com/r/unixporn/comments/udw5kv/dwm_frozen_glass/
https://www.reddit.com/r/unixporn/comments/u3hynu/i3gaps_my_daily_driver_for_now/
https://www.reddit.com/r/unixporn/comments/u4ywpj/dwm_working_remotely/
https://www.reddit.com/r/unixporn/comments/u794cx/bspwm_first_rice_but_stolen_configs_and_dotfiles/
https://www.reddit.com/r/unixporn/comments/tvjoa3/gnome_my_own_nord_theme/
https://www.reddit.com/r/unixporn/comments/u1yh6d/i3gaps_it_is_so_fcking_awesome_that_i_love_it/
https://www.reddit.com/r/unixporn/comments/txd7z2/gnome_my_own_minimalistic_nord_theme/
https://www.reddit.com/r/unixporn/comments/tyzyg3/lightdm_based_on_glsl_theme_osmos/
https://www.reddit.com/r/unixporn/comments/tzjdqx/bspwm_unixy/
https://www.reddit.com/r/unixporn/comments/tzu6nu/awesome_beautiful_day_aesthetic_night/
https://www.reddit.com/r/unixporn/comments/tzvob7/bspwm_suggest_me_some_wallpapers/
https://www.reddit.com/r/unixporn/comments/tzvsdp/bspwm_in_love_with_my_desktop/
https://www.reddit.com/r/unixporn/comments/tvyp6l/kde_plasma_newbie_4_months_in/
https://www.reddit.com/r/unixporn/comments/tvhiv9/qtile_why_is_my_code_spaghetti_code_all_of_a/
https://www.reddit.com/r/unixporn/comments/tnqtzt/sway_ship_of_theseus/
https://www.reddit.com/r/unixporn/comments/tnm25h/fvwm_dark_star/
https://www.reddit.com/r/unixporn/comments/tmv3ty/dwm_feeling_a_bit_suckless/
https://www.reddit.com/r/unixporn/comments/tmic3k/gnome_floating_window_managers_can_still_look_good/
https://www.reddit.com/r/unixporn/comments/talyc5/sway_first_tilingwm_waybar_fancy_colors/
https://www.reddit.com/r/unixporn/comments/ta0rcj/stumpwm_emacs_stumpwm_on_nixos_gotham_theme_low/
https://www.reddit.com/r/unixporn/comments/t95s1f/qtile_a_nordiced_rice_with_some_spice_thrown_in/
https://www.reddit.com/r/unixporn/comments/t8ypms/dwm_tokyocatppuccin/
https://www.reddit.com/r/unixporn/comments/t8bsew/bspwm_cold_rice/
https://www.reddit.com/r/unixporn/comments/t7z7fn/xfce_adrenaline/

https://youtube.com/watch?v=MwnK6arB2Rc

https://github.com/Bismuth-Forge/bismuth
https://github.com/elkowar/eww

https://github.com/kencx/keyb
https://linux.die.net/man/1/setxkbmap
https://linux.die.net/man/1/xcursorgen

## Blogs

https://cthu.lu/
https://www.joseferben.com/archives/
https://www.commitstrip.com/en/?

## Misc

http://starlogs.net/#fedeizzo/epoc
https://dagger.io/
https://github.com/n8n-io/n8n
https://nix.dev
https://ntfy.sh/
https://spectrum-os.org/
https://app.revolt.chat/login/create
https://github.com/revoltchat/revolt
https://app.arcade.software/share/RIrEisEk7V36paQmqtNI

## Security

https://github.com/xtls/xray-core
https://dnstwister.report/
https://github.com/aya-rs/aya
https://github.com/deepfence/ThreatMapper
https://www.cisa.gov/free-cybersecurity-services-and-tools
https://xsinator.com/
https://riptutorial.com/c/example/23858/trigraphs
https://www.csoonline.com/article/3629049/kubernetes-hardening-drilling-down-on-the-nsa-cisa-guidance.html#tk.rss_all
-->
