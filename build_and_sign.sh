#! /usr/bin/env bash

HOST=$1

if [[ $HOST == "" ]]; then
  echo "This script builds the derivation corresponding to the config of the given hostname."
  echo "Then signs it and all the outputs on which it depends."
  echo "This way, other computers that trust this one as a binary cache can avoid having"
  echo "to compile those packages again."
  echo "You need to have root permissions to sign the outputs in the store."
  echo ""
  echo "This script takes one argument, the hostname."
  echo "Example of usage -to build cetus config-:"
  echo "./build_and_sign.sh cetus"
else
  if [ -d result ]; then
    echo "A directory called 'result' is already present."
    echo "Remove it in order to use this script."
    exit 1
  fi

  # I prefer getting the sudo password prompt here rather than later
  sudo echo -n ""

  nixos-rebuild build --flake ".#${HOST}"

  if [ -d result ]; then
    TIMESTAMP=$(date +%s)
    DEP_FILE_NAME="dependencies_${TIMESTAMP}.tmp"
    store_path=$(readlink -f ./result)
    nix-store --query --requisites "${store_path}" >$DEP_FILE_NAME
    #nix path-info --derivation --recursive "${store_path}" >$DEP_FILE_NAME
    echo "Signing the packages"
    while read -r DEPENDENCY; do
      sudo nix store sign --key-file /run/secrets/nixcache --recursive ${DEPENDENCY}
    done <$DEP_FILE_NAME
    rm -rf $DEP_FILE_NAME result
    echo "Done!"
  else
    echo "Missing the results folder!!!"
    exit 1
  fi
fi
