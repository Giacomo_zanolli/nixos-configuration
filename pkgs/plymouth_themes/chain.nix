{ pkgs ? import <nixpkgs> { } }:
pkgs.stdenv.mkDerivation rec {
  pname = "chain-plymouth-theme";
  version = "1.0.0";
  themeName = "chain";

  src = pkgs.fetchFromGitHub {
    #Downloads https://github.com/<owner>/<repo>/archive/<rev>.tar.gz
    owner = "Hugopikachu";
    repo = "plymouth-theme-chain";
    rev = "v1.0.0";
    sha256 = "sha256-qvCzYK5Ti/YH7c6rg0AJS/C/efe4cERLY9tS+WFmluM=";
  };

  configurePhase = ''
    mkdir -p $out/share/plymouth/themes/
  '';

  buildPhase = "";

  installPhase = ''
    target=$out/share/plymouth/themes/chain
    cp -r . $target
    cat chain.plymouth | sed  "s@\/usr\/@$out\/@" > $target/chain.plymouth
    rm $target/change-color.sh
  '';
}
