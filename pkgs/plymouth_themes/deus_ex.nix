# Based on https://blog.sidhartharya.com/using-custom-plymouth-theme-on-nixos
{ pkgs ? import <nixpkgs> { } }:
pkgs.stdenv.mkDerivation rec {
  pname = "plymouth-theme-deus_ex";
  version = "0.0.1";

  themeName = "deus_ex";
  themePack = "2";
  themePath = "pack_${themePack}/${themeName}";

  src = pkgs.fetchFromGitHub {
    #Downloads https://github.com/<owner>/<repo>/archive/<rev>.tar.gz
    owner = "adi1090x";
    repo = "plymouth-themes";
    rev = "bf2f570bee8e84c5c20caac353cbe1d811a4745f";
    sha256 = "sha256-VNGvA8ujwjpC2rTVZKrXni2GjfiZk7AgAn4ZB4Baj2k=";
  };

  # buildInputs = [
  #   pkgs.git
  # ];

  configurePhase = ''
    mkdir -p $out/share/plymouth/themes/
  '';

  buildPhase = "";

  installPhase = ''
    cp -r ${themePath} $out/share/plymouth/themes
    cat ${themePath}/${themeName}.plymouth | sed  "s@\/usr\/@$out\/@" > $out/share/plymouth/themes/${themeName}/${themeName}.plymouth
  '';
}
