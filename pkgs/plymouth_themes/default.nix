{ self }:
{
  plymouth-theme-deus_ex = self.callPackage ./deus_ex.nix { };
  plymouth-theme-chain = self.callPackage ./chain.nix { };
}
