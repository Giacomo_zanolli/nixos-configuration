{ lib
, rustPlatform
, makeWrapper
, psmisc
, fetchFromGitHub
}:

rustPlatform.buildRustPackage {
  pname = "snake";
  version = "0.1.4";

  src = fetchFromGitHub {
    owner = "kschat";
    repo = "snake";
    rev = "v0.1.4";
    sha256 = "sha256-SJPSCVMldNte+9zPud+mPcYqmTlarxklfcAe8sTia6M=";
  };

  cargoHash = "sha256-1mWjO1BxfNP3Sl3EesE/uHlsMitRoXtNRdLrjF75jZA="; #lib.fakeSha256;

  nativeBuildInputs = [ makeWrapper ];
}
