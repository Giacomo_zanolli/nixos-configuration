self: super: {
  swhkd = self.callPackage ./swhkd.nix { };
  xclipboard = self.callPackage ./xclipboard.nix { };
  xsnow = self.callPackage ./xsnow.nix { };
  rain.sh = self.callPackage ./rain.nix { };
  snake = self.callPackage ./snake.nix { };
  pixel_fireworks = self.callPackage ./pixel_fireworks.nix { };
  pixel_sand = self.callPackage ./pixel_sand.nix { };
  shell_smash = self.callPackage ./shell_smash.nix { };
  tetrotime = self.callPackage ./tetrotime.nix { };
  fddf = self.callPackage ./fddf.nix { };
  plymouth_themes = (import ./plymouth_themes) { inherit self; };
}
