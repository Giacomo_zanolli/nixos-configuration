{ lib
, rustPlatform
, makeWrapper
, psmisc
, fetchFromGitHub
}:

rustPlatform.buildRustPackage {
  pname = "pixel_fireworks";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "jakobwesthoff";
    repo = "pixel_fireworks";
    rev = "8b76fe5d6a3d54f53b5832cb2033de62712e9e75";
    sha256 = "sha256-IjEZTyEdmI+WIEeMntxYVZu+7/LbSKaVYEMf/Pi/cIg=";
  };

  cargoHash = "sha256-00Y9veEXbH3WGml17tNxCxju9hmc/RrRpKuENmePjL4=";

  nativeBuildInputs = [ makeWrapper ];
}
