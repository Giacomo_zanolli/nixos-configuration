{ lib
, rustPlatform
, makeWrapper
, psmisc
, fetchFromGitHub
}:

rustPlatform.buildRustPackage {
  pname = "tetrotime";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "jakobwesthoff";
    repo = "tetrotime";
    rev = "24383330234173375e13959bb436252904a95151";
    sha256 = "sha256-2yTKwzA1LlqGZiX0bZuxuwrloLIHprivtFrixTR5Jiw=";
  };

  cargoHash = "sha256-IR2A4+J/0D1dyU5dbKo6gFN5XPUaDXqHjpwtuKqB7lE=";

  nativeBuildInputs = [ makeWrapper ];
}
