{ lib
, rustPlatform
, makeWrapper
, fetchFromGitHub
}:

rustPlatform.buildRustPackage {
  pname = "fddf";
  version = "1.7.0";

  src = fetchFromGitHub {
    owner = "birkenfeld";
    repo = "fddf";
    rev = "v1.7.0";
    sha256 = "sha256-Je8GaSGMbkOp17+ap2f4tZI8irI68JxnnjZc6R1WyRs=";
  };

  cargoHash = "sha256-7BkMP4I0twKugSfoMimHsnUr8IOBRJGiAkVbQ3UZkkI="; #lib.fakeSha256;

  nativeBuildInputs = [ makeWrapper ];
}
