{ lib
, rustPlatform
, makeWrapper
, psmisc
, fetchFromGitHub
}:

rustPlatform.buildRustPackage {
  pname = "swhkd";
  version = "1.2.1";

  src = fetchFromGitHub {
    owner = "waycrate";
    repo = "swhkd";
    #Using this commit instead of the previous, tagged release otherwise the cargo.lock was out of sync
    rev = "19d6395cb9e0e20aea5bf938f0330fa890acd906";
    sha256 = "sha256-Q9aIwATmZNOaskosdXgUlmZMIr/V/dxd8H16B4CtPt4=";
  };

  cargoHash = "sha256-OsHLJ6P24u63MLpnv9zUye6Mks+oP2XiRcEORMbIL6k="; #lib.fakeSha256;

  nativeBuildInputs = [ makeWrapper ];

  postInstall = ''
    install -D -m0444 -t "$out/share/polkit-1/actions" ./com.github.swhkd.pkexec.policy

    substituteInPlace "$out/share/polkit-1/actions/com.github.swhkd.pkexec.policy" \
      --replace /usr/bin/swhkd \
        "$out/bin/swhkd"
  '';
}
