{ stdenv
, fetchFromGitHub
}:

#   package =
stdenv.mkDerivation rec {
  pname = "rain";
  version = "0.0.1";

  buildInputs = [ ];

  src = fetchFromGitHub {
    owner = "lbgists";
    repo = "rain.sh";
    rev = "eb3becff704b838ff3b23dc1d3301c23004c73d4";
    sha256 = "sha256-1ZEGQDD8uTspaMTfVb7ARHvkuaWAdIaxiAdMQqTxfLk=";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp rain.sh $out/bin/rain
  '';
}
