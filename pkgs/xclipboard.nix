{ stdenv
, fetchFromGitHub
, autoconf
, automake
, xorg
, pkg-config
}:

#   package =
stdenv.mkDerivation rec {
  pname = "xclipboard";
  version = "1.1.3";

  buildInputs = [ autoconf automake xorg.utilmacros pkg-config xorg.libXaw ];

  src = fetchFromGitHub {
    owner = "freedesktop";
    repo = "xclipboard";
    rev = "xclipboard-${version}";
    sha256 = "sha256-N1FdZNGy1GtlkaKL1e+vDj7/9h/xcNeXR67eEB7VV0I=";
  };

  preConfigure = "./autogen.sh";
  configureFlags = [ "--with-appdefaultdir=$(out)" ];
}
