{ lib
, rustPlatform
, makeWrapper
, psmisc
, fetchFromGitHub
}:

rustPlatform.buildRustPackage {
  pname = "shell_smash";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "jakobwesthoff";
    repo = "shell_smash";
    rev = "3bb06fc95d5e3c875e91dcb1c7fa01dc717d7def";
    sha256 = "sha256-LNLqkODO4yGHa96p2MHXDw9tVljYUeD+Xxl2m1JP3FY=";
  };

  cargoHash = "sha256-eJ6yV9XXkpgLZth/AnRuWKoZLdOqQcsOfV/wComXVt8=";

  nativeBuildInputs = [ makeWrapper ];
}
