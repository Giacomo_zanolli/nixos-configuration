{ lib
, rustPlatform
, makeWrapper
, psmisc
, fetchFromGitHub
}:

rustPlatform.buildRustPackage {
  pname = "pixel_sand";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "jakobwesthoff";
    repo = "pixel_sand";
    rev = "bdb7589aea5c586ecf5405ca2e7f6713c2095832";
    sha256 = "sha256-7IPkPG+Is4yWp3LwkHIjl+zxbWPT0Ps6WgOFNRFnFUo=";
  };

  cargoHash = "sha256-1EmmKP7Gaq9672dSI0ekZc/xspNMGDmbLaBwh8lyGRw=";

  nativeBuildInputs = [ makeWrapper ];
}
