#! /usr/bin/env sh

if [ "$1" == "-s" ] || [ "$1" == "--switch" ]; then
  echo "Building and switching"
  nixos-rebuild switch --use-remote-sudo
elif [ "$1" == "-b" ] || [ "$1" == "--build-only" ]; then
  echo "Building only"
  nixos-rebuild build --use-remote-sudo
elif [ "$1" == "-t" ] || [ "$1" == "--test" ]; then
  echo "Building and testing"
  nixos-rebuild test --use-remote-sudo
elif [ "$1" == "deploy" ]; then
  if [ $# != 2 ]; then
    echo "No hostname specified as second argument. Exiting"
  else
    HOSTNAME=$2
    # ./build_and_sign.sh "$HOSTNAME" &&
    deploy ".#$HOSTNAME"
  fi
else
  echo "Building and marking as boot"
  nixos-rebuild boot --use-remote-sudo
fi
