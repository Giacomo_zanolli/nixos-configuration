{
  description = "C++ development environment with debugging support and VSCodium";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = {
            allowUnfree = true;
            allowUnfreePredicate = pkg: builtins.elem (pkgs.lib.getName pkg) [
              "vscode-extension-ms-vscode-cpptools"
            ];
          };
        };
      in
      {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            gcc
            gdb
            cmake
            gnumake
            clang-tools
            (pkgs.vscode-with-extensions.override {
              vscode = pkgs.vscodium;
              vscodeExtensions = with pkgs.vscode-extensions; [
                ms-vscode.cpptools
                ms-vscode.cmake-tools
                vadimcn.vscode-lldb
                jnoortheen.nix-ide
              ];
            })
          ];

          shellHook = ''
            echo "C++ development environment with VSCodium loaded"
          '';
        };
      }
    );
}
