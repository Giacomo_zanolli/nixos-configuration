{
  description = "Dev environment with all you need to develop in go with VSCode/Codium";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = {
            # allowUnfree = true;
          };
        };
      in
      {
        devShell = with pkgs; mkShell rec {
          buildInputs = [
            gcc
            glibc
            go
            #Go specific
            gopkgs
            go-outline
            gopls
            gotest
            gomodifytags
            impl
            delve
            go-tools # includes `staticcheck`
            govulncheck
            (vscode-with-extensions.override {
              vscode = vscodium;
              vscodeExtensions = with vscode-extensions; [
                golang.go
                esbenp.prettier-vscode
                #Nix specific
                jnoortheen.nix-ide
              ];
            })
          ];
        };
      });
}
