This template is for developing Android apps with Flutter.

To use this template:

1. You must have adb (Android Debug Bridge) active. You can refer to [this page](https://nixos.wiki/wiki/Android) for setup instructions. (On nixos, `programs.adb.enable = true;` and `users.users.yourUsername.extraGroups = [ "adbusers" ]`)
1. Copy the `flake.nix` and `flake.lock` in the root directory of the flutter project,
1. Create a `.envrc` file in the same directory
1. Run `direnv allow`
1. Done!

## Troubleshooting

- If when building you get an error like "Unexpected Kernel Format Version 75 (expected 77)",
  run the `nuke_flutter.sh` script
- If when building you get an error like "Failed to install the following SDK components: build-tools;28.0.3 Android SDK Build-Tools 28.0.3", then just add the missing version (in this case 28.0.3 to the `buildToolsVersions` list in `flake.nix`)
