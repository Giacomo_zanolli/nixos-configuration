#! /bin/sh

#Execute this if you start getting errors like:
#"Unexpected Kernel Format Version 75 (expected 77)"

#This script removes flutter, dart, gradle and the android sdk
#so the next time everything starts clean

rm -rf ~/.android ~/.dart ~/.gradle ~/.pub-cache ~/.flutter ~/.config/flutter ~/.dartServer ~/.flutter-devtools
