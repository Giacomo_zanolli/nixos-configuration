# 0.0.9

- Add virt-manager for KVM virtual machines
- Can hybernate!

# 0.0.8

- Change bash for zsh
- Change shell prompt to starship
- Change konsole for alacritty
- Add direnv templates for fast development environment bootstrapping
- Well, it's getting difficult to always track all the changes..

# 0.0.7

- Got Flutter to work
- Many other changes I did not keep track of. But luckily there is git

# 0.0.6

- New partitioning scheme with LUKS2 encrypted swap (that supports hibernation)
  and amnesic root filesystem (ZFS)
- ZFS datasets (/, /nix and /home) are compressed with lz4
- Added info to connect to wifi when necessary

# 0.0.5

- Wallpapers are downloaded automatically and it is now trival to specify fallbacks
- Revisiting the partition scheme.
  Now the disk is 512MB EFI + LUKS.
  LUKS inside has LVM
  LVM has inside a swap partition, an ext4 /nix partition,
  an 8GB zfs partition for / and the remaining space for ext4 /home.

# 0.0.4

- Can interpolate nix variables in config files for easier theming.
- Added sxhkd shortcuts for browsers and file explorers.
- Integrate first run script in setup script

# 0.0.3

- More modular configuration, which makes it easier to switch between themes.

# 0.0.2

- Improved install script

# 0.0.1

- First config, no home-manager
