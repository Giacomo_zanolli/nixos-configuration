#! /usr/bin/env sh

TARGET=devon

rm nixos.qcow2
rm -r ./result

nixos-rebuild build-vm --flake .#$TARGET

./result/bin/run-nixos-vm
