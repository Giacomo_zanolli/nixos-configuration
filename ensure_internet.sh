#! /bin/bash

if [ $(ping -c 1 google.com) -ne 0 ]; then
  if [ $(ping -c 1 github.com) -ne 0 ]; then
    if [ $(ping -c 1 8.8.8.8) -ne 0 ]; then
      if [ $(ping -c 1 1.1.1.1) -ne 0 ]; then
        echo "Well, it looks like we are offline."
        exit 1
      fi
    fi
  fi
fi

echo "Checking if we have no network card up and running"
ip link | grep --quiet "state UP"
isANetworkCardUp=$?
interfaceNumbers=()
interfaceNames=()
if [ ! isANetworkCardUp ]; then
  echo "Listing network interfaces. Please choose the one to activate"
  cardRegex="([0-9]+): (.+):.+state (\w+) .+"
  while read line; do
    if [[ $line =~ $cardRegex ]]; then
      number="${BASH_REMATCH[1]}"
      name="${BASH_REMATCH[2]}"
      state="${BASH_REMATCH[3]}"
      echo $number: $name $state
      interfaceNumbers+=("$number")
      interfaceNames+=("$name")
    fi
  done < <(ip link)

  valid=0
  while [ $valid -eq 0 ]; do
    echo
    read -p "Choose the number of the card to activate: " chosenNumber
    for i in "${interfaceNumbers[@]}"; do
      if [ "$i" -eq "$chosenNumber" ]; then
        valid=1
      fi
    done
  done

  chosenInterface=${interfaceNames[chosenNumber - 1]}
  ip link set $chosenInterface up
  echo "Waiting for interface $chosenInterface to go up"
  sleep 5
fi

echo "The following WiFi networks are available"
sudo iw $chosenInterface scan | grep SSID

read -p "Input the SSID of the network you want to use: " ssid
read -p "Input the passphrase for $ssid: " passphrase

wpa_passphrase $ssid $passphrase >/etc/wpa_supplicant/wpa_supplicant.conf

wpa_supplicant -i $chosenInterface -c /etc/wpa_supplicant/wpa_supplicant.conf | grep "CTRL-EVENT-CONNECTED"
areWeConnected=$?

if [ $areWeConnected -ne 0 ]; then
  echo "Error in connecting to the Wifi. Exiting"
  exit 1s
fi

echo "Success. Starting in background"
wpa_supplicant -B -i $chosenInterface -c /etc/wpa_supplicant/wpa_supplicant.conf
echo "Waiting for connection"
sleep 10

echo "Getting an IP"
udhcpc -i $chosenInterface
